//
//  SelectMethodBackgroundMenu.h
//  Piktomir
//
//  Created by Бесшапошников Никита on 27.03.14.
//
//

#ifndef __Piktomir__SelectMethodExecuteTypeMenu__
#define __Piktomir__SelectMethodExecuteTypeMenu__

#include <iostream>
#include "cocos2d.h"

const float SMB_BORDER = 5.0f;
const float SMB_SEPARATOR = 5.0f;

class MethodLayer;

class SelectMethodExecuteTypeMenu : public cocos2d::Layer
{
public:
    SelectMethodExecuteTypeMenu(MethodLayer *methodLayer, int buttonIndex):
        cocos2d::Layer(),
        methodLayer(methodLayer),
        buttonIndex(buttonIndex)
    {}
    
    ~SelectMethodExecuteTypeMenu()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    virtual bool init() override;
    
    static SelectMethodExecuteTypeMenu *create(MethodLayer *methodLayer, int buttonIndex);
private:
    MethodLayer *methodLayer;
    int buttonIndex;
    
    cocos2d::LayerColor *layerColor;
    
    void selectExecuteType(cocos2d::Ref *sender);
};

#endif /* defined(__Piktomir__SelectMethodBackgroundMenu__) */
