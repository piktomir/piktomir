//
//  ClientLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 19.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "ClientLayer.h"
#include "PMSettings.h"

USING_NS_CC;
USING_NS_CC_EXT;

bool ClientLayer::initWithSize(float width)
{
    if(!init())
        return false;
    
    setContentSize(Size(width - 2 * CLIENTLAYER_BOUND, 20));
    
    checkBox->getParent()->setPosition( Point(CLIENTLAYER_BOUND/2, getContentSize().height/2 - checkBox->getContentSize().height/2) );
    
    nameLabel->setPosition( Point(CLIENTLAYER_BOUND + checkBox->getContentSize().width,0) );
    statusLabel->setPosition( Point(getContentSize().width / 2, 0) );
    gameLabel->setPosition( Point(nameLabel->getPositionX() + nameLabel->getContentSize().width + 10.0, 0) );
    //statusSprite->setPosition( Point(getContentSize().width, 0) );
    
    return true;
}

bool ClientLayer::initWithName(const char *name,float width)
{
    if(!initWithSize(width))
        return false;
    
    nameLabel->setString(name);
    return true;
}

bool ClientLayer::init()
{
    if ( !LayerColor::initWithColor( Color4B(100,100,100,200) ) )
    {
        return false;
    }
    
    
    
   
    Sprite *uncheckedSprite = Sprite::create(PICT_PATH "System/unchecked1.png");
    //uncheckedSprite->setScale( 0.5 );
    
    checkBox = MenuItemSprite::create(uncheckedSprite, uncheckedSprite, CC_CALLBACK_1(ClientLayer::onCheckClicked, this));
    checkBox->setPosition( Point::ZERO );
    checkBox->setAnchorPoint(Point::ZERO);
    checked = false;
    
    //checkBox->setAnchorPoint( Point::ZERO );
    
    Menu *menu = Menu::create(checkBox, nullptr);
    menu->setAnchorPoint( Point::ZERO );
    menu->setContentSize( checkBox->getContentSize() );
    addChild(menu);
    
    nameLabel = Label::create("", "Arial", 18);
    gameLabel = Label::create("", "Arial", 18);
    statusLabel = Label::create("", "Arial", 18);
    
    nameLabel->setAnchorPoint( Point::ZERO );
    gameLabel->setAnchorPoint( Point::ZERO );
//    statusSprite->setAnchorPoint( Point(1.0,0.0) );
//    statusSprite->setPosition( Point::ZERO );
    
    addChild(nameLabel);
    addChild(statusLabel);
    addChild(gameLabel);
    //addChild(statusSprite);
    
    return true;
}

void ClientLayer::refreshStatus()
{

}

void ClientLayer::setGame(int world, int level)
{
    std:
    gameLabel->setString(StringUtils::format("Игра: %d.%d",world + 1,level + 1).c_str());
}

void ClientLayer::clearGame()
{
    gameLabel->setString("");
}

void ClientLayer::forceSetChecked(bool flag)
{
    checked = flag;
    
    if(checked)
    {
        checkBox->setNormalImage(Sprite::create(PICT_PATH "System/checked1.png"));
        checkBox->setSelectedImage(Sprite::create(PICT_PATH "System/checked1.png"));
    }
    else
    {
        checkBox->setNormalImage(Sprite::create(PICT_PATH "System/unchecked1.png"));
        checkBox->setSelectedImage(Sprite::create(PICT_PATH "System/unchecked1.png"));
    }
}

void ClientLayer::setWidth(float width)
{
    setContentSize( Size(width - 2 * CLIENTLAYER_BOUND, 20) );
}

void ClientLayer::onCheckClicked(cocos2d::Ref *sender)
{
    checked = !checked;
    
    if(checked)
    {
        checkBox->setNormalImage(Sprite::create(PICT_PATH "System/checked1.png"));
        checkBox->setSelectedImage(Sprite::create(PICT_PATH "System/checked1.png"));
    }
    else
    {
        checkBox->setNormalImage(Sprite::create(PICT_PATH "System/unchecked1.png"));
        checkBox->setSelectedImage(Sprite::create(PICT_PATH "System/unchecked1.png"));
    }
}