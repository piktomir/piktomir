//
//  ClientLayer.h
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 19.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__ClientLayer__
#define __pictomir_cocos2d_x__ClientLayer__

#include <iostream>
#include <algorithm>
#include "cocos2d.h"
#include "cocos-ext.h"
#include "PMSettings.h"

#include "PMClient.h"

const float CLIENTLIST_SEPARATOR = 10.0;
const float CLIENTLAYER_BOUND = 20.0;

class ClientLayer :public cocos2d::LayerColor
{

    SYNTHESIZE(cocos2d::Label *, nameLabel, NameLabel);
    SYNTHESIZE(cocos2d::Label *,statusLabel, StatusLabel);
    SYNTHESIZE(cocos2d::Sprite *,statusSprite,StatusSprite);
    SYNTHESIZE(bool, checked, Checked);
public:
    ClientLayer()
    {
        
    }
    
    virtual bool init();
    bool initWithSize(float width);
    bool initWithName(const char *name,float width);
    
    CREATE_FUNC(ClientLayer);
    
    static ClientLayer* create(float width)
    {
        ClientLayer *pRet = new ClientLayer();
        if (pRet && pRet->initWithSize(width))
        {
            pRet->autorelease();
            return pRet;
        }
        else
        {
            delete pRet;
            pRet = nullptr;
            return nullptr;
        }
    }
    
    static ClientLayer* create(const char *name, float width)
    {
        ClientLayer *pRet = new ClientLayer();
        if (pRet && pRet->initWithName(name, width))
        {
            pRet->autorelease();
            return pRet;
        }
        else
        {
            delete pRet;
            pRet = nullptr;
            return nullptr;
        }
    }

    
    ~ClientLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    void setWidth(float width);
    
    void forceSetChecked(bool flag);
    
    void setGame(int world, int level);
    void clearGame();
    
private:
    void refreshStatus();
    cocos2d::MenuItemSprite *checkBox;
    cocos2d::Label *gameLabel;
    
    void onCheckClicked(cocos2d::Ref *sender);
};

#endif /* defined(__pictomir_cocos2d_x__ClientLayer__) */
