//
//  HintLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/10/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "HintLayer.h"
#include "PMTextureCache.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

int HintLayer::hintCount;

HintLayer::~HintLayer()
{
    removeAllChildrenWithCleanup(true);
    --hintCount;
}

HintLayer *HintLayer::create(std::string text)
{
    HintLayer *pRet = new HintLayer(text);
    
    if(pRet && pRet->init()) {
        pRet->autorelease();
        return pRet;
    } else {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }

}

bool HintLayer::init()
{
    ++hintCount;
    
    if ( !initWithColor(Color4B(145,145,145,255 * 0.4)) )
    {
        return false;
    }
 
    LabelTTF *label = LabelTTF::create(text.c_str(), "Marker Felt", 20);
    Sprite *sprite = PMTextureCache::instanse()->getIconSprite("info");
    
    setContentSize( Size(label->getContentSize().width + sprite->getContentSize().width,sprite->getContentSize().height) );
    
    label->setPosition( Point(sprite->getContentSize().width,0) );
    sprite->setPosition( Point::ZERO );
    label->setAnchorPoint( Point(0,0) );
    sprite->setAnchorPoint( Point(0,0) );   
    
    addChild(label);
    addChild(sprite);
    
    Size screenSize = PMSettings::instance()->getScreenSize();
    
    setPosition( Point(160, screenSize.height - 50 - hintCount * sprite->getContentSize().height) );
    
    return true;
}
#ifdef MAP_EDITOR
int HintEditLayer::hintCount;

HintEditLayer::~HintEditLayer()
{
    --hintCount;
}

HintEditLayer *HintEditLayer::create(std::string text, std::string placeHolder)
{
    HintEditLayer *pRet = new HintEditLayer(text, placeHolder);
    
    if(pRet && pRet->init()) {
        pRet->autorelease();
        return pRet;
    } else {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
    
}

bool HintEditLayer::init()
{
    ++hintCount;
    
    if ( !initWithColor(Color4B(145,145,145,255 * 0.4)) )
    {
        return false;
    }
    
    Scale9Sprite* EditBoxSprite = Scale9Sprite::create("System/scalesprite.png");
    
    Sprite *sprite = PMTextureCache::instanse()->getInfoIconSprite();
    hint = EditBox::create(Size(400, 30), EditBoxSprite);
    hint->setFontColor(Color3B::BLACK);
    
    hint->setPlaceholderFont("Marker Felt",18);
    hint->setFont("Marker Felt",18);
    
    hint->setPlaceHolder(placeHolder.c_str());
    hint->setText(text.c_str());
    
    hint->setAnchorPoint(Point::ZERO);
    hint->setPosition(Point(sprite->getContentSize().width,0));
    
    setContentSize( Size(hint->getContentSize().width + sprite->getContentSize().width,sprite->getContentSize().height) );
    
    sprite->setPosition( Point::ZERO );
    sprite->setAnchorPoint( Point(0,0) );
    
    addChild(hint);
    addChild(sprite);
    
    Size screenSize = PMSettings::instance()->getScreenSize();
    
    setPosition( Point(160, screenSize.height - 50 - hintCount * sprite->getContentSize().height) );
    
    return true;
}
#endif

