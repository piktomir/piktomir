//
//  ProgramLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 30.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#include "MethodLayer.h"
#include <zlib.h>
#include "utils.h"
#include <string.h>
#include "ProgramLayer.h"
#include "PMSettings.h"
#include "CCGL.h"
#include "PMTextureCache.h"
#include "PMSettings.h"
#include "MethodStackLayer.h"
#include "GameLayer.h"
#include "RobotMethod.h"

#ifdef MAP_EDITOR
#include "PMPopoverMethodButton.h"
#include "PMPopoverRepeaterButton.h"
#include "PMPopoverConditionbutton.h"
#endif

USING_NS_CC;
using namespace std;


void MethodLayer::setMethod (int type, int index)
{
    switch (type) {
        case 0:
            methods[index].backgroundType = _method::pmNone;
            break;
        case 1:
            if(methods[index].backgroundType == _method::pmNone)
                methods[index].backgroundType = _method::pmNormal;
            methods[index].executeType = _method::pmExecuteOnTrue;
            break;
        case 2:
            if(methods[index].backgroundType == _method::pmNone)
                methods[index].backgroundType = _method::pmNormal;
            methods[index].executeType = _method::pmExecuteAlways;
            
            break;
        case 3:
            if(methods[index].backgroundType == _method::pmNone)
                methods[index].backgroundType = _method::pmNormal;
            methods[index].executeType = _method::pmExecuteOnFalse;
            
            break;
    }
    
    removeAllChildrenWithCleanup(true);
    drawFunctions();
}

bool MethodLayer::init()
{
    if ( !initWithColor(Color4B(0,0,0,255)) )
    {
        return false;
    }
    
    draggedSprite = nullptr;
    draggedMethod = -1;
    hint = nullptr;
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(MethodLayer::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(MethodLayer::touchEnded, this);
    listener->onTouchMoved = CC_CALLBACK_2(MethodLayer::touchMoved, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

void MethodLayer::drawFunctions()
{
#ifdef MAP_EDITOR
    useRepeater = true;
    useCondition = true;
    resizable = true;
    
    newHint = std::vector<int>(0);
    
    if(lastBackground.size()!=methods.size()) {
        lastBackground.resize(methods.size());
        for (int i = 0; i < methods.size(); ++i) {
            lastBackground[i] = methods[i].backgroundType;
        }
    }
#endif
    
    if(hidden)
    {
        string givenLabelText = LocalizedString("GivenText");
        string doLabelText = LocalizedString("DoText");
        
        givenLabelText += givenText;
        doLabelText += doText;
        
        Label* givenLabel = Label::create(givenLabelText.c_str(), "Arial", 18, Size::ZERO,TextHAlignment::LEFT);
        Label* doLabel = Label::create(doLabelText.c_str(), "Arial", 18, Size::ZERO,TextHAlignment::LEFT);
        
        realWidth = MAX(givenLabel->getContentSize().width, doLabel->getContentSize().width);
        realHeight = givenLabel->getContentSize().height + doLabel->getContentSize().height + 20;
        
        givenLabel->setPosition( Point(0,doLabel->getContentSize().height + 20 ) );
        givenLabel->setAnchorPoint( Point(0,0) );
        
        doLabel->setPosition( Point(0,10) );
        doLabel->setAnchorPoint( Point(0,0) );
        
        addChild(givenLabel);
        addChild(doLabel);

    }
    else
    {
        int useCorR = (useRepeater || useCondition) ? 1 : 0;
        int useCAndR = (useRepeater && useCondition) ? 1 : 0;
        int addWidth = (useCAndR && width < 2)? 1 : 0;
        
        
        realWidth = width * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) + addWidth * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) - METHODLAYER_TILE_OFFSET;
        realHeight = height * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) + useCorR * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET);
    }
    
    string display_name;
    Sprite *nameSprite = nullptr;
    
    if(methodID == CMDMAIN){
        display_name = LocalizedString("MainProgram");
    } else if(methodID == CMDA) {
        display_name = LocalizedString("SubProgram");
    } else if(methodID == CMDB) {
        display_name = LocalizedString("SubProgram");
    } else if(methodID == CMDC) {
        display_name = LocalizedString("SubProgram");
    }
    
    if(methodID != CMDMAIN)
    {
        nameSprite = PMTextureCache::instanse()->getProgramSprite(methodID, _method::pmExecuteOnTrue, NORMAL_STATE);
    }
    
    Label* nameLabel = Label::create(display_name.c_str(), "Arial", 18, Size::ZERO,TextHAlignment::LEFT);
    
    
    nameLabel->setPosition( Point(-BORDER_RADIUS / 2,realHeight + METHODLAYER_TILE_OFFSET ) );
    nameLabel->setAnchorPoint( Point(0,0) );
    
    nameLength = nameLabel->getContentSize().width;
    
    if(nameSprite != nullptr)
    {
        addChild(nameSprite , 0, NAME_SPRITE_TAG);
        nameSprite->setPosition( Point(nameLength, realHeight + BORDER_RADIUS ));
        nameSprite->setAnchorPoint( Point::ZERO );
        nameSprite->setScale(0.8f);
        nameLength += nameSprite->getContentSize().width;
    }
    
    

    
#ifdef MAP_EDITOR
    
    MenuItemSprite *saveHintSprite = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("saveHint"), PMTextureCache::instanse()->getIconSprite("saveHint"), CC_CALLBACK_1(MethodLayer::makeHint, this));
    
    //MenuItemImage *saveHintSprite = MenuItemImage::create("System/SaveHintIcon.png", "System/SaveHintIcon_Selected.png", "System/SaveHintIcon_Disabled.png", );
    saveHintSprite->setPosition(Point(0,-3));
    saveHintSprite->setAnchorPoint(Point::ZERO);
    deleteMenu = Menu::create(saveHintSprite, nullptr);
    deleteMenu->setAnchorPoint(Point::ZERO);
    deleteMenu->setPosition(Point(nameLength + 40, realHeight + BORDER_RADIUS / 2));
    addChild(deleteMenu, 2);
    
    if(name != "main")
    {
         MenuItemSprite *deleteSprite = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("deleteIcon"), PMTextureCache::instanse()->getIconSprite("deleteIcon"), CC_CALLBACK_1(MethodLayer::deleteMethod, this));
        //MenuItemImage *deleteSprite = MenuItemImage::create("System/DeleteIcon.png", "System/DeleteIcon_Selected.png", "System/DeleteIcon_Disabled.png", CC_CALLBACK_1(MethodLayer::deleteMethod, this));
        deleteSprite->setPosition(Point(40,0));
        deleteSprite->setAnchorPoint(Point::ZERO);
        deleteMenu->addChild(deleteSprite);
        deleteMenu->setAnchorPoint(Point::ZERO);
        deleteMenu->setPosition(Point(nameLength + 40, realHeight + BORDER_RADIUS / 2));
    }
#endif

    addChild(nameLabel, ML_BACKGROUND_ZORDER, NAME_LABEL_TAG);
    
    setContentSize( Size(realWidth, realHeight + nameLabel->getContentSize().height + METHODLAYER_TILE_OFFSET) );
    
    if(!hidden)
    {
        backGroundLayer  = Layer::create();
        backGroundLayer->setContentSize( Size(realWidth,realHeight) );
        backGroundLayer->setPosition( Point::ZERO );
    
        methodLayer  = Layer::create();
        methodLayer->setContentSize( Size(realWidth,realHeight) );
        methodLayer->setPosition( Point::ZERO );
        
        addChild(backGroundLayer, ML_BACKGROUND_ZORDER);
        addChild(methodLayer, ML_CONTENT_ZORDER);
        
        int Y = realHeight;
        int X = 0;
        
        Y -= BUTTON_TILE_SIZE;
        
        if (useRepeater)
        {
            Sprite *tmp = PMTextureCache::instanse()->getRepeaterSprite(EMPTY_REPEATER, NORMAL_STATE);
            tmp->setPosition( Point(X, Y) );
            tmp->setAnchorPoint( Point::ZERO );
            
            addChild(tmp, ML_BACKGROUND_ZORDER, REPEATER_BUTTON_TAG);
            
            if(repeater < 7  && repeater != REPEAT_DISABLE)
            {
                tmp = PMTextureCache::instanse()->getRepeaterSprite(repeater, NORMAL_STATE);
            }
            else if (repeater == REPEAT_DISABLE)
            {
                tmp = PMTextureCache::instanse()->getRepeaterSprite(1, DISABLED_STATE);
            }
            else if(repeater >= 7)
            {
                tmp = PMTextureCache::instanse()->getRepeaterSprite(7, NORMAL_STATE);
                
                char string[3];
                sprintf(string, "%d", repeater);
                
                Label *sprLabel = Label::create(string, "Arial", 20, Size::ZERO,TextHAlignment::LEFT);
                sprLabel->setPosition( Point(tmp->getContentSize().width/2, tmp->getContentSize().height/2) );
                
                tmp->addChild(sprLabel, 1, 1);

            }
            
            tmp->setPosition( Point(X, Y) );
            tmp->setAnchorPoint( Point::ZERO );
            addChild(tmp, ML_CONTENT_ZORDER, REPEATER_SPRITE_TAG);
            
            X += BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET;
            
            if(!useCondition)
                Y -= (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET);
            
        }
        
        if (useCondition)
        {
            Sprite *tmp = PMTextureCache::instanse()->getRobotConditionSprite(EMPTY_CONDITION, NORMAL_STATE);
            tmp->setPosition( Point(X, Y) );
            tmp->setAnchorPoint( Point::ZERO );
            addChild(tmp, ML_BACKGROUND_ZORDER, CONDITION_BUTTON_TAG);
            
            
            if(condition != EMPTY_CONDITION && condition != -1)
            {
                tmp = PMTextureCache::instanse()->getRobotConditionSprite(condition, NORMAL_STATE);
            }
            else if (condition == -1)
            {
                tmp = PMTextureCache::instanse()->getRobotConditionSprite(EMPTY_CONDITION, DISABLED_STATE);
            }
            else
            {
                tmp = PMTextureCache::instanse()->getRobotConditionSprite(EMPTY_CONDITION, NORMAL_STATE);
            }
            
            tmp->setPosition( Point(X, Y) );
            tmp->setAnchorPoint( Point::ZERO );
            
            addChild(tmp, ML_CONTENT_ZORDER, CONDITION_SPRITE_TAG);
            
            Y -= (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET);
        }
        
        for (int j = 0; j < width * height; ++j)
        {
            if(methods[j].backgroundType != _method::pmNone)
            {
                Sprite *tmp = PMTextureCache::instanse()->getRobotMethodSprite(EMPTY_METHOD, methods[j].executeType, NORMAL_STATE);
                tmp->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) * (j%width) + BUTTON_TILE_SIZE / 2,
                                      Y - (j/width) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) + BUTTON_TILE_SIZE / 2)  );
                //tmp->setAnchorPoint( Point::ZERO );
                
                backGroundLayer->addChild(tmp);
                
                if(methods[j].methodID >= EMPTY_METHOD)
                {
                    tmp = PMTextureCache::instanse()->getRobotMethodSprite(methods[j].methodID, methods[j].executeType, NORMAL_STATE);
                }
                else if(methods[j].methodID >= CMDC && methods[j].methodID < EMPTY_METHOD)
                {
                    tmp = PMTextureCache::instanse()->getProgramSprite(methods[j].methodID, methods[j].executeType, NORMAL_STATE);
                }
                
                tmp->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) * (j%width), Y - (j/width) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET))  );
                tmp->setAnchorPoint( Point::ZERO );
                
                methodLayer->addChild(tmp);
                
            }
#ifdef MAP_EDITOR
            else
            {
                Sprite *tmp = PMTextureCache::instanse()->getRobotMethodSprite(EMPTY_METHOD, methods[j].executeType, DISABLED_STATE);
                tmp->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) * (j%width), Y - (j/width) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET))  );
                tmp->setAnchorPoint( Point::ZERO );
                
                backGroundLayer->addChild(tmp);
                
                if(methods[j].methodID >= EMPTY_METHOD)
                {
                    tmp = PMTextureCache::instanse()->getRobotMethodSprite(methods[j].methodID, methods[j].executeType, DISABLED_STATE);
                }
                else if(methods[j].methodID >= CMDC && methods[j].methodID < EMPTY_METHOD)
                {
                    tmp = PMTextureCache::instanse()->getProgramSprite(methods[j].methodID, methods[j].executeType, DISABLED_STATE);
                }
                
                tmp->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) * (j%width), Y - (j/width) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET))  );
                tmp->setAnchorPoint( Point::ZERO );
                
                methodLayer->addChild(tmp);
            }
#endif
            if(methods[j].backgroundType == _method::pmBlocked)
            {
                Sprite *tmp = PMTextureCache::instanse()->getIconSprite("lock");
                tmp->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) * (j%width) + BUTTON_TILE_SIZE - tmp->getContentSize().width,
                                      Y - (j/width) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET))  );
                tmp->setAnchorPoint( Point::ZERO );
                addChild(tmp, ML_BUTTONS_ZORDER);
            }
#ifdef MAP_EDITOR
            else
            {
                Sprite *tmp = PMTextureCache::instanse()->PMTextureCache::instanse()->getIconSprite("unlock");
                tmp->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) * (j%width) + BUTTON_TILE_SIZE - tmp->getContentSize().width,
                                      Y - (j/width) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET))  );
                tmp->setAnchorPoint( Point::ZERO );
                methodLayer->addChild(tmp, 14);
            }
            
#endif
            if(methods[j].backgroundType == _method::pmChangeBackground)
            {
//                MenuItemSprite *selectBackground = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("selectColor", NORMAL_STATE),
//                                                                      PMTextureCache::instanse()->getIconSprite("selectColor", SELECTED_STATE),
//                                                                      PMTextureCache::instanse()->getIconSprite("selectColor", DISABLED_STATE),
//                                                                      CC_CALLBACK_1(MethodLayer::showSelectBackgroundMenu, this));
//                selectBackground->setAnchorPoint( Point::ZERO );
//                selectBackground->setTag(j);
//                
//                Menu *menu = Menu::create(selectBackground, nullptr);
//                                                              
//                menu->setContentSize( Size(selectBackground->getContentSize().width, selectBackground->getContentSize().height));
//                                                              
//                menu->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) * (j%width) + BUTTON_TILE_SIZE - selectBackground->getContentSize().width,
//                                         Y - (j/width) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET)) );
//                menu->setAnchorPoint( Point::ZERO );
//                
//                addChild(menu, ML_BUTTONS_ZORDER);
                
                
                Sprite *tmp = PMTextureCache::instanse()->getIconSprite("selectColor");
                tmp->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) * (j%width) + BUTTON_TILE_SIZE - tmp->getContentSize().width,
                                        Y - (j/width) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET))  );
                tmp->setAnchorPoint( Point::ZERO );
                addChild(tmp, ML_BUTTONS_ZORDER);
            }
            
            
        }
        
        
        if(resizable)
        {
            resizeSprite = Sprite::create("System/unchecked.png");
            
            if(width == 1)
                resizeSprite->setPosition(Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) + BUTTON_TILE_SIZE -10, -10));
            else
                resizeSprite->setPosition(Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) * (width-1) + BUTTON_TILE_SIZE -10, -10));
            
            resizeSprite->setAnchorPoint(Point::ZERO);
            addChild(resizeSprite, ML_BUTTONS_ZORDER);
        }
#ifdef MAP_EDITOR
        printf("%d",lockHeight);
        if(lockHeight)lockHeightSprite = PMTextureCache::instanse()->PMTextureCache::instanse()->getIconSprite("unlock");
        else lockHeightSprite = PMTextureCache::instanse()->PMTextureCache::instanse()->getIconSprite("lock");
        if(width == 1) lockHeightSprite->setPosition(Point(-10, -10));
        else lockHeightSprite->setPosition(Point( -10, -10));
        lockHeightSprite->setAnchorPoint( Point::ZERO );
        addChild(lockHeightSprite, 14);
#endif
        
        pushProgramButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("paste", NORMAL_STATE),
                                                   PMTextureCache::instanse()->getIconSprite("paste", SELECTED_STATE),
                                                   PMTextureCache::instanse()->getIconSprite("paste", DISABLED_STATE),
                                                   CC_CALLBACK_1(MethodLayer::addProgram, this));
        
        pushProgramButton->setPosition( Point(PMSettings::instance()->getProgramLayerWidth(),0) );
        pushProgramButton->setAnchorPoint( Point::ZERO );
        
        Menu *menu = Menu::create(pushProgramButton, nullptr);
        
        menu->setContentSize( Size(PMSettings::instance()->getProgramLayerWidth(), pushProgramButton->getContentSize().height));
        
        menu->setPosition( Point(0,getContentSize().height - nameLabel->getContentSize().height - METHODLAYER_TILE_OFFSET - menu->getContentSize().height) );
        menu->setAnchorPoint( Point::ZERO );
        
        addChild(menu, 10);
    }
    
    drawHint();
}


bool MethodLayer::conditionAtTouch(cocos2d::Touch *touch)
{
    if(!useCondition)
        return false;
    
    Sprite *sprite  = (Sprite *)getChildByTag(CONDITION_SPRITE_TAG);
    
    Rect boundingBox = sprite->boundingBox();
    boundingBox.setRect(boundingBox.getMinX() - METHODLAYER_TILE_OFFSET/2, boundingBox.getMinY() + METHODLAYER_TILE_OFFSET/2,
                        boundingBox.size.width + METHODLAYER_TILE_OFFSET, boundingBox.size.height + METHODLAYER_TILE_OFFSET);

    return boundingBox.containsPoint(this->convertTouchToNodeSpace(touch));
    
}

bool MethodLayer::repeaterAtTouch(cocos2d::Touch *touch)
{
    if(!useRepeater)
        return false;
    
    Sprite *sprite  = (Sprite *)getChildByTag(REPEATER_SPRITE_TAG);
    
    Rect boundingBox = sprite->boundingBox();
    boundingBox.setRect(boundingBox.getMinX() - METHODLAYER_TILE_OFFSET/2, boundingBox.getMinY() + METHODLAYER_TILE_OFFSET/2,
                        boundingBox.size.width + METHODLAYER_TILE_OFFSET, boundingBox.size.height + METHODLAYER_TILE_OFFSET);
    
    return boundingBox.containsPoint(this->convertTouchToNodeSpace(touch));
}

int MethodLayer::methodAtTouch(Touch *touch)
{
    for(int j = 0; j < methods.size(); ++j)
    {
        Sprite *sprite  = (Sprite *)backGroundLayer->getChildren().at(j);
        
        Rect boundingBox = sprite->boundingBox();
        boundingBox.setRect(boundingBox.getMinX() - METHODLAYER_TILE_OFFSET/2, boundingBox.getMinY() + METHODLAYER_TILE_OFFSET/2,
                            boundingBox.size.width + METHODLAYER_TILE_OFFSET, boundingBox.size.height + METHODLAYER_TILE_OFFSET);
        
        if(boundingBox.containsPoint(this->convertTouchToNodeSpace(touch)))
        {
            return j;
        }
        
    }
    
    return -1;
}

int MethodLayer::lockAtTouch(Touch *touch)
{
    for(int j = methods.size(); j < 2*methods.size(); ++j)
    {
        Sprite *sprite  = (Sprite *)methodLayer->getChildren().at(j);
        
        Rect boundingBox = sprite->boundingBox();
        
        if(boundingBox.containsPoint(methodLayer->convertTouchToNodeSpace(touch)))
        {
            return j-methods.size();
        }
        
    }
    
    return -1;
}

void MethodLayer::resizeWidth(int sgn)
{
    if(sgn == 1)
    {
        _method emptyMethod(0, _method::pmNormal, _method::pmExecuteOnTrue);
        width++;
        for(int i = 0; i < height; ++i)
        {
            methods.insert(methods.begin() + width*(i+1) - 1, emptyMethod);
            //backGround.insert(backGround.begin() + width*(i+1) - 1, 1);
        }
    }
    else
    {
        for(int i = 0; i < height; ++i)
        {
            methods.erase(methods.begin() + width*(height - i) - 1);
            //backGround.erase(backGround.begin() + width*(height - i) - 1);
        }
        width--;
    }
    removeAllChildrenWithCleanup(true);
    drawFunctions();
}

void  MethodLayer::resize(int sgn)
{
    if(sgn == 1)
    {
        _method emptyMethod(0, _method::pmNormal, _method::pmExecuteOnTrue);
        
        for(int i = 0; i < width; ++i)
        {
            
            methods.push_back(emptyMethod);
            //backGround.push_back(1);
        }
    }
    else
    {
        for(int i = 0; i < width; ++i)
        {
            methods.pop_back();
            //backGround.pop_back();
        }
    }
    setContentSize( Size(getContentSize().width, getContentSize().height + sgn * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET)) );
    realHeight += sgn * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET);
    height += sgn;
    
    removeAllChildrenWithCleanup(true);
    drawFunctions();
    parentLayer->update();
    

}

cocos2d::Sprite *MethodLayer::cloneMethodSprite(int method)
{
    return draggedSprite = Sprite::createWithSpriteFrame( ((Sprite *)methodLayer->getChildren().at(method))->getSpriteFrame());
}

cocos2d::Sprite *MethodLayer::cloneConditionSprite()
{
    return Sprite::createWithSpriteFrame( ((Sprite *)getChildByTag(CONDITION_SPRITE_TAG))->getSpriteFrame() );
}

cocos2d::Sprite *MethodLayer::cloneRepeaterSprite()
{
    return Sprite::createWithSpriteFrame( ((Sprite *)getChildByTag(REPEATER_SPRITE_TAG))->getSpriteFrame() );
}

bool MethodLayer::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    #ifdef MAP_EDITOR
    if (PMPopoverMethodButton::getInstanse() && PMPopoverMethodButton::getInstanse()->getShowed()) {
        return false;
    }
    #endif

    Point touchPoint = this->convertTouchToNodeSpace(touch);

#ifndef MAP_EDITOR 
    // here will be a sprite
    if(resizable && touchPoint.y >=0 && touchPoint.y <= 50 && touchPoint.x >= realWidth - 50 && touchPoint.x <= realWidth)
    {
        touchType = mlResize;
        startPoint = parentLayer->convertTouchToNodeSpace(touch);
        
        return true;
    }
#else
    if(resizeSprite->boundingBox().containsPoint(touchPoint))
    {
        touchType = mlResize;
        startPoint = _parent->convertTouchToNodeSpace(touch);
        
        return true;
    }
#endif
    
    
    return false;
    
}


void MethodLayer::touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
//    if( touch && draggedSprite != nullptr &&
//       (touchType == mlDragRepeater || touchType == mlDragMethod || touchType == mlDragCondition))
//        draggedSprite->setPosition(convertTouchToNodeSpace(touch));
#ifndef MAP_EDITOR
    if(touch && touchType == mlResize)
    {
        Point touchPoint  = parentLayer->convertTouchToNodeSpace(touch);
        
        int diff = (startPoint - touchPoint).y;
        
        if(diff >= BUTTON_TILE_SIZE)
        {
            if(height + 1 > maxHeight)
                return;
                
            resize(1);
        
            startPoint = touchPoint;
        }
        
        if(diff <= -BUTTON_TILE_SIZE)
        {
            if(height - 1 < minHeight)
                return;
            
            resize(-1);
            
            startPoint = touchPoint;
        }
    }
#else
    if(touch && touchType == mlResize)
    {
        Point touchPoint  = _parent->convertTouchToNodeSpace(touch);
        
        int diff = (startPoint-touchPoint).y;
        
        if(diff >= BUTTON_TILE_SIZE)
        {
            resize(1);
            startPoint.y = touchPoint.y;
        }
        
        if(diff <= -BUTTON_TILE_SIZE)
        {
            if(height - 1 < 1)
                return;
            
            resize(-1);
            startPoint.y = touchPoint.y;
        }
        
        
        diff = (startPoint-touchPoint).x;
        
        if(diff >= BUTTON_TILE_SIZE)
        {
            if(width < 2)
                return;
            resizeWidth(-1);
            startPoint.x = touchPoint.x;
        }
        
        if(diff <= -BUTTON_TILE_SIZE)
        {
            resizeWidth(1);
            startPoint.x = touchPoint.x;
        }
    }
#endif
}

void MethodLayer::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
}

void MethodLayer::setButton(SelectButton *activeButton, Touch *touch)
{
    if(activeButton != nullptr)
    {
        int method = methodAtTouch(touch);
        bool isConditionAtTouch = conditionAtTouch(touch);
        bool isRepeaterAtTouch = repeaterAtTouch(touch);
        
        if(activeButton->isMethod() && method != -1 && methods[method].backgroundType != _method::pmBlocked )
            addAction(method, activeButton->getMethodID(), true);
        else if(activeButton->isCondition() && isConditionAtTouch)
            addCondition(activeButton->getCondition());
        else if(activeButton->isRepeater() && isRepeaterAtTouch)
            addRepeater(activeButton->getRepeater());
    }
    else
    {
        int method = methodAtTouch(touch);
        
        if(method != -1 && methods[method].backgroundType == _method::pmChangeBackground)
        {
            setNextExecuteType(method);
        }
    }
}

void MethodLayer::addAction(int methodIndex,int newMethodID, bool byProgramLayer)
{
    SpriteFrame *frame = nullptr;
    
    if(methods[methodIndex].backgroundType == _method::pmBlocked)
        return;
    
    if(methods[methodIndex].methodID != newMethodID)
    {
        if(newMethodID >= EMPTY_METHOD)
            frame = PMTextureCache::instanse()->getRobotMethodSpriteFrame(newMethodID, methods[methodIndex].executeType, NORMAL_STATE);
        else
            frame = PMTextureCache::instanse()->getProgramSpriteFrame(newMethodID, methods[methodIndex].executeType, NORMAL_STATE);
        
        methods[methodIndex].methodID = newMethodID;
    }
    else if(methods[methodIndex].backgroundType == _method::pmChangeBackground)
    {
        setNextExecuteType(methodIndex);
    }
    
    if(frame != nullptr)
        ((Sprite *)methodLayer->getChildren().at(methodIndex))->setSpriteFrame(frame);
#ifndef MAP_EDITOR
    parentLayer->getParentLayer()->saveMap();
#endif
}

void MethodLayer::addCondition(int newCondition)
{
    SpriteFrame *frame = nullptr;
    
    if(newCondition == -1)
    {
        frame = PMTextureCache::instanse()->getRobotConditionSpriteFrame(EMPTY_CONDITION, DISABLED_STATE);
        condition = -1;
    }
    
    else if(condition != newCondition)
    {
        frame = PMTextureCache::instanse()->getRobotConditionSpriteFrame(newCondition, NORMAL_STATE);
        
        condition = newCondition;
    }
   /* else if(parentLayer->getActiveButton() != nullptr)
    {
        condition = EMPTY_CONDITION;
        tex = PMTextureCache::instanse()->getConditionBackgroundTexture();
    }*/
    
    if(frame != nullptr)
        ((Sprite *)getChildByTag(CONDITION_SPRITE_TAG))->setSpriteFrame(frame);
    
#ifndef MAP_EDITOR
    parentLayer->getParentLayer()->saveMap();
#endif
}

void MethodLayer::addRepeater(int newRepeater)
{
    SpriteFrame *frame = nullptr;
    
    //if(newRepeater == -1) {
      //  tex = PMTextureCache::instanse()->getRepeaterTexture_Disabled(1);
        //repeater = -1;
    //}
    
    Sprite *spr = (Sprite *)getChildByTag(REPEATER_SPRITE_TAG);
    if(spr->getChildByTag(1))
        spr->removeChildByTag(1);
    
    if(repeater != newRepeater)
    {
        if(newRepeater < 7 && repeater != REPEAT_DISABLE)
        {
            frame = PMTextureCache::instanse()->getRepeaterSpriteFrame(newRepeater, NORMAL_STATE);
        }
        else if(newRepeater >= 7)
        {
            frame = PMTextureCache::instanse()->getRepeaterSpriteFrame(7, NORMAL_STATE);
            
            char string[3];
            sprintf(string, "%d", newRepeater);
            
            Label *sprLabel = Label::create(string, "Arial", 20, Size::ZERO, TextHAlignment::LEFT);
            sprLabel->setPosition( Point(spr->getContentSize().width/2, spr->getContentSize().height/2) );
            
            spr->addChild(sprLabel, 10, 1);

        }
        else if(newRepeater == REPEAT_DISABLE)
        {
            frame = PMTextureCache::instanse()->getRepeaterSpriteFrame(1, DISABLED_STATE);
        }
        
        
        
        repeater = newRepeater;
    }
    /*else if(parentLayer->getActiveButton() != nullptr)
    {
        repeater = 1;
        tex = PMTextureCache::instanse()->getRepeaterTexture(repeater);
    }*/
    
    if(frame != nullptr)
        spr->setSpriteFrame(frame);
    
#ifndef MAP_EDITOR
    parentLayer->getParentLayer()->saveMap();
#endif
}

void MethodLayer::setNextExecuteType(int method)
{
    methods[method].executeType = (_method::ExecuteType)((methods[method].executeType + 1) % 3);
    
    SpriteFrame *frame = PMTextureCache::instanse()->getRobotMethodSpriteFrame(EMPTY_METHOD, methods[method].executeType, NORMAL_STATE);
    Sprite *sprite = (Sprite *)backGroundLayer->getChildren().at(method);
    
    sprite->setSpriteFrame(frame);
    
    frame = PMTextureCache::instanse()->getRobotMethodSpriteFrame(methods[method].methodID, methods[method].executeType, NORMAL_STATE);
    sprite = (Sprite *)methodLayer->getChildren().at(method);
    
    sprite->setSpriteFrame(frame);
}

void MethodLayer::setControlledMode(bool flag)
{
    setEnabled( !flag , false);
    
    if(!hidden)
    {
        double endX = realWidth + BORDER_RADIUS;
        
        if(!flag)
            endX = PMSettings::instance()->getProgramLayerWidth();
        
        FiniteTimeAction* moveButton = MoveTo::create(SYSTEM_ANIMATION_DELAY / 2, Point(endX,0) );
        pushProgramButton->runAction(moveButton);
    }
}

void MethodLayer::addProgram(cocos2d::Ref *sender)
{
    std::vector<int> methods = ((GameLayer *)parentLayer->getParentLayer())->getMethodStackLayer()->getProgram();
    
    for(int j = 0; j < methods.size() && j < width * height; ++j)
    {
        addAction(j, methods[j]);
    }
    
    for(int j = methods.size(); j < width * height; ++j)
    {
        addAction(j, EMPTY_METHOD);
    }

}

/*void MethodLayer::showSelectExecuteTypeMenu(cocos2d::Ref *sender)
{
    Node *senderNode = (Node *)sender;
    Node *parentNode = senderNode->getParent();
    
    SelectMethodExecuteTypeMenu *menu = SelectMethodExecuteTypeMenu::create(this, senderNode->getTag());
    menu->setPosition( Point(parentNode->getPositionX() - menu->getContentSize().width / 4,
                             parentNode->getPositionY() + parentNode->getContentSize().height) );
    
    addChild(menu, ML_HINT_ZORDER);
}*/

void MethodLayer::drawHint()
{
#ifdef MAP_EDITOR
    if(hint == nullptr) {
        hint = Hint::create();
        
        HintPart part;
        part.type = TextHint;
        part.hintText = "";
        hint->hintParts.push_back(part);
    }
#endif
    if(hint != nullptr)
    {
        showHintButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("info", NORMAL_STATE),
                                                PMTextureCache::instanse()->getIconSprite("info", SELECTED_STATE),
                                                PMTextureCache::instanse()->getIconSprite("info", DISABLED_STATE),
                                                CC_CALLBACK_1(MethodLayer::showHint, this));
        
        Menu *menu = Menu::create(showHintButton, nullptr);
        
        double x = nameLength + showHintButton->getContentSize().width / 2;
        double y = getContentSize().height - showHintButton->getContentSize().height / 2;
        
        showHintButton->setPosition( Point::ZERO );
        menu->setPosition( Point(x,y) );
        menu->setContentSize(showHintButton->getContentSize());
        
        addChild(menu, ML_BUTTONS_ZORDER, INFO_BUTTON_TAG);
    }
}

void MethodLayer::showHint(cocos2d::Ref *sender)
{
#ifndef MAP_EDITOR
    for(int i = 0; i < hint->hintParts.size(); ++i)
    {
        if(hint->hintParts[i].type == TextHint)
        {
            if(getChildByTag(HINT_LAYER_TAG) == nullptr)
            {
                MethodHintLayer *hintLayer = MethodHintLayer::create(hint->hintParts[i].hintText, (Node *)sender);

                addChild(hintLayer, ML_HINT_ZORDER, HINT_LAYER_TAG);
            }
        }
        else if(hint->hintParts[i].type == MethodHint)
        {
            for(int j = 0; j < hint->hintParts[i].funcHint.size(); ++j)
            {
                addAction(j, hint->hintParts[i].funcHint[j].methodID);
            }
        }
    }
#else
//    if(!hintShowed)
//    {
//        std::string HintName = "";
//        
//        switch (methodID) {
//            case CMDMAIN:
//                HintName = "Подсказка для главной программы";
//                break;
//            case CMDA:
//                HintName = "Подсказка для подпрограммы А";
//                break;
//            case CMDB:
//                HintName = "Подсказка для подпрограммы B";
//                break;
//            case CMDC:
//                HintName = "Подсказка для подпрограммы C";
//                break;
//        }
//        
////        if (hint->hintParts[0].type == TextHint) hintLayer = HintEditLayer::create(hint->hintParts[0].hintText,HintName);
////        else hintLayer = HintEditLayer::create(hint->hintParts[1].hintText,HintName);
//        
//        parentLayer->getParentLayer()->addChild(hintLayer,20);
//        hintShowed = true;
//    }
//    else
//    {
//        std::string text = "";//hintLayer->hint->getText();
//        if(text!="") {
//            HintPart part;
//            part.type = TextHint;
//            part.hintText = text;
//            
//            if(hint->hintParts[0].type == TextHint)
//            {
//                hint->hintParts[0] = part;
//            }
//            else {
//                if (hint->hintParts.size() == 1)
//                {
//                    hint->hintParts.push_back(part);
//                }
//                else hint->hintParts[1] = part;
//            }
//        }
//        parentLayer->getParentLayer()->removeChild(hintLayer);
//        hintShowed = false;
//    }
#endif
}

void MethodLayer::highlightMethodBreak(int method)
{
    SpriteFrame *selectedTexture = PMTextureCache::instanse()->getRobotMethodSpriteFrame(EMPTY_METHOD, methods[method].executeType, FAILED_STATE);
    Sprite *sprite = (Sprite *)backGroundLayer->getChildren().at(method);
    
    sprite->setSpriteFrame(selectedTexture);
    //sprite->setContentSize(selectedTexture->getContentSize());
    //sprite->setTextureRect(Rect(0,0,selectedTexture->getContentSize().width,selectedTexture->getContentSize().height));
}

void MethodLayer::highlightMethod(int method)
{
    SpriteFrame *selectedTexture = PMTextureCache::instanse()->getRobotMethodSpriteFrame(EMPTY_METHOD, methods[method].executeType, SELECTED_STATE);
    Sprite *sprite = (Sprite *)backGroundLayer->getChildren().at(method);
    
    sprite->setSpriteFrame(selectedTexture);
//    sprite->setContentSize(selectedTexture->getContentSize());
//    sprite->setTextureRect(Rect(0,0,selectedTexture->getContentSize().width,selectedTexture->getContentSize().height));
}

void MethodLayer::clearHighlightMethod(int method)
{
    SpriteFrame *normalTexture = PMTextureCache::instanse()->getRobotMethodSpriteFrame(EMPTY_METHOD, methods[method].executeType, NORMAL_STATE);
    Sprite *sprite = (Sprite *)backGroundLayer->getChildren().at(method);
    
    sprite->setSpriteFrame(normalTexture);
//    sprite->setContentSize(normalTexture->getContentSize());
//    sprite->setTextureRect(Rect(0,0,normalTexture->getContentSize().width,normalTexture->getContentSize().height));
}

void ccDrawArc(float cx, float cy, float r, float start_angle, float arc_angle, int num_segments)
{

    float theta = arc_angle / float(num_segments - 1);
	float tangetial_factor = tanf(theta);
    
	float radial_factor = cosf(theta);
    
	
	float x = r * cosf(start_angle);
	float y = r * sinf(start_angle);
    
    Point *arc_vertices;
    arc_vertices = (Point *)malloc(sizeof(Point) * (num_segments));
    
	for(int ii = 0; ii < num_segments; ii++)
	{
        arc_vertices[ii].x = x+ cx;
        arc_vertices[ii].y = y +cy;
        
		float tx = -y;
		float ty = x;
        
		x += tx * tangetial_factor;
		y += ty * tangetial_factor;
        
		x *= radial_factor;
		y *= radial_factor;
    }
    
    DrawPrimitives::drawPoly(arc_vertices, num_segments, false);
    free(arc_vertices);
}

void MethodLayer::draw(cocos2d::Renderer* renderer, const kmMat4 &transform, bool transformUpdated)
{
    
    lineDrawCommand.init(_globalZOrder);
    lineDrawCommand.func = CC_CALLBACK_0(MethodLayer::onDraw, this, transform, transformUpdated);
    Director::getInstance()->getRenderer()->addCommand(&lineDrawCommand);
    
    //LayerColor::draw(renderer, transform, transformUpdated);
}

void MethodLayer::onDraw(const kmMat4 &transform, bool transformUpdated)
{
    //LayerColor::onDraw();
    
    kmMat4 oldMat;
    kmGLGetMatrix(KM_GL_MODELVIEW, &oldMat);
    kmGLLoadMatrix(&_modelViewTransform);
    
    double endX = realWidth + BORDER_RADIUS / 2 ;
    double startY = realHeight - BORDER_RADIUS / 2;
    
    double endY = METHODLAYER_TILE_OFFSET + BORDER_RADIUS / 2 ;
    
    int startX =  - BORDER_RADIUS / 2;
    
    DrawPrimitives::setDrawColor4F(255,255,255,255);
    glLineWidth(1.5f);
    //draw lines
    DrawPrimitives::drawLine(Point(startX + BORDER_RADIUS,startY + BORDER_RADIUS), Point(endX - BORDER_RADIUS ,startY + BORDER_RADIUS));
    DrawPrimitives::drawLine(Point(startX,startY), Point(startX ,endY));
    DrawPrimitives::drawLine(Point(startX + BORDER_RADIUS,endY - BORDER_RADIUS), Point(endX - BORDER_RADIUS ,endY - BORDER_RADIUS));
    DrawPrimitives::drawLine(Point(endX ,endY), Point(endX ,startY));
    
    //draw circles
    ccDrawArc( startX + BORDER_RADIUS,startY , BORDER_RADIUS,CC_DEGREES_TO_RADIANS(90),CC_DEGREES_TO_RADIANS(90), 20);
    ccDrawArc( startX + BORDER_RADIUS,endY , BORDER_RADIUS,CC_DEGREES_TO_RADIANS(180),CC_DEGREES_TO_RADIANS(90), 20);
    ccDrawArc( endX - BORDER_RADIUS,endY, BORDER_RADIUS,CC_DEGREES_TO_RADIANS(270),CC_DEGREES_TO_RADIANS(90), 20);
    ccDrawArc( endX - BORDER_RADIUS,startY, BORDER_RADIUS,CC_DEGREES_TO_RADIANS(0),CC_DEGREES_TO_RADIANS(90), 20);
    
    kmGLLoadMatrix(&oldMat);
}

vector<_instruction> MethodLayer::makeMethod()
{
    int cond  = useCondition ? condition : -1;
    int rep = useRepeater ? repeater : -2;
    
    return RobotMethod::makeMethod(methodID, methods,index,cond, rep,false);
}
#ifdef MAP_EDITOR
void MethodLayer::deleteMethod(cocos2d::Ref* sender)
{
    parentLayer->getScroll()->getContainer()->removeChild(this, true);
    parentLayer->layers.erase(parentLayer->layers.begin()+index);
    
    if (index < parentLayer->layers.size()) {
        for (int i = index; i < parentLayer->layers.size(); ++i) {
            parentLayer->layers[i]->moveUp();
        }
    }
    
    parentLayer->update();
}

void MethodLayer::moveUp ()
{
    switch (index) {
        case 2:
            methodID = CMDA;
            name = "Command 1";
            break;
        case 3:
            methodID = CMDB;
            name = "Command 2";
            break;
    }
    index--;
    
    removeChildByTag(NAME_SPRITE_TAG);
    
    Sprite* nameSprite = nullptr;
    
   if(methodID == CMDA) {
        nameSprite = PMTextureCache::instanse()->getProgramSprite(CMDA, _method::pmExecuteAlways, NORMAL_STATE);
    } else if(methodID == CMDB) {
        nameSprite = PMTextureCache::instanse()->getProgramSprite(CMDB, _method::pmExecuteAlways, NORMAL_STATE);
    } else if(methodID == CMDC) {
        nameSprite = PMTextureCache::instanse()->getProgramSprite(CMDC, _method::pmExecuteAlways, NORMAL_STATE);
    }
    
    if(nameSprite != nullptr)
    {
        addChild(nameSprite , 0, NAME_SPRITE_TAG);
        nameSprite->setPosition( Point(nameLength-nameSprite->getContentSize().width, realHeight + BORDER_RADIUS / 2));
        nameSprite->setAnchorPoint( Point::ZERO );
        nameSprite->setScale(0.8f);
    }
}

void MethodLayer::makeHint (cocos2d::Ref* sender)
{
    //newHint = methods;
}

void MethodLayer::readyForSave ()
{
    if (repeater == -1) {
        repeater = 0;
        useRepeater = false;
    }
    
    if (condition == -1) {
        condition = 0;
        useCondition = false;
    }
    
    if (newHint.size() == methods.size()) {
        
        HintPart part;
        part.type = MethodHint;
        //part.funcHint = newHint;
        
        if(hint->hintParts[0].type == MethodHint)
        {
            hint->hintParts[0] = part;
        }
        else {
            if (hint->hintParts.size() == 1)
            {
                hint->hintParts.push_back(part);
            }
            else hint->hintParts[1] = part;
        }
    }
    
    
    if(hint->hintParts[0].type == TextHint && hint->hintParts[0].hintText == "")
    {
        hint->hintParts.erase(hint->hintParts.begin());
    }
    else {
        if(hint->hintParts.size() == 1 && hint->hintParts[1].type == TextHint && hint->hintParts[1].hintText == "")
        {
            hint->hintParts.erase(hint->hintParts.end());
        }
    }
}

void MethodLayer::showMethodTypeEditor(int index) {
    PMPopoverMethodButton::create(index, width, realHeight, this);
}

void MethodLayer::showRepeaterTypeEditor() {
    PMPopoverRepeaterButton::create(width, realHeight, this);
}

void MethodLayer::showConditionTypeEditor() {
    PMPopoverConditionButton::create(width, realHeight, this);
}

#endif
