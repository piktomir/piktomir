//
//  RobotLooseLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "RobotLooseLayer.h"
#include "GameLayer.h"
#include "MainMenuLayer.h"

USING_NS_CC;

RobotLooseLayer *RobotLooseLayer::create(GameLayer *parent)
{
    RobotLooseLayer *pRet = new RobotLooseLayer(parent);
    
    if(pRet && pRet->init()) {
        pRet->autorelease();
        return pRet;
    } else {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
    
}


bool RobotLooseLayer::init()
{
    Size mapSize = PMSettings::instance()->mapSize();
    
    if ( !initWithColor(Color4B(145,145,145,255 * 0.45f)) )
    {
        return false;
    }
    
    Label *label = Label::create(LocalizedString("RobotBroken"), PMSettings::instance()->getFont(), 42);
    
    label->setAnchorPoint( Point::ZERO );
    label->setPosition( Point::ZERO );
    
    addChild(label, 20);
    
    setContentSize(Size(label->getContentSize().width, label->getContentSize().height));
    setPosition(Point(mapSize.width / 2 - getContentSize().width / 2, -getContentSize().height));
    
    return true;
}

void RobotLooseLayer::mainMenu(cocos2d::Ref *sender)
{
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, MainMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void RobotLooseLayer::nextLevel(cocos2d::Ref *sender)
{
    PMSettings::instance()->incLevel();
    if (PMSettings::instance()->getLevel() == PMSettings::instance()->getMapsCount(parent->getWorldPart()) + 1) {
        PMSettings::instance()->incWorldPart();
        PMSettings::instance()->setLevel(1);
    }
    
    if (PMSettings::instance()->getWorldPart() >= PMSettings::instance()->getWorldsCount()) {
        PMSettings::instance()->setWorldPart(0);
    }
    
    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, GameLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void RobotLooseLayer::selfRemove()
{
    parent->removeChild(this,true);
}

void RobotLooseLayer::show()
{
    Size mapSize = PMSettings::instance()->mapSize();
    
    FiniteTimeAction* moveLayer = MoveTo::create(SYSTEM_ANIMATION_DELAY, Point(mapSize.width / 2 - getContentSize().width / 2, 70));
    
    runAction( moveLayer );
}

void RobotLooseLayer::hide()
{
    retain();
    
    Size mapSize = PMSettings::instance()->mapSize();
    
    FiniteTimeAction* moveLayer = MoveTo::create(SYSTEM_ANIMATION_DELAY,  Point(mapSize.width / 2 - getContentSize().width / 2, -getContentSize().height));
    
    FiniteTimeAction* moveEnd = CallFunc::create(CC_CALLBACK_0(RobotLooseLayer::selfRemove, this));
    
    runAction( Sequence::create(moveLayer, moveEnd, nullptr) );
}