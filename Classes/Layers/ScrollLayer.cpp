//
//  ScrollLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 10.04.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "ScrollLayer.h"
#include "GameLayer.h"

#ifdef MAP_EDITOR
#include "MapEditorGameLayer.h"
#endif

USING_NS_CC;


bool ScrollLayer::init()
{
    parallax = ParallaxNode::create();
    parallax->setPosition( Point::ZERO );
    
    Size mapSize = PMSettings::instance()->getScreenSize();
    
    Sprite *backGround = nullptr;
    
    if(!previewDraw)
        backGround = Sprite::create(PICT_PATH "Background/background2.png");
    
    //backGround->setPosition( Point(mapSize.width /2 , mapSize.height / 2) );
    //backGround->setAnchorPoint( Point::ZERO );
    
    //resizeSprite(backGround, mapSize.width , mapSize.height);
    if(backGround != nullptr)
    {
        backGround->setTag(0);
        parallax->addChild(backGround, -1, Point(0.4f,0.5f), Point(mapSize.width /2 , mapSize.height / 2));
    }
    
    addChild(parallax);
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(ScrollLayer::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(ScrollLayer::touchEnded, this);
    listener->onTouchMoved = CC_CALLBACK_2(ScrollLayer::touchMoved, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

bool ScrollLayer::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(!active)
        return false;
    
    int activeLayer = layer->getActiveLayer();
    Point touchPoint = this->convertTouchToNodeSpace(touch);
    
    if(boundingBox().containsPoint(touchPoint))
    {                
        Point mapPoint = layer->getLayer(activeLayer)->getMapLayer()->convertTouchToNodeSpace(touch);
        Size  mapSize = layer->getLayer(activeLayer)->getMapLayer()->getContentSize();
        
#ifdef MAP_EDITOR
        
        for (int i = 0; i < layer->getWorld()->robotCount(); i++) {
            if (layer->getWorld()->robotAt(i)->type() == pmRobot4) {
                if (((Robot4*)layer->getWorld()->robotAt(i))->boundingBox().containsPoint(mapPoint))
                {
                    dragedRobot = i;
                    touchType = DragRobot;
                    return true;
                }
            }
        }
        
        if ( ((Map4*)MapEditorGameLayer::getInstanse()->getWorld()->getMap())->resizeSprite->boundingBox().containsPoint(mapPoint)) {
            touchType = ResizeMap;
            startPoint = mapPoint;
            return true;
        }
#endif
        
        if( mapPoint.x >= -((Map4*)layer)->mapElementSize.width / 2 && mapPoint.x <= mapSize.width &&  mapPoint.y >= 0 && mapPoint.y <= mapSize.height  + ((Map4*)layer)->mapElementSize.height / 2)
        {
            startLayerPosition =parallax->getPosition();
            startPoint = touchPoint;
            startSize = getContentSize();
            touchType = DragLayer;
#ifdef MAP_EDITOR
            if(MapEditorGameLayer::getInstanse()->editing) {
                touchType = EditLayer;
                if(layer->type() == pmMap4) {
                    ((Map4*)layer)->changeElementTypeAtPoint(mapPoint, activeLayer);
                }
            }
#endif
            return true;
        }
        else if(layer->getLayerCount() != 1)
        {
            double centerX = PMSettings::instance()->mapSize().width/2;
            double centerY = PMSettings::instance()->mapSize().height/2;
        
            layer->scrollToInitialPosition();
            
            if( touchPoint.y <= LAYER_POSITION_Y(touchPoint.x + SCROLLLAYER_TOUCH_OFFSET, centerX , centerY) &&
               touchPoint.y >= LAYER_POSITION_Y(touchPoint.x - SCROLLLAYER_TOUCH_OFFSET, centerX , centerY) )
            {
                startPoint  = touchPoint;
                touchType = ScrollLayers;
                return true;
            }
        }
    }
    return false;
}


void ScrollLayer::touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    int activeLayer = layer->getActiveLayer();
    Point touchPoint = this->convertTouchToNodeSpace(touch);
   
    // set the new sprite position
    if(touchType == DragLayer)
    {
        Point diff = touch->getDelta();
        
        Point currentPos = parallax->getPosition();
        parallax->setPosition( currentPos + diff );
        
        startPoint = touchPoint;
        
        return;
    }
#ifdef MAP_EDITOR
    if(touchType == EditLayer) {
        Point touch_ = layer->getLayer(activeLayer)->getMapLayer()->convertTouchToNodeSpace(touch);
        
        if(layer->type() == pmMap4) {
            ((Map4*)layer)->changeElementTypeAtPoint(touch_, activeLayer);
        }
        return;
    }
    
    if (touchType == DragRobot) {
        Point touch_ = layer->getLayer(activeLayer)->getMapLayer()->convertTouchToNodeSpace(touch);
        ((Robot4*)layer->getWorld()->robotAt(dragedRobot))->setPosition(((Map4*)layer)->coordAtPoint(touch_), ((Robot4*)layer->getWorld()->robotAt(dragedRobot))->getCurLayer(), ((Robot4*)layer->getWorld()->robotAt(dragedRobot))->getCurDirection());
        ((Map4*)layer)->update();
        return;
    }
    
    if (touchType == ResizeMap) {
        Point touch_ = layer->getLayer(activeLayer)->getMapLayer()->convertTouchToNodeSpace(touch);
        int tmp = ((Map4*)layer)->resizeAtTouch(((Map4*)layer)->dxdyAtPoint((touch_- startPoint)));
        if(tmp == 0) {
            startPoint = touch_;
        }
        if(tmp == 1) {
            startPoint.x = touch_.x;
        }
        if(tmp == 2) {
            startPoint.y = touch_.y;
        }
        
        return;
    }
    
#endif
    if(touchType == ScrollLayers)
    {
        double centerX = PMSettings::instance()->mapSize().width/2;
        double centerY = PMSettings::instance()->mapSize().height/2;
        
        Point touchPoint = this->convertTouchToNodeSpace(touch);
        double dx = startPoint.x - touchPoint.x;
        
        double x,y;
        
        for(int i = 0; i < layer->getLayerCount(); ++i)
        {
            double curLyaerX = LAYER_POSITION_X(fabs((float)activeLayer - i));
            double prevLyaerX = (dx > 0.0) ? LAYER_POSITION_X(fabs((float)activeLayer - i + 1)) : LAYER_POSITION_X(fabs((float)activeLayer - i - 1));
            x = layer->getLayer(i)->getPositionX() - dx * fabs(curLyaerX - prevLyaerX) / LAYER_POSITION_X(1);
            
            y = LAYER_POSITION_Y(x, centerX , centerY);
            
            layer->getLayer(i)->setPosition( Point(x,y) );
            
            int opacity = LAYER_OPACITY(x - centerX);
            if(opacity > 255)
                opacity = 255;

            layer->setLayerOpacity(i, opacity);
            layer->getLayer(i)->getMapLayer()->setScale(SCALE_FUNCTION(x - centerX));
            
            if(fabs(x - centerX - LAYER_POSITION_X(0)) <= SCROLLLAYER_ACTIVELAYER_DELTA)
            {
                layer->setActiveLayer(i);
                activeLayer = i;
                
                CCLOG("ScrollLayer - set Active Layer %d",i);
            }
            
        }
        
        startPoint = touchPoint;
    }

}

void ScrollLayer::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    int activeLayer = layer->getActiveLayer();
    
    double centerX = PMSettings::instance()->mapSize().width/2;
    double centerY = PMSettings::instance()->mapSize().height/2;
    
    
    if(touchType == DragLayer)
    {
#ifdef MAP_EDITOR
        if (fabs(touch->getStartLocation().x - touch->getLocation().x) < 20 && fabs(touch->getStartLocation().y - touch->getLocation().y) < 20) {
            Point touch_ = layer->getLayer(activeLayer)->getMapLayer()->convertTouchToNodeSpace(touch);
            
//            if(layer->type() == pmMap4) {
//                ((Map4*)layer)->changeElementTypeAtPoint(touch_, activeLayer);
//            }
            
            Point point = ((Map4*)layer)->xyAtTouch(touch_, activeLayer);
            Point point2 = ((Map4*)layer)->xyAtTouch(touch_+Point(9, 12), activeLayer);
            if (point2.x>point.x) {
                point.x=point2.x;
            }
            if (point2.y<point.y) {
                point.y=point2.y;
            }
            
            Point realPoint = (((Map4*)layer)->Coordinates(point.x,point.y,0,0)-Point(39,42));
            
            if ( fabs(realPoint.x - touch_.x) < 20 ) {
                if ( fabs(realPoint.y - touch_.y) < 20 ) {
                    ((Map4*)layer)->switchLeftWall(point+Point(0,1), activeLayer);
                    ((Map4*)layer)->switchUpWall(point, activeLayer);
                } else {
                    ((Map4*)layer)->switchLeftWall(point, activeLayer);
                }
            } else if (fabs(realPoint.y - touch_.y) < 20) {
                ((Map4*)layer)->switchUpWall(point, activeLayer);
            } else {
                ((Map4*)layer)->changeElementTypeAtPoint(touch_, activeLayer);
            }
        }
#endif
        setContentSize(startSize);
        
        Point movePoint;
        
        Layer *mapLayer = layer->getLayer(activeLayer)->getMapLayer();
        
        Size mapSize = mapLayer->boundingBox().size;
        mapSize.height  /= 2;
        
        Point minPoint = mapLayer->boundingBox().origin;
        
        minPoint = layer->getLayer(activeLayer)->convertToWorldSpaceAR(minPoint);
        Point maxPoint = Point(minPoint.x + mapSize.width,minPoint.y + mapSize.height);
        
        
        if(mapSize.width > getContentSize().width)
        {
            if(minPoint.x > 0)
                movePoint.x = mapLayer->boundingBox().size.width/2 - PMSettings::instance()->mapSize().width/2;
            else if(maxPoint.x < PMSettings::instance()->mapSize().width)
                 movePoint.x = PMSettings::instance()->mapSize().width/2 - mapLayer->boundingBox().size.width/2;
            else
                movePoint.x = parallax->getPositionX();
        }
        else
            movePoint.x = startLayerPosition.x;
        
        if(mapSize.height > getContentSize().height)
        {
            
            if(minPoint.y > 0)
                movePoint.y = mapLayer->boundingBox().size.height/2;
            else if(maxPoint.y < PMSettings::instance()->mapSize().height)
                movePoint.y = PMSettings::instance()->mapSize().height - mapLayer->boundingBox().size.height/2;
            else
                movePoint.x = parallax->getPositionX();
        }
        else
            movePoint.y = startLayerPosition.y;
        
        FiniteTimeAction* moveLayer = MoveTo::create(SYSTEM_ANIMATION_DELAY / 2,
                                                         movePoint);
            
        parallax->runAction(moveLayer);
        
        return;
    }
    
    if(touchType == ScrollLayers)
    {
        
        double min = PMSettings::instance()->mapSize().width;

        for(int i = 0; i < layer->getLayerCount(); ++i)
        {
            if(fabs( layer->getLayer(i)->getPositionX() - centerX - LAYER_POSITION_X(0) ) < min)
            {
                layer->setActiveLayer(i);
                
                min = fabs( layer->getLayer(i)->getPositionX() - centerX - LAYER_POSITION_X(0));
            }
        }
        
        activeLayer = layer->getActiveLayer();
        
        for(int i = 0; i < layer->getLayerCount(); ++i)
        {
            double x = ( i < activeLayer ) ? -(LAYER_POSITION_X( fabs((float)activeLayer - i) )) : LAYER_POSITION_X( fabs((float)activeLayer - i) );
            double y = LAYER_POSITION_Y(x + centerX, centerX, centerY);
            
            FiniteTimeAction* moveLayer = MoveTo::create(SYSTEM_ANIMATION_DELAY / 2,
                                                              Point(x+centerX,y));
            FiniteTimeAction* scaleLayer = ScaleTo::create(SYSTEM_ANIMATION_DELAY / 2,
                                                             SCALE_FUNCTION(x));
           // FiniteTimeAction* fadeLayer = FadeTo::create(PMSettings::instance()->getAnimationSpeed() / 2,
                                                     //          LAYER_OPACITY(x));
           // Spawn *fadeAndScaleAction = Spawn::createWithTwoActions(fadeLayer, scaleLayer);
            
            layer->getLayer(i)->getMapLayer()->runAction(scaleLayer /*Spawn::createWithTwoActions(moveLayer, scaleLayer)*/);
            layer->getLayer(i)->runAction(moveLayer);
            layer->setLayerOpacity(i, LAYER_OPACITY(x));
        }
    }

}
