//
//  PauseLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "PauseLayer.h"
#include "GameLayer.h"
#include "MainMenuLayer.h"
#include "ScrollLayer.h"

USING_NS_CC;

PauseLayer *PauseLayer::create(GameLayer *parent,bool isRobotLoose)
{
    PauseLayer *pRet = new PauseLayer(parent,isRobotLoose);
    
    if(pRet && pRet->init()) {
        pRet->autorelease();
        return pRet;
    } else {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
    
}

bool PauseLayer::init()
{
    Size screenSize = PMSettings::instance()->getScreenSize();
    
    if ( !initWithColor(Color4B(145,145,145,255), screenSize.width, screenSize.height) )
    {
        return false;
    }
    
    setOpacity( 255 * 0.45f );
    
    MenuItemFont *pauseItem2 = MenuItemFont::create(LocalizedString("MainMenu"),
                                                    CC_CALLBACK_1(PauseLayer::mainMenu, this));
    MenuItemFont *pauseItem3  = nullptr;
    
#if(CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 )
    pauseItem3 = MenuItemFont::create(LocalizedString("Exit"),
                                      CC_CALLBACK_1(PauseLayer::exitGame, this));
    
    pauseItem3->setPosition(screenSize.width/2, screenSize.height/2);
#endif
    
    Menu *pauseMenu = Menu::create(pauseItem2, pauseItem3, nullptr);
    
    addChild(pauseMenu);
    
    if(!isRobotLoose)
    {
        MenuItemFont *pauseItem1 = MenuItemFont::create(LocalizedString("ContinueLevel"),
                                                        CC_CALLBACK_1(GameLayer::continueLevel, parent));
        
        pauseItem1->setPosition(screenSize.width/2, screenSize.height/2 + 100);
        pauseMenu->addChild(pauseItem1);
    }
    
    pauseMenu->setPosition( Point::ZERO );
    pauseItem2->setPosition(screenSize.width/2, screenSize.height/2 + 50);
    
    setPosition(Point(0, screenSize.height));
    
    return true;
}

void PauseLayer::mainMenu(cocos2d::Ref *sender)
{
    PointObject *object = (PointObject *)parent->getWorld()->getMap()->getScroll()->getParallax()->getParallaxArray()->arr[0];
    
    Sprite *background = (Sprite *)object->getChild();
    
    object->setRatio(Point(1.0,1.0));
    object->setOffset( Point(background->getContentSize().width / 2, background->getContentSize().height / 2) );
    
    TransitionSlideInL* trans = TransitionSlideInL::create( 1.5f * SYSTEM_ANIMATION_DELAY, MainMenuLayer::scene());
    Director::getInstance()->replaceScene(trans);
}

void PauseLayer::nextLevel(cocos2d::Ref *sender)
{
    PMSettings::instance()->incLevel();
    if (PMSettings::instance()->getLevel() == PMSettings::instance()->getMapsCount(parent->getWorldPart()) + 1) {
        PMSettings::instance()->incWorldPart();
        PMSettings::instance()->setLevel(1);
    }
    
    if (PMSettings::instance()->getWorldPart() >= PMSettings::instance()->getWorldsCount()) {
        PMSettings::instance()->setWorldPart(0);
    }
    
    TransitionFade* trans = TransitionFade::create( 1.5f * SYSTEM_ANIMATION_DELAY, GameLayer::scene());
    Director::getInstance()->replaceScene(trans);
}


void PauseLayer::exitGame(cocos2d::Ref *sender)
{
    removeAllChildrenWithCleanup(true);
    exit(0);
}

void PauseLayer::selfRemove()
{
    parent->removeChild(this,true);
}

void PauseLayer::show()
{
    
    FiniteTimeAction* moveLayer = MoveTo::create(SYSTEM_ANIMATION_DELAY, Point::ZERO);
    
    runAction( moveLayer );
}

void PauseLayer::hide()
{
    retain();
    
    Size screenSize = PMSettings::instance()->getScreenSize();
    
    FiniteTimeAction* moveLayer = MoveTo::create(SYSTEM_ANIMATION_DELAY, Point(0, screenSize.height));
    
    FiniteTimeAction* moveEnd = CallFunc::create(CC_CALLBACK_0(PauseLayer::selfRemove, this));
    
    runAction( Sequence::create(moveLayer, moveEnd, nullptr) );
}