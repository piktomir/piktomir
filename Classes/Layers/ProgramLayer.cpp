//
//  ProgramLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 30.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#include "ProgramLayer.h"
#include "GameLayer.h"
#include "PMSettings.h"
#include "cocos-ext.h"
#include "MethodLayer.h"

#ifdef MAP_EDITOR
#include "MapEditorGameLayer.h"
#endif

USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

bool ProgramLayer::init()
{
    if ( !LayerColor::init() )
    {
        return false;
    }

    
    activeButton = nullptr;
    draggedSprite = nullptr;
    
    scroll  = ScrollView::create();
    scroll->setDirection(ScrollView::Direction::VERTICAL);
    scroll->setZoomScale(0.5);
    scroll->setClippingToBounds(true);
    scroll->setBounceable(false);
    
    initWithColor(Color4B(100,100,100,150), PMSettings::instance()->getProgramLayerWidth() , PMSettings::instance()->getScreenSize().height);
    
    setPosition( Point(PMSettings::instance()->getScreenSize().width - PMSettings::instance()->getProgramLayerWidth(), 0) );
    
    float lastY = PMSettings::instance()->getScreenSize().height - PROGRAMLAYER_BORDER_Y;
    
    
    
    labelWorld = Label::create("1", PMSettings::instance()->getFont(), 20);
    labelWorld->setAnchorPoint( Point::ZERO );
    labelWorld->setPosition( Point(20,
                                 PMSettings::instance()->getScreenSize().height - 10 - labelWorld->getContentSize().height) );
    
    addChild(labelWorld,2);
    
    worldLabelHeight = labelWorld->getContentSize().height;
    
    lastY -= worldLabelHeight;
    
    actionMenu = Menu::create();
    
    actionMenu->setPosition( Point(PROGRAMLAYER_BORDER_X, lastY) );
    addChild(actionMenu, PL_ACTIONMENU_ZORDER);
    
    addChild(scroll,PL_SCROLL_ZORDER);

    reset();
    
    Layer *touchOverlay = Layer::create();
    touchOverlay->setContentSize(getContentSize());
    addChild(touchOverlay, PL_TOUCHOVERLAY_ZORDER);
    
    listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(ProgramLayer::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(ProgramLayer::touchEnded, this);
    listener->onTouchMoved = CC_CALLBACK_2(ProgramLayer::touchMoved, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, touchOverlay);
    //_eventDispatcher->addEventListenerWithFixedPriority(listener, -159);
    
    return true;
}

void ProgramLayer::setWorldAndLevel()
{
    string worldLabel = StringUtils::format(LocalizedString("WorldAndLevelPattern").c_str(),
                                            parentLayer->getWorldPart()+1,
                                            parentLayer->getLevel(),
                                            PMSettings::instance()->getMapsCount(parentLayer->getWorldPart()));
    
    labelWorld->setString(worldLabel);
}

void ProgramLayer::drawLayers(AbstractWorld *world)
{
    bool useRepeater = false;
    bool useCondition = false;
    
    for(int i = 0; i < layers.size(); ++i)
    {
        if(layers[i]->getUseRepeater())
        {
            useRepeater = true;
        }
        
        if(layers[i]->getUseCondition())
        {
            useCondition = true;
        }
    }
    
    float lastActionY = 0.0;
    
    if(useRepeater)
        lastActionY += BUTTON_TILE_SIZE + PROGAMLAYER_MENU_SEPARATOR;
    
    if(useCondition)
        lastActionY += BUTTON_TILE_SIZE + PROGAMLAYER_MENU_SEPARATOR;
    
    float height = lastActionY + BUTTON_TILE_SIZE + PROGAMLAYER_MENU_SEPARATOR;
    
    actionMenu->setContentSize( Size(PMSettings::instance()->getProgramLayerWidth(),height) );
    actionMenu->setPosition( Point(PROGRAMLAYER_BORDER_X, PMSettings::instance()->getScreenSize().height - PROGRAMLAYER_BORDER_Y - height) );
    actionMenu->setAnchorPoint( Point::ZERO );
    
    for(int i = 1; i < world->getBaseRobot()->getNativeMethodCount(); ++i)
    {
        Sprite *normalImage = PMTextureCache::instanse()->getRobotMethodSprite(i,_method::pmExecuteOnTrue, NORMAL_STATE);
        Sprite *selectedImage = PMTextureCache::instanse()->getRobotMethodSprite(i,_method::pmExecuteOnTrue, SELECTED_STATE);
        Sprite *disabledImage = PMTextureCache::instanse()->getRobotMethodSprite(i,_method::pmExecuteOnTrue, DISABLED_STATE);
        
        SelectButton *button =  new SelectButton(SelectButton::Method, i, normalImage,selectedImage, disabledImage,
                                                 CC_CALLBACK_1(ProgramLayer::selectAction, this));
        
        button->setPosition( Point((i-1) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET / 2), lastActionY) );
        button->setAnchorPoint( Point::ZERO );
        actionMenu->addChild(button);
        
    }
    
    for(int i = 1; i < layers.size(); ++i)
    {
        Sprite *normalImage = PMTextureCache::instanse()->getProgramSprite(CMDA - i + 1,_method::pmExecuteOnTrue, NORMAL_STATE);
        Sprite *selectedImage = PMTextureCache::instanse()->getProgramSprite(CMDA - i + 1,_method::pmExecuteOnTrue, SELECTED_STATE);
        Sprite *disabledImage = PMTextureCache::instanse()->getProgramSprite(CMDA - i + 1,_method::pmExecuteOnTrue, DISABLED_STATE);
        
        SelectButton *button = new SelectButton(SelectButton::Method ,CMDA - i + 1, normalImage, selectedImage, disabledImage,
                                                CC_CALLBACK_1(ProgramLayer::selectAction,this));
        
        
        button->setPosition(Point((i - 2 + world->getBaseRobot()->getNativeMethodCount()) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET / 2), lastActionY));
        button->setAnchorPoint( Point::ZERO );
        
        actionMenu->addChild(button);
    }
    
#ifdef MAP_EDITOR
    map4EditorsMenu * menu = map4EditorsMenu::getInstanse();
    menu->getEditMenu()->setPosition( Point(PROGRAMLAYER_BORDER_X+25, PMSettings::instance()->getScreenSize().height - PROGRAMLAYER_BORDER_Y - height + 80) );
    menu->getEditMenu()->setAnchorPoint(Point::ZERO);
    
    menu->getEditMenu()->removeFromParent();
    
    addChild(menu->getEditMenu(),10);
    
    lastActionY-=120;
#endif
    
    // controls
#ifndef MAP_EDITOR

    if(useRepeater)
    {
        lastActionY -= PROGAMLAYER_MENU_SEPARATOR + BUTTON_TILE_SIZE;
        
        for(int i = 2; i < REPEATER_BUTTON_NUM; ++i)
        {
            Sprite *normalImage;
            Sprite *selectedImage;
            Sprite *disabledImage;
            
            SelectButton *button;
            
            if(i < 7)
            {
                normalImage = PMTextureCache::instanse()->getRepeaterSprite(i, NORMAL_STATE);
                selectedImage = PMTextureCache::instanse()->getRepeaterSprite(i, SELECTED_STATE);
                disabledImage = PMTextureCache::instanse()->getRepeaterSprite(i, DISABLED_STATE);

                button = new SelectButton(SelectButton::Repeater, i, normalImage, selectedImage, disabledImage,
                                        CC_CALLBACK_1(ProgramLayer::selectAction,this));
            }
            else
            {
                normalImage = PMTextureCache::instanse()->getRepeaterSprite(REPEAT_FOREVER, NORMAL_STATE);
                selectedImage = PMTextureCache::instanse()->getRepeaterSprite(REPEAT_FOREVER, SELECTED_STATE);
                disabledImage = PMTextureCache::instanse()->getRepeaterSprite(REPEAT_FOREVER, DISABLED_STATE);
                
                button = new SelectButton(SelectButton::Repeater, REPEAT_FOREVER, normalImage, selectedImage, disabledImage,
                                          CC_CALLBACK_1(ProgramLayer::selectAction,this));
                
            }
            button->setAnchorPoint( Point::ZERO );
            button->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET / 2) * (i-2),lastActionY) );
            actionMenu->addChild(button);
        }
        
        ChangeableRepeaterButton *chRepeatButton = ChangeableRepeaterButton::create(CC_CALLBACK_1(ProgramLayer::selectAction, this));
        
        chRepeatButton->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET / 2) * (REPEATER_BUTTON_NUM - 2),lastActionY) );
        chRepeatButton->setAnchorPoint( Point::ZERO );
        actionMenu->addChild(chRepeatButton);
        

    }
    

    if(useCondition)
    {
        lastActionY -= PROGAMLAYER_MENU_SEPARATOR + BUTTON_TILE_SIZE;
        
        for(int i = 1; i < world->getBaseRobot()->getConditionsCount() + 1; ++i)
        {
            Sprite *normalImage = PMTextureCache::instanse()->getRobotConditionSprite(i, NORMAL_STATE);
            Sprite *selectedImage = PMTextureCache::instanse()->getRobotConditionSprite(i, SELECTED_STATE);
            Sprite *disabledImage = PMTextureCache::instanse()->getRobotConditionSprite(i, DISABLED_STATE);
            
            SelectButton *button = new SelectButton(SelectButton::Condition, i , normalImage, selectedImage, disabledImage,
                                                    CC_CALLBACK_1(ProgramLayer::selectAction,this));
            
            button->setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET / 2) * (i - 1),lastActionY) );
            button->setAnchorPoint( Point::ZERO );
            actionMenu->addChild(button);
        }
    }

  
#else
MenuItemLabel * addFunction = MenuItemLabel::create(Label::create(LocalizedString("AddSubProgram"), PMSettings::instance()->getFont(), 22), CC_CALLBACK_1(ProgramLayer::addMethod, this));
    addFunction->setAnchorPoint(Point::ZERO);
    addFunction->setPosition(Point::ZERO);
    addFunctionMenu = Menu::create(addFunction, nullptr);
    addFunctionMenu->setAnchorPoint(Point::ZERO);
    scroll->addChild(addFunctionMenu, 10);
    
#endif
    
    for(int i = 0; i < layers.size(); ++i)
        scroll->addChild(layers[i]);
    
    update();


    
    actionMenu->getEventDispatcher()->pauseEventListenersForTarget(actionMenu, true);
}

void ProgramLayer::update()
{
    float lastY = PMSettings::instance()->getScreenSize().height - PROGRAMLAYER_BORDER_Y - worldLabelHeight;
    
#ifndef MAP_EDITOR
    for(int i = 0; i < layers.size(); ++i)
    {
        if(layers[i]->getUseRepeater())
        {
            lastY -= PROGAMLAYER_MENU_SEPARATOR + BUTTON_TILE_SIZE;
            break;
        }
    }
    //Conditions
    for(int i = 0; i < layers.size(); ++i)
    {
        if(layers[i]->getUseCondition())
        {
            lastY -= PROGAMLAYER_MENU_SEPARATOR + BUTTON_TILE_SIZE;
            break;
        }
    }
    
#else
    lastY = PMSettings::instance()->getScreenSize().height - PROGRAMLAYER_BORDER_Y - worldLabelHeight;
    lastY -= 120;
#endif
    
    lastY -= 4 * PROGRAMLAYER_PROGRAM_SEPARATOR;
    
    float viewHeight = 0;
    float viewWidth = 0;
    
    float scrollLastY = 0;
    
    for (int i = 0; i < layers.size(); ++i)
    {
        viewHeight += layers[i]->getContentSize().height + PROGRAMLAYER_PROGRAM_SEPARATOR;
        viewWidth = MAX(layers[i]->getContentSize().width, viewWidth);
    }
    
    scroll->setViewSize( Size(PMSettings::instance()->getProgramLayerWidth(),lastY) );
    
    scrollLastY = viewHeight;
    
    for (int i = 0; i < layers.size(); ++i)
    {
        layers[i]->setPosition( Point(PROGRAMLAYER_BORDER_X + BORDER_RADIUS/2,scrollLastY - layers[i]->getContentSize().height ) );
        lastY -= layers[i]->getContentSize().height + PROGRAMLAYER_PROGRAM_SEPARATOR;
        scrollLastY -= layers[i]->getContentSize().height + PROGRAMLAYER_PROGRAM_SEPARATOR;
        
    }
    
#ifdef MAP_EDITOR
    if(addFunctionMenu)addFunctionMenu->setPosition(Point(PROGRAMLAYER_BORDER_X + BORDER_RADIUS/2,scrollLastY - 30 ));
#endif

    scroll->setPosition( Point(0, 0) );
    scroll->setContentSize( Size(PMSettings::instance()->getProgramLayerWidth(),viewHeight ) );
    scroll->setContentOffset(Point(0,lastY),false);
}

SelectButton *ProgramLayer::findMenuButtonAtTouch(Touch *touch)
{
    for(int j = 0; j < actionMenu->getChildrenCount(); ++j)
    {
        SelectButton *button  = (SelectButton*)actionMenu->getChildren().at(j);

        if(button->boundingBox().containsPoint(actionMenu->convertTouchToNodeSpace(touch)))
        {
            return button;
        }
        
    }
    
    return nullptr;
}

int ProgramLayer::layerAtTouch(cocos2d::Touch *touch)
{
    for(int i = 0; i < layers.size(); ++i) {
        if(layers[i]->containsTouch(touch)) {
            return i;
        }
    }
    return -1;
}


bool ProgramLayer::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(!enabled)
        return false;
    
    if(!touch)
        return false;
    
    touchType = plNone;
    draggedSprite = nullptr;
    
    if((selectedButton = findMenuButtonAtTouch(touch)))
    {
        touchType = plSelectMenuItem;
        selectedButton->selected();
    }
    else
    {
        selectedLayer = layerAtTouch(touch);
        
        if(selectedLayer != -1)
        {
            selectedMethod = layers[selectedLayer]->methodAtTouch(touch);
            
            if(selectedMethod != -1)
            {
                selectedMethodLayerItem = layers[selectedLayer]->getMethods()[selectedMethod].methodID;
                touchType = mlSelectMethod;
                layers[selectedLayer]->highlightMethod(selectedMethod);
            }
            else if(layers[selectedLayer]->conditionAtTouch(touch))
            {
                selectedMethodLayerItem = layers[selectedLayer]->getCondition();
                touchType = mlSelectCondition;
            }
            else if(layers[selectedLayer]->repeaterAtTouch(touch))
            {
                selectedMethodLayerItem = layers[selectedLayer]->getRepeater();
                touchType = mlSelectRepeater;
            }
        }
    }
    
    if(touchType == plNone)
        return false;

    
    return true;
}

bool ProgramLayer::isDragging()
{
    return touchType==plDragMenuItem || touchType==mlDragMethod || touchType==mlDragCondition || touchType==mlDragRepeater;
}

bool ProgramLayer::isSelecting()
{
    return touchType==plSelectMenuItem || touchType==mlSelectMethod ||
            touchType==mlSelectCondition || touchType==mlSelectRepeater;
}

void ProgramLayer::touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(controlledMode)
        return;
    
    if(!touch)
        return;
    
    
    if(isSelecting() && touch->getStartLocation().getDistance(touch->getLocation()) > TOUCH_CRITICAL_DISTANCE)
    {
        if(touchType == plSelectMenuItem)
        {
            touchType = plDragMenuItem;
            draggedSprite = selectedButton->cloneSprite();
            
            if(isAnimationRunnnig)
            {
                isAnimationRunnnig = false;
                activeButton->stopAnimation();
                activeButton = nullptr;
            }
            
        }
        else if(touchType == mlSelectMethod && layers[selectedLayer]->isMethodDraggable(selectedMethod))
        {
            touchType = mlDragMethod;
            draggedSprite = layers[selectedLayer]->cloneMethodSprite(selectedMethod);
        }
        else if(touchType == mlSelectCondition && layers[selectedLayer]->isConditionDraggable())
        {
            touchType = mlDragCondition;
            draggedSprite = layers[selectedLayer]->cloneConditionSprite();
        }
        else if(touchType == mlSelectRepeater && layers[selectedLayer]->isRepeaterDraggable())
        {
            touchType = mlDragRepeater;
            draggedSprite = layers[selectedLayer]->cloneRepeaterSprite();
        }
        else
        {
            touchType = plCancelTouch;
        }
        
        if(draggedSprite != nullptr)
        {
            addChild(draggedSprite, 20);
            draggedSprite->setPosition(convertTouchToNodeSpace(touch));
        }
    }
    else if(isDragging())
    {
        draggedSprite->setPosition(convertTouchToNodeSpace(touch));
    }

}

void ProgramLayer::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(!touch)
        return;
    
    if(controlledMode && isDragging())
        return;
    
    if(touchType == plCancelTouch)
    {
        if(selectedLayer != -1 && selectedMethod != -1)
            layers[selectedLayer]->clearHighlightMethod(selectedMethod);
        
        return;
    }
    
    if(isSelecting())
    {
        if(touchType == plSelectMenuItem)
        {
            if(controlledMode)
            {
                parentLayer->getMethodStackLayer()->addMethod(selectedButton);
                selectedButton->jumpOnce();
            }
            else
                selectAction(selectedButton);
            
            selectedButton->unselected();
        }
        else
        {
            if(touchType == mlSelectMethod)
            {
                layers[selectedLayer]->clearHighlightMethod(selectedMethod);
            }
            layers[selectedLayer]->setButton(activeButton, touch);
#ifdef MAP_EDITOR
            if(touchType == mlSelectMethod)
                if (touch->getStartLocation().getDistance(touch->getLocation()) < 5) {
                    layers[selectedLayer]->showMethodTypeEditor(selectedMethod);
                    setEnabled(false);
                }
            if(touchType == mlSelectRepeater)
                if (touch->getStartLocation().getDistance( touch->getLocation()) < 5) {
                    layers[selectedLayer]->showRepeaterTypeEditor();
                    setEnabled(false);
                }
            if(touchType == mlSelectCondition)
                if (touch->getStartLocation().getDistance( touch->getLocation()) < 5) {
                    layers[selectedLayer]->showConditionTypeEditor();
                    setEnabled(false);
                }
#endif
        }
        
    }
    else
    {
        int layer = layerAtTouch(touch);
        
        if(layer == -1)
        {
            if(touchType == mlDragMethod)
                layers[selectedLayer]->clearHighlightMethod(selectedMethod);
            
            if(touchType == plDragMenuItem)
                selectedButton->unselected();
            
            switch (touchType)
            {
                case plDragMenuItem:
                    selectedButton->unselected();
                    break;
                case mlDragMethod:
                {
                    layers[selectedLayer]->clearHighlightMethod(selectedMethod);
                    layers[selectedLayer]->addAction(selectedMethod, EMPTY_METHOD);
                } break;
                case mlDragCondition:
                    layers[selectedLayer]->addCondition(EMPTY_CONDITION);
                    break;
                case mlDragRepeater:
                    layers[selectedLayer]->addRepeater(1);
                    break;
                default:
                    break;
            }

            
            removeChild(draggedSprite);
            draggedSprite = nullptr;
            
            return;
        }
        
        switch (touchType)
        {
            case plDragMenuItem:
                layers[layer]->setButton(selectedButton, touch);
                selectedButton->unselected();
                break;
            case mlDragMethod:
            {
                layers[selectedLayer]->clearHighlightMethod(selectedMethod);
                
                int method = layers[layer]->methodAtTouch(touch);
                
                if(method != -1)
                    layers[layer]->addAction(method, selectedMethodLayerItem, true);
                else
                    layers[selectedLayer]->addAction(selectedMethod, EMPTY_METHOD);
            } break;
            case mlDragCondition:
                layers[layer]->addCondition(selectedMethodLayerItem);
                break;
            case mlDragRepeater:
                layers[layer]->addRepeater(selectedMethodLayerItem);
                break;
            default:
                break;
        }

        removeChild(draggedSprite);
    }
    
}

void ProgramLayer::selectAction(cocos2d::Ref *sender)
{
    SelectButton *senderButton = (SelectButton *)sender;
    
    if(isAnimationRunnnig)
    {
        isAnimationRunnnig = false;
        activeButton->stopAnimation();
    }
    
    if(senderButton->isMethod())
    {
        if(activeButton == nullptr || activeButton->getMethodID() != senderButton->getMethodID())
        {
            activeButton = senderButton;
            
            isAnimationRunnnig = true;
            activeButton->startAnimation();
            
        }
        else if(!controlledMode)
        {
            activeButton = nullptr;
        }
    }
    else if(senderButton->isCondition() && !controlledMode)
    {
        if(activeButton == nullptr || activeButton->getCondition() != senderButton->getCondition())
        {
            activeButton = senderButton;

            isAnimationRunnnig = true;
            activeButton->startAnimation();
            
        }
        else if(!controlledMode)
        {
            activeButton = nullptr;
        }
        
    }
    else if(senderButton->isRepeater())
    {
        if(activeButton == nullptr || activeButton->getRepeater() != senderButton->getRepeater())
        {

            activeButton = senderButton;
            
            if(!controlledMode)
            {
                isAnimationRunnnig = true;
                activeButton->startAnimation();
            }
            
        }
        else if(!controlledMode)
        {
            activeButton = nullptr;
        }
        
    }
}

void ProgramLayer::setControlledMode(bool flag)
{
    controlledMode = flag;
    
    for(int i = 0; i < layers.size(); ++i)
    {
        layers[i]->setControlledMode(flag);
    }
}

void ProgramLayer::setEnabled(bool state)
{
    enabled = state;
    
    for(int i = 0; i< actionMenu->getChildrenCount(); ++i)
    {
        ((MenuItem *)actionMenu->getChildren().at(i))->setEnabled(state);
    }
    
#ifdef MAP_EDITOR
    map4EditorsMenu::getInstanse()->setEnabled(state);
    
    ((MenuItem*)addFunctionMenu->getChildren().at(0))->setEnabled(state);
#endif
    
    for(int i = 0; i< layers.size(); ++i)
    {
        layers[i]->setEnabled(state);
    }
    
    if(isAnimationRunnnig)
    {
        if(!state)
            activeButton->stopAnimation();
        else
            activeButton->startAnimation();
    }
}

void ProgramLayer::reset()
{
    if(activeButton != nullptr && isAnimationRunnnig)
        activeButton ->stopAnimation();
    
    isAnimationRunnnig = false;
    activeButton = nullptr;
}

std::vector<_instruction> ProgramLayer::getMainMethod()
{
    MethodLayer *layer = layers[0];
    
    return layer->makeMethod();
}

#ifdef MAP_EDITOR

void ProgramLayer::addMethod (cocos2d::Ref * sender)
{
    if(layers.size() > 3) return;
    MethodLayer * methodLayer = MethodLayer::create(this);
    methodLayer->setIndex(layers.size());
    
    switch (layers.size()) {
        case 1:
            methodLayer->setMethodID(CMDA);
            methodLayer->setName("Command 1");
            break;
        case 2:
            methodLayer->setMethodID(CMDB);
            methodLayer->setName("Command 2");
            break;
        case 3:
            methodLayer->setMethodID(CMDC);
            methodLayer->setName("Command 3");
            break;
    }
    
    layers.push_back(methodLayer);
    
    methodLayer->setRepeater(1);
    methodLayer->setCondition(1);
    methodLayer->setUseRepeater(true);
    methodLayer->setUseCondition(true);
    methodLayer->setWidth(2);
    methodLayer->setHeight(1);
    
    methodLayer->setMethods(std::vector<_method>(2,_method(0, _method::pmNormal, _method::pmExecuteOnTrue)));
    
    methodLayer->setHidden(false);
    
    methodLayer->setResizable(false);
    methodLayer->setMaxHeight(1);
    methodLayer->setMinHeight(1);
    
    methodLayer->drawFunctions();
    
    getScroll()->addChild(methodLayer, 10);
    
    update();
}

#endif


