//
//  MethodScrollLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/19/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "MethodScrollLayer.h"

#include "MethodStackLayer.h"

USING_NS_CC;
USING_NS_CC_EXT;

MethodScrollLayer *MethodScrollLayer::create(MethodStackLayer *parent)
{
    MethodScrollLayer *pRet = new MethodScrollLayer(parent);
    
    if(pRet && pRet->init()) {
        pRet->autorelease();
        return pRet;
    } else {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
    
}

bool MethodScrollLayer::init()
{
    if(!ScrollView::init())
        return false;

    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(MethodScrollLayer::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(MethodScrollLayer::touchEnded, this);
    listener->onTouchMoved = CC_CALLBACK_2(MethodScrollLayer::touchMoved, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

void MethodScrollLayer::setOpacity(GLubyte opacity)
{
    for(int i = 0; i < getChildrenCount(); ++i)
    {
        ((Sprite *)getChildren().at(i))->setOpacity(opacity);
    }
}

bool MethodScrollLayer::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(!active)
        return false;
    
    Point touchPoint = this->convertTouchToNodeSpace(touch);
    
    if(parent->getLastSprite()!= nullptr && parent->getLastSprite()->boundingBox().containsPoint(touchPoint))
    {
        startPoint = parent->getLastSprite()->getPosition();
        dragStarted = true;
        removed = false;
        return true;
    }
    
    return ScrollView::onTouchBegan(touch,pEvent);
}


void MethodScrollLayer::touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(dragStarted)
    {
        Point diff = touch->getDelta();
        diff.x = 0;
        
        Point currentPos = parent->getLastSprite()->getPosition();
        parent->getLastSprite()->setPosition( currentPos + diff );
    }
    
    ScrollView::onTouchMoved(touch,pEvent);
}

void MethodScrollLayer::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(dragStarted)
    {
        if(parent->getLastSprite()->getPositionY() > getContentSize().height && !removed)
        {
            removed = true;
            parent->removeMethod();
        }
        else
        {
            FiniteTimeAction* moveButton = MoveTo::create(SYSTEM_ANIMATION_DELAY / 2, startPoint);
            parent->getLastSprite()->runAction(moveButton);
        }
    }
    
    ScrollView::onTouchEnded(touch,pEvent);
}
