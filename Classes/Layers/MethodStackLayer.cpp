//
//  MethodStackLayer.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/19/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "MethodStackLayer.h"
#include "GameLayer.h"
#include "PMSettings.h"

USING_NS_CC;
USING_NS_CC_EXT;

MethodStackLayer *MethodStackLayer::create(GameLayer *parent,RobotManager *robotManager)
{
    MethodStackLayer *pRet = new MethodStackLayer(parent, robotManager);
    
    if(pRet && pRet->init()) {
        pRet->autorelease();
        return pRet;
    } else {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
    
}

bool MethodStackLayer::init()
{
    Size mapSize = PMSettings::instance()->mapSize();
    
    if ( !Layer::init() )
    {
        return false;
    }
    
    stackCapasity = ((int)mapSize.height - 400) / (int)BUTTON_TILE_SIZE;
    
    showButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("show", NORMAL_STATE),
                                        PMTextureCache::instanse()->getIconSprite("show", SELECTED_STATE),
                                        PMTextureCache::instanse()->getIconSprite("show", DISABLED_STATE),
                                        CC_CALLBACK_1(MethodStackLayer::show, this));
    
    hideButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("hide", NORMAL_STATE),
                                        PMTextureCache::instanse()->getIconSprite("hide", SELECTED_STATE),
                                        PMTextureCache::instanse()->getIconSprite("hide", DISABLED_STATE),
                                        CC_CALLBACK_1(MethodStackLayer::hide, this));
    
    clearButton = MenuItemSprite::create(PMTextureCache::instanse()->getIconSprite("clear", NORMAL_STATE),
                                         PMTextureCache::instanse()->getIconSprite("clear", SELECTED_STATE),
                                         PMTextureCache::instanse()->getIconSprite("clear", DISABLED_STATE),
                                        CC_CALLBACK_1(MethodStackLayer::clearCallback, this));
    
    showButton->setAnchorPoint( Point::ZERO );
    showButton->setPosition( Point::ZERO );
    hideButton->setAnchorPoint( Point::ZERO );
    hideButton->setPosition( Point::ZERO );
    clearButton->setPosition( Point(0, - hideButton->getContentSize().height) );
    clearButton->setAnchorPoint( Point::ZERO );
    clearButton->setEnabled(false);
    
    hideButton->setVisible(false);
    
    Menu *menu = Menu::create(showButton, hideButton, clearButton, nullptr);
    menu->setAnchorPoint( Point::ZERO );
    menu->setPosition( Point(0, stackCapasity * BUTTON_TILE_SIZE - showButton->getContentSize().height) );
    
    setContentSize(Size(BUTTON_TILE_SIZE + showButton->getContentSize().width, stackCapasity * BUTTON_TILE_SIZE));
    setPosition(Point(mapSize.width - showButton->getContentSize().width, 200));
    
    colorLayer = LayerColor::create(Color4B(100,100,100,150), BUTTON_TILE_SIZE, stackCapasity * BUTTON_TILE_SIZE);
    colorLayer->setAnchorPoint( Point(0,0) );
    colorLayer->setPosition( Point(showButton->getContentSize().width,0) );
    
    scroll = MethodScrollLayer::create(this);
    scroll->setViewSize( Size(BUTTON_TILE_SIZE, stackCapasity * BUTTON_TILE_SIZE) );
    scroll->setAnchorPoint( Point(0,0) );
    scroll->setPosition( Point(showButton->getContentSize().width,0) );
    scroll->setContentSize( Size(BUTTON_TILE_SIZE, stackCapasity * BUTTON_TILE_SIZE) );
    scroll->setContentOffset( Point(0, 0), true);
    scroll->setClippingToBounds(true);
    scroll->setDirection(ScrollView::Direction::VERTICAL);
    scroll->setZoomScale(0.5);
    scroll->setBounceable(true);
    
    addChild(menu);
    addChild(colorLayer);
    addChild(scroll, 10);
    
    topPoint = Point(showButton->getContentSize().width, getContentSize().height - BUTTON_TILE_SIZE);
    curPoint = Point::ZERO;
    
    //clear();
    lastSprite = nullptr;
	lastButtonRepeater = false;

    setOpacity(0);
    
    return true;
}

void MethodStackLayer::setEnabled(bool flag)
{
    showButton->setEnabled(flag);
    hideButton->setEnabled(flag);
    clearButton->setEnabled(flag);
    
    scroll->setActive(flag);
}

void MethodStackLayer::setOpacity(GLubyte opacity)
{
    colorLayer->setOpacity(opacity);
    scroll->setOpacity(opacity);
}

void MethodStackLayer::toggleIcon()
{
    showButton->setVisible( !showButton->isVisible() );
    hideButton->setVisible( !hideButton->isVisible() );
}

void MethodStackLayer::addMethod(SelectButton *button)
{
    lastButton = button;
    
    parent->getProgramLayer()->setEnabled(false);
    
    if(button->isRepeater() && lastButtonRepeater)
        return;
    
    if(button->isRepeater())
        lastButtonRepeater = true;
    
    lastSprite = button->cloneSprite();
    
    if(BUTTON_TILE_SIZE * (methodStack.size() + 1) > scroll->getContentSize().height)
    {
        scroll->setContentSize( Size(BUTTON_TILE_SIZE, scroll->getContentSize().height + BUTTON_TILE_SIZE));
        scroll->setContentOffset(Point(0, BUTTON_TILE_SIZE * stackCapasity - scroll->getContentSize().height), true);
    }
    
    Point worldPosition = parent->getProgramLayer()->getSelectMenu()->convertToWorldSpace(button->getPosition());
    Point thisPostion = this->convertToNodeSpace(worldPosition);
    
    thisPostion = Point(thisPostion.x - lastSprite->getContentSize().width / 2,thisPostion.y - lastSprite->getContentSize().height / 2);
    
    lastSprite->setPosition(thisPostion);
    
    addChild(lastSprite, 20);
    spriteStack.push(lastSprite);
    
    lastSprite->setAnchorPoint(Point::ZERO);
    
    FiniteTimeAction* moveSpriteParaboloic = CCJumpBy::create(3.0f * SYSTEM_ANIMATION_DELAY, topPoint - lastSprite->getPosition(),110.0f, 1);
    FiniteTimeAction* moveSpriteDown = MoveTo::create(3.0f * SYSTEM_ANIMATION_DELAY, curPoint);
    
    FiniteTimeAction* reorder = CallFunc::create(CC_CALLBACK_0(MethodStackLayer::reorderSprite, this));
    FiniteTimeAction* moveEnd = CallFunc::create(CC_CALLBACK_0(MethodStackLayer::endAddMethod, this));
    FiniteTimeAction* moveStart = CallFunc::create(CC_CALLBACK_0(MethodStackLayer::startAddMethod, this));
    
    curPoint = Point(0, curPoint.y + BUTTON_TILE_SIZE);
    
    lastSprite->runAction( Sequence::create(moveStart, moveSpriteParaboloic, reorder, moveSpriteDown, moveEnd, nullptr) );
    

    clearButton->setEnabled(true);
}

void MethodStackLayer::reorderSprite()
{
    Point worldPosition = convertToWorldSpace(lastSprite->getPosition());
    
    lastSprite->retain();
    
    removeChild(lastSprite, false);
    
    scroll->addChild(lastSprite,1);

    Point pos = scroll->convertToNodeSpace(worldPosition);
    
    if(methodStack.size() > stackCapasity)
        pos.y += scroll->getContentSize().height - BUTTON_TILE_SIZE * stackCapasity;
    
    lastSprite->setPosition(pos);
    
    lastSprite->release();
}

void MethodStackLayer::removeMethod()
{
    _instruction instr = methodStack.top();
    methodStack.pop();
    
    _instruction nextInstr = {-1,-1,-1,-1};
    
    if(!methodStack.empty())
        nextInstr = methodStack.top();
    
    int count = 1;
    
    if(nextInstr.command == INSTR_START_LOOP )
    {
        count = nextInstr.param;
    }
    
    if(instr.command != INSTR_START_LOOP)
    {
        instr.param = robotManager->getRobot()->getMethod(instr.param).getInverseMethod();
        
        robotManager->forceAddMethod(instr.param);
    }
    
    if(instr.command != INSTR_START_LOOP)
        robotManager->setState(RobotManager::ControlledByStack);
    
    scroll->removeChild(lastSprite);
    
    spriteStack.pop();
    
    if(methodStack.size() > 0)
    {
        lastSprite = spriteStack.top();
        
        if(methodStack.size() + 1 > stackCapasity)
        {
            setContentSize(Size(BUTTON_TILE_SIZE, scroll->getContentSize().height - BUTTON_TILE_SIZE));
            scroll->setContentOffset(Point(0, BUTTON_TILE_SIZE * stackCapasity - scroll->getContentSize().height), true);
        }
    }
    else
        lastSprite = nullptr;
    
    curPoint.y -= BUTTON_TILE_SIZE;
}

void MethodStackLayer::startAddMethod()
{
    _instruction instr;
    
    if(lastButton->isMethod())
    {
        int count = 1;
        
        if(lastButtonRepeater)
            count = methodStack.top().param;
        
        robotManager->forceAddMethod(lastButton->getMethodID(), count);
        
        instr.command = INSTR_EXECUTE;
        instr.param = lastButton->getMethodID();
        instr.otherParams[1] = -1;
        instr.otherParams[0] = -1;
    }
    else if(lastButton->isRepeater())
    {
        instr.command = INSTR_START_LOOP;
        instr.param = lastButton->getRepeater();
        instr.otherParams[1] = 0;
        instr.otherParams[0] = 0;
    }
    
    methodStack.push(instr);
    
}

void MethodStackLayer::clear()
{
    scroll->getContainer()->removeAllChildrenWithCleanup(true);
    
    methodStack = std::stack<_instruction>();
    spriteStack = std::stack<Sprite *>();
    
    lastButtonRepeater = false;
    lastButton = nullptr;
    lastSprite = nullptr;
    clearButton->setEnabled(false);
    
    curPoint = Point::ZERO;

}

void MethodStackLayer::clearCallback(cocos2d::Ref* object)
{
    clear();
    
    parent->restartLevel(nullptr);
}

void MethodStackLayer::endAddMethod()
{
    if(lastButton->isMethod())
    {
        lastButtonRepeater = false;
    
        robotManager->setState(RobotManager::ControlledByStack);
    }
    
    parent->getProgramLayer()->setEnabled(true);
}

void MethodStackLayer::show(cocos2d::Ref *sender)
{
    Size mapSize = PMSettings::instance()->mapSize();
    
    FiniteTimeAction* moveLayer = MoveTo::create(SYSTEM_ANIMATION_DELAY, Point(mapSize.width - BUTTON_TILE_SIZE - showButton->getContentSize().width - 10, 200));
    
    setEnabled(false);
    
    FiniteTimeAction *fadeLayer = FadeTo::create(SYSTEM_ANIMATION_DELAY, 150);
    
    FiniteTimeAction *fadeColorLayer = TargetedAction::create(colorLayer, fadeLayer);
    
    FiniteTimeAction* moveEnd = CallFunc::create(CC_CALLBACK_0(MethodStackLayer::showEnded, this));
    
    runAction( Sequence::create(moveLayer,fadeColorLayer,moveEnd, nullptr) );
    
    
    robotManager->setControlled(true);
    
}

void MethodStackLayer::hide(cocos2d::Ref *sender)
{
    parent->reorderChild(this,10);
    scroll->setActive(false);
    
    Size mapSize = PMSettings::instance()->mapSize();
    
    FiniteTimeAction* moveLayer = MoveTo::create(SYSTEM_ANIMATION_DELAY,  Point(mapSize.width - showButton->getContentSize().width, 200));
    
    setEnabled(false);
    
    FiniteTimeAction *fadeLayer = FadeTo::create(SYSTEM_ANIMATION_DELAY, 0);
    
    FiniteTimeAction *fadeColorLayer = TargetedAction::create(colorLayer, fadeLayer);
    
    FiniteTimeAction* moveEnd = CallFunc::create(CC_CALLBACK_0(MethodStackLayer::hideEnded, this));
    
    runAction( Sequence::create(moveEnd, fadeColorLayer, moveLayer, nullptr) );

    robotManager->setControlled(false);
}

void MethodStackLayer::showEnded()
{
    setEnabled(true);
    
    parent->reorderChild(this,30);
    parent->getProgramLayer()->setControlledMode(true);
    toggleIcon();
    
    parent->getProgramControlLayer()->setEnabled(false);
    scroll->setActive(true);
    
    //setOpacity(150);
}

void MethodStackLayer::hideEnded()
{
    parent->getProgramLayer()->setControlledMode(false);
    toggleIcon();
    
    parent->getProgramControlLayer()->setEnabled(true);
    
    parent->restartLevel(nullptr);
    
    //setOpacity(0);
}

std::vector<int> MethodStackLayer::getProgram()
{
    std::vector<int> ret;
    
    
    while(!methodStack.empty())
    {
        _instruction instr = methodStack.top();
        methodStack.pop();
        
        if(instr.command == INSTR_START_LOOP)
            continue;
        
        _instruction nextInstr = {-1,-1,-1,-1};
        
        if(!methodStack.empty())
            nextInstr = methodStack.top();
        
        if(nextInstr.command == INSTR_START_LOOP)
        {
            for(int  j = 0; j < nextInstr.param; ++j)
            {
                ret.push_back(instr.param);
            }
        }
        else
            ret.push_back(instr.param);
    }
    //reverse 
    for(int i = 0; i < ret.size() / 2; ++i)
    {
        int temp = ret[i];
        ret[i] = ret[ret.size() - 1 - i];
        ret[ret.size() - 1 - i] = temp;
    }
    
    clear();
    
    hide(nullptr);
    
    return ret;
}

