//
//  MethodHintLayer.cpp
//  Piktomir
//
//  Created by [scaR] on 20.03.14.
//
//

#include "MethodHintLayer.h"
#include "PMTextureCache.h"

#include "utils.h"

USING_NS_CC;

MethodHintLayer *MethodHintLayer::create(std::string text, cocos2d::Node *callButton)
{
    MethodHintLayer *pRet = new MethodHintLayer();
    
    if(pRet && pRet->initWithText(text, callButton)) {
        pRet->autorelease();
        return pRet;
    } else {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
    
}

bool MethodHintLayer::initWithText(std::string text, Node *callButton)
{   
    if ( !init())
    {
        return false;
    }
    
    Node *parentNode = callButton->getParent();
    
    Point worldPoint = parentNode->convertToWorldSpace(parentNode->getPosition());
    setAnchorPoint( Point::ZERO );
    
    TTFConfig config("fonts/tahoma.ttf", 17);
    
    Label *label = Label::createWithTTF(config, text, TextHAlignment::CENTER, 150);
    Sprite *sprite = PMTextureCache::instanse()->getIconSprite("info");
    
    setContentSize( Size(150 + sprite->getContentSize().width + 2 * METHOD_HINT_BORDER,
                         label->getContentSize().height + 2 * METHOD_HINT_BORDER) );
    
    sprite->setAnchorPoint( Point::ZERO );
    sprite->setPosition( Point(0, getContentSize().height - sprite->getContentSize().height) );
    
    label->setAnchorPoint( Point(0.5, 0.5) );
    label->setPosition( Point(sprite->getContentSize().width / 2 + getContentSize().width / 2, getContentSize().height / 2) );
    
    layerColor= LayerColor::create(Color4B(145,145,145,255 * 0.8), getContentSize().width, getContentSize().height);
    
    layerColor->addChild(sprite);
    layerColor->addChild(label);
    
    if(worldPoint.y - label->getContentSize().height < 0)
    {
        setPosition( Point(parentNode->getPositionX() - getContentSize().width - parentNode->getContentSize().width,
                           parentNode->getPositionY() + parentNode->getContentSize().height) );
    }
    else
    {
        setPosition( Point(parentNode->getPositionX() - getContentSize().width,
                           parentNode->getPositionY() - getContentSize().height - parentNode->getContentSize().height / 2) );
    }

    
    auto shape = DrawNode::create();

    Point *rect = new Point[20 * 4];
    
    float BORDER_RADIUS = sprite->getContentSize().height/2;
    
    Point *arc0 = getArcPoints(BORDER_RADIUS, BORDER_RADIUS , BORDER_RADIUS ,CC_DEGREES_TO_RADIANS(180),CC_DEGREES_TO_RADIANS(90), 20);
    Point *arc1 = getArcPoints(getContentSize().width - BORDER_RADIUS, BORDER_RADIUS, BORDER_RADIUS, CC_DEGREES_TO_RADIANS(270), CC_DEGREES_TO_RADIANS(90), 20);
    Point *arc2 = getArcPoints(getContentSize().width - BORDER_RADIUS, getContentSize().height - BORDER_RADIUS, BORDER_RADIUS,CC_DEGREES_TO_RADIANS(0),CC_DEGREES_TO_RADIANS(90), 20);
    Point *arc3 = getArcPoints(BORDER_RADIUS, getContentSize().height - BORDER_RADIUS, BORDER_RADIUS,CC_DEGREES_TO_RADIANS(90),CC_DEGREES_TO_RADIANS(90), 20);
    
    memcpy(rect, arc0, 20 * sizeof(Point));
    memcpy(rect + 20, arc1, 20 * sizeof(Point));
    memcpy(rect + 40, arc2, 20 * sizeof(Point));
    memcpy(rect + 60, arc3, 20 * sizeof(Point));
    
    static Color4F green(0, 1, 0, 1);
    shape->drawPolygon(rect, 4 * 20, green, 0, green);
    shape->setPosition( Point(0, 0) );
    
    auto clipper = ClippingNode::create();
    clipper->setAnchorPoint(Point(0, 0));
    clipper->setPosition( Point::ZERO );
    clipper->setStencil(shape);
    clipper->addChild(layerColor);
    
    
    addChild(clipper, 0);

    delete rect;
    delete arc0;
    delete arc1;
    delete arc2;
    delete arc3;
    
    
    listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(MethodHintLayer::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(MethodHintLayer::touchEnded, this);
    listener->onTouchMoved = CC_CALLBACK_2(MethodHintLayer::touchMoved, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    startSelfRemove();
    
    return true;
}

void MethodHintLayer::startSelfRemove()
{
    FiniteTimeAction *delay  = DelayTime::create(METHOD_HINT_SELFREMOVE_DELAY);
    
    FiniteTimeAction *fadeLayer = FadeTo::create(SYSTEM_ANIMATION_DELAY, 0);
    
    FiniteTimeAction *fadeColorLayer = TargetedAction::create(layerColor, fadeLayer);
    
    FiniteTimeAction* end = CallFunc::create(CC_CALLBACK_0(MethodHintLayer::selfRemove, this));
    
    runAction( Sequence::create(delay, fadeColorLayer, end, nullptr) );
}

void MethodHintLayer::selfRemove()
{
    getParent()->removeChild(this);
}

bool MethodHintLayer::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    if(layerColor->boundingBox().containsPoint(convertTouchToNodeSpace(touch)))
        return true;
    
    return false;
}

void MethodHintLayer::touchMoved(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    
}

void MethodHintLayer::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent)
{
    selfRemove();
}
