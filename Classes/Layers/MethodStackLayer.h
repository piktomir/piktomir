//
//  MethodStackLayer.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/19/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__MethodStackLayer__
#define __pictomir_cocos2d_x__MethodStackLayer__

#include <iostream>
#include "cocos2d.h"

#include "RobotManager.h"
#include "SelectButton.h"
#include "MethodScrollLayer.h"

class GameLayer;

class MethodStackLayer : public cocos2d::Layer
{
public:
    
    MethodStackLayer():
        cocos2d::Layer(),
        parent(nullptr),
        robotManager(nullptr)
    {}
    
    MethodStackLayer(GameLayer *parent,RobotManager *robotManager):
        cocos2d::Layer(),
        parent(parent),
        robotManager(robotManager)
    {}
    
    ~MethodStackLayer()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    virtual bool init();
    
    CREATE_FUNC(MethodStackLayer);
    
    static MethodStackLayer *create(GameLayer *parent, RobotManager *robotManager);
    
    void show(cocos2d::Ref *sender);
    void hide(cocos2d::Ref *sender);
    
    void setEnabled(bool flag);
    
    void addMethod(SelectButton *button);
    
    bool isLastButtonRepeater()
    {
        return lastButtonRepeater;
    }
    
    void clearCallback(cocos2d::Ref* object);
    void clear();
    
    bool working ()
    {
        return hideButton->isVisible();
    }
    
    std::vector<int> getProgram();
    
    void saveStates()
    {
        showHideState = showButton->isEnabled();
        clearState = clearButton->isEnabled();
        scrollState = scroll->getActive();
    }

    void restoreStates()
    {
        showButton->setEnabled(showHideState);
        hideButton->setEnabled(showHideState);
        clearButton->setEnabled(clearState);
        scroll->setActive(scrollState);
    }
    
private:
    
    GameLayer *parent;
    RobotManager *robotManager;
    
    cocos2d::MenuItemSprite* showButton;
    cocos2d::MenuItemSprite* hideButton;
    cocos2d::MenuItemSprite* clearButton;
    
    int stackCapasity;
    
    cocos2d::Point topPoint;
    cocos2d::Point curPoint;
    
    std::stack<_instruction> methodStack;
    std::stack<cocos2d::Sprite *> spriteStack;
    
    bool lastButtonRepeater;
    SYNTHESIZE(cocos2d::Sprite *, lastSprite, LastSprite);
    SelectButton *lastButton;
    MethodScrollLayer *scroll;
    cocos2d::LayerColor *colorLayer;
    
    bool showHideState;
    bool clearState;
    bool scrollState;
    
    void toggleIcon();
    
    void showEnded();
    void hideEnded();
    
    void reorderSprite();
    void endAddMethod();
    void startAddMethod();
    
    void removeMethod();
    
    void setOpacity(GLubyte opacity);
};

#endif /* defined(__pictomir_cocos2d_x__MethodStackLayer__) */
