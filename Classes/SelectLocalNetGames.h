//
//  SelectLocalNetGames.h
//  Piktomir
//
//  Created by Danila Eremin on 03.04.14.
//
//

#ifndef __Piktomir__SelectLocalNetGames__
#define __Piktomir__SelectLocalNetGames__

#include <iostream>
#include "cocos2d.h"

USING_NS_CC;

class SelectLocalNetGames : public cocos2d::Layer
{
public:
    virtual bool init();
    
    CREATE_FUNC(SelectLocalNetGames);
    
    static cocos2d::Scene* scene();
    
    ~SelectLocalNetGames()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    void uploadMapsToServer (cocos2d::Ref *sender);
    
private:
    void setNetGame(cocos2d::Ref *sender);
    void setLocalGame(cocos2d::Ref *sender);
    void mainMenu(cocos2d::Ref *sender);
};

#endif /* defined(__Piktomir__SelectLocalNetGames__) */
