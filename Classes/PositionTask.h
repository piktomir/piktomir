//
//  PositionTask.h
//  pictomir
//
//  Created by [scaR] on 22.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#ifndef __pictomir__PositionTask__
#define __pictomir__PositionTask__

#include <iostream>
#include "Task.h"
#include "cocos2d.h"

struct _position_task_update_data: public _task_update_data
{
    _position_task_update_data(int layer, cocos2d::Point point):
        _task_update_data(pmPositionTask),
        layer(layer),
        point(point)
    {}
    
    int layer;
    cocos2d::Point point;
};

class PositionTask: public Task
{
private:
    SYNTHESIZE(int, targetLayer, TargetLayer);
    SYNTHESIZE(cocos2d::Point, targetPoint, TargetPoint);
    
    int curLayer = 0;
    cocos2d::Point curPoint = cocos2d::Point::ZERO;
public:
    
    friend class XMLWorldSaver;
    
    PositionTask (AbstractRobot *robot, int targetLayer, cocos2d::Point targetPoint):
        Task(robot),
        targetLayer(targetLayer),
        targetPoint(targetPoint)
    {}
    
    static PositionTask *create(AbstractRobot *robot, int targetLayer, cocos2d::Point targetPoint)
    {
        return new PositionTask(robot, targetLayer, targetPoint);
    }
    
    virtual bool complete()
    {
        return curLayer == targetLayer && targetPoint.equals(curPoint);
    }
    
    virtual void clear()
    {
        curPoint = cocos2d::Point::ZERO;
    }
    virtual void update(const std::shared_ptr<_task_update_data> &data)
    {
        curLayer = ((_position_task_update_data *)data.get())->layer;
        curPoint = ((_position_task_update_data *)data.get())->point;
    }
    
    ~PositionTask()
    {
        
    }

};

#endif /* defined(__pictomir__PositionTask__) */
