//
//  TeacherActionLayer.h
//  pictomir-cocos2d-x
//
//  Created by Admin on 05/02/14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__TeacherActionLayer__
#define __pictomir_cocos2d_x__TeacherActionLayer__

#include <iostream>
#include "cocos2d.h"
#include "cocos-ext.h"

#include "ClientListLayer.h"
#include "TeacherMapSelectLayer.h"
#include "PMServerHost.h"
#include "PMManager.h"

const float TAL_HEIGHT = 150.0;
const float TAL_STARTGAME_SECTIONSIZE = 150.0;
const float TAL_GROUPACTIONS_SECTIONSIZE = 250.0;

class TeacherActionLayer :public cocos2d::LayerColor
{
public:
    virtual bool init();
    bool initWithControlLayers(ClientListLayer *freeClientListLayer_,ClientListLayer *playingClientListLayer_,TeacherMapSelectLayer *mapSelectLayer_);
    
    CREATE_FUNC(TeacherActionLayer);
    
    ~TeacherActionLayer()
    {
        //PMManager::instance()->enable();
        removeAllChildrenWithCleanup(true);
    }
    
    static TeacherActionLayer* create(ClientListLayer *freeClientListLayer,ClientListLayer *playingClientListLayer,TeacherMapSelectLayer *mapSelectLayer)
    {
        TeacherActionLayer *pRet = new TeacherActionLayer();
        if (pRet && pRet->initWithControlLayers(freeClientListLayer, playingClientListLayer, mapSelectLayer))
        {
            pRet->autorelease();
            return pRet;
        }
        else
        {
            delete pRet;
            pRet = nullptr;
            return nullptr;
        }
    }

    
private:
    
    cocos2d::CustomCommand lineDrawCommand;
    
    float headerLabelHeight;
    
    ClientListLayer *freeClientListLayer;
    ClientListLayer *playingClientListLayer;
    
    TeacherMapSelectLayer *mapSelectLayer;
    
    void startGames(cocos2d::Ref *sender);
    
    void createGroup(cocos2d::Ref *sender);
    void deleteGroup(cocos2d::Ref *sender);
    void addToGroup(cocos2d::Ref *sender);
    void deleteFromGroup(cocos2d::Ref *sender);
    
    void setPlayingClients();
    
    void draw(cocos2d::Renderer* renderer, const kmMat4 &transform, bool transformUpdated);
    void onDraw(const kmMat4 &transform, bool transformUpdated);
    void addClient(PMClient *client);
    void removeClient(PMClient *client);
};
#endif /* defined(__pictomir_cocos2d_x__TeacherActionLayer__) */
