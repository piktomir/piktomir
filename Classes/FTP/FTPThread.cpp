//
//  FTPThread.cpp
//  Piktomir
//
//  Created by Бесшапошников Никита on 03.04.14.
//
//

#include "FTPThread.h"

#include "utils.h"
#include "PMSettings.h"
#include "FTPuser.h"

USING_NS_CC;
using namespace std;

void FTPThread::start()
{
    thread t(&FTPThread::downloadMaps, this);
    t.detach();
}

void FTPThread::downloadMaps()
{
    std::string writableMapPath = writablePath + "Maps";
    PMSettings::instance()->setMapPath(writableMapPath);
    
    FTPuser *ftpUser = new FTPuser(writablePath,"ak");
    
    if(ftpUser->checkFTP())
    {
        int version;
        
        if((version = ftpUser->downloadVerMap()) != -1)
        {
            int myVersion = PMSettings::instance()->getMapVersion();
            
            if(myVersion < version || !folderExists(writableMapPath))
            {
                if(ftpUser->downloadMap())
                {
                    rmdir(writableMapPath.c_str());
                    
                    platform_mkdir((writablePath + "Maps").c_str());
                    
                    if(unzipFile((writablePath + "Maps.zip").c_str(), writablePath + "Maps/" ) == 0)
                    {
                        PMSettings::instance()->setMapVersion(version);
                    }
                    else
                    {
                        result = "Error unzipping map archive";
                    }
                    
                }
            }
            else if(folderExists(writableMapPath))
            {
                result = "Your maps verison is up to date, using local copy!";
            }
        }
    }
    else
    {
        result = "No FTP connection, using local copy!";
    }
    
    if(!folderExists(writableMapPath))
        PMSettings::instance()->setMapPath("Maps");
    
	PMSettings::instance()->loadWorlds();

    // wait for init this scene
    while(!Director::getInstance()->getRunningScene()) ;
    
    Director::getInstance()->getRunningScene()->getScheduler()->performFunctionInCocosThread(callback);
}