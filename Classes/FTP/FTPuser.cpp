//
//  user.cpp
//  Piktomir FTP access
//
//  Created by Anton Dedkov on 02.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "FTPuser.h"
#include <stdio.h>
#include "PMSettings.h"

const char* const ftpUsersFile = "ftp://vip.niisi.ru:2121/users.txt";
const char* const ftpPath = "ftp://vip.niisi.ru";
const char* const ftpLogin = "piktomir";
const char* const ftpPass = "mexmat2013";
const int ftpPort = 2121;

using namespace std;
string data_ = "";

size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up)
{
    for (int c = 0; c<size*nmemb; c++)
    {
        data_.push_back(buf[c]);
    }
    return size*nmemb; //tell curl how many bytes we handled
}

FTPuser::FTPuser (const FTPuser & user)
{
    user_ = user.user_;
    passHash_ = user.passHash_;
    
    writeablePath_ = user.writeablePath_;
    
    downloadChecked = user.downloadChecked;
    uploadChecked = user.uploadChecked;
}

FTPuser::FTPuser (std::string _writeablePath_, std::string login, std::string pass)
{
    writeablePath_ = _writeablePath_;
    user_ = login;
    passHash_ = pass;
}

FTPuser::FTPuser (std::string _writeablePath_, std::string login)
{
    writeablePath_ = _writeablePath_;
    user_ = login;
    passHash_ = "e678d62957a6112ba1cc88d47196324e";
}

FTPuser::FTPuser (std::string _writeablePath_)
{
    writeablePath_ = _writeablePath_;
    user_ = "default";
    passHash_ = "e678d62957a6112ba1cc88d47196324e";
}

bool FTPuser::checkFTP ()
{
    uploadChecked = false;
    downloadChecked = false;
    if (user_ == "default" && passHash_ == "e678d62957a6112ba1cc88d47196324e") {
        uploadChecked = true;
        downloadChecked = true;
        return true;
    } if (user_ == "default" && passHash_ == "d41d8cd98f00b204e9800998ecf8427e") {
        uploadChecked = false;
        downloadChecked = true;
        return true;
    }
    
    if (passHash_ != "d41d8cd98f00b204e9800998ecf8427e" && passHash_ != "e678d62957a6112ba1cc88d47196324e")
    {
        CURL* curl; //our curl object
        
        curl_global_init(CURL_GLOBAL_ALL); //pretty obvious
        curl = curl_easy_init();
        
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 5);
        curl_easy_setopt(curl, CURLOPT_URL, "http://www.piktomir.ru/login.php?login=tmp");
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress
        
        curl_easy_perform(curl);
        
        cout << endl << data_ << endl;
        
        curl_easy_cleanup(curl);
        curl_global_cleanup();
        if (data_.substr(data_.length()-4) == "TRUE") {
            uploadChecked = true;
            downloadChecked = true;
            return true;
        }
    }
    else
    {
        CURL* curl; //our curl object
        
        curl_global_init(CURL_GLOBAL_ALL); //pretty obvious
        curl = curl_easy_init();
        
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 5);
        curl_easy_setopt(curl, CURLOPT_URL, "http://www.piktomir.ru/login.php?login=tmp");
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress
        
        curl_easy_perform(curl);
        
        cout << endl << data_ << endl;
        
        curl_easy_cleanup(curl);
        curl_global_cleanup();
        if (data_.length() > 4 && data_.substr(data_.length()-4) == "TRUE")
        {
            uploadChecked = false;
            downloadChecked = true;
            return true;
        }
    }
    
    return false;
}

int FTPuser::downloadVerMap()
{
    if (!downloadChecked)
    {
        return -1;
    }
    
    string mapVerPath = writeablePath_ + "ver.txt";
    char ftpVerFile[100] = "";
    
    
    sprintf(ftpVerFile, "ftp://vip.niisi.ru:2121/%s/ver.txt", user_.c_str());
    
    
    if(downloadFileFTP(ftpVerFile, ftpLogin, ftpPass, mapVerPath.c_str()) != CURLE_OK)
    {
        CCLOGERROR("Error downloading map version from ftp, using local copy.");
        return -1;
    }
    
    int version = -1;
    FILE *file = fopen(mapVerPath.c_str(), "rt");
    
    fscanf(file,"%d",&version);
    
    fclose(file);
    
    return version;
}

bool FTPuser::downloadMap()
{
    if (!downloadChecked)
    {
        return false;
    }
    
    string mapPath = writeablePath_ + "Maps.zip";
    char ftpMapFile[100] = "";
    
    sprintf(ftpMapFile, "ftp://vip.niisi.ru:2121/%s/Maps.zip", user_.c_str());
    
    if(downloadFileFTP(ftpMapFile, ftpLogin, ftpPass, mapPath.c_str()) != CURLE_OK)
    {
        CCLOG("Error downloading map version from ftp, using local copy.");
        return false;
    }
    
    return true;
}

bool FTPuser::uploadMap (std::string mapVerPath, std::string mapPath)
{
    if (!uploadChecked)
    {
        return false;
    }
    
    char ftpVerFile[100] = "";
    sprintf(ftpVerFile, "ftp://vip.niisi.ru:2121/%s/ver.txt", user_.c_str());
    
    char ftpMapFile[100] = "";
    sprintf(ftpMapFile, "ftp://vip.niisi.ru:2121/%s/Maps.zip", user_.c_str());
    
    if(!uploadFileFTP(ftpVerFile, ftpLogin, ftpPass, mapVerPath.c_str()))
    {
        CCLOG("Error uploading version file from local, to ftp.");
        return false;
    }
    
    if(!uploadFileFTP(ftpMapFile, ftpLogin, ftpPass, mapPath.c_str()))
    {
        CCLOG("Error uploading map version from local, to ftp.");
        return false;
    }
    
    return true;
}
