//
//  FTPThread.h
//  Piktomir
//
//  Created by Бесшапошников Никита on 03.04.14.
//
//

#ifndef __Piktomir__FTPThread__
#define __Piktomir__FTPThread__

#include <iostream>
#include <thread>
#include <unistd.h>

#include "cocos2d.h"

class FTPThread
{
private:
    std::function<void ()> callback;
    std::string result;
    
    void downloadMaps();
    
public:
    FTPThread(const std::function<void ()> callback):
        callback(callback),
        result("OK")
    {}
    
    void start();
    
    std::string &getResultString()
    {
        return result;
    }
    
    ~FTPThread()
    {
    
    }
};

#endif /* defined(__Piktomir__FTPThread__) */
