//
//  XmlWorldReader.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/3/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__XmlWorldReader__
#define __pictomir_cocos2d_x__XmlWorldReader__

#include <iostream>

#include "cocos2d.h"
#include "tinyxml2.h"

#include "AbstractWorld.h"
#include "ProgramLayer.h"
#include "GameLayer.h"

#include "PaintTask.h"
#include "PositionTask.h"

class XmlWorldReader
{
public:
    
    XmlWorldReader():
        previewLoad(false)
    {}
    
    AbstractWorld *readWorld(GameLayer *layer,std::string mapPath, int _baseRobot);
    AbstractWorld *readWorldPreview(std::string mapPath);
private:

    bool previewLoad;
    tinyxml2::XMLElement *curNode;
    
    WorldType worldType;
    AbstractWorld *world;
    GameLayer *gameLayer;
    int baseRobot;
    SYNTHESIZE(std::string, error, Error);
    
    //creating objects
    void createWorldByType();
    bool createRobot(RobotType robotType);
    
    //robot Nodes
    bool readRobots();
    bool readRobotNode(RobotType robotType);
    bool readRobot4Node();
    bool readLamp4Node();
    bool readTransportRobot4Node();
    
    //Map Nodes
    bool readMapNode();
    bool readMap4Node();
    
    //Functions node
    bool readFunctionsNode(bool isBaseRobot);
    
    //Method Node
    bool readMethodNode(MethodLayer *methodLayer);
    
    //Task node
    bool readTaskNode();
    
    //Hints Node
    bool readHintsNode();
};


#endif /* defined(__pictomir_cocos2d_x__XmlWorldReader__) */
