//
//  XmlWorldSaver.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 22.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "XmlWorldSaver.h"
#include "cocos-ext.h"
#include "cocos2d.h"

using namespace std;
using namespace tinyxml2;

XMLWorldSaver::XMLWorldSaver(AbstractWorld * world, GameLayer * gamelayer, std::string path)
{
	doc = new tinyxml2::XMLDocument;
	
    XMLDeclaration * decl = doc->NewDeclaration();
    doc->LinkEndChild( decl );
    
	XMLElement * mapNode = doc->NewElement("map");
	doc->LinkEndChild( mapNode );
    
    world_ = world;
    layer_ = gamelayer;
    path_ = path;
    
    if (world_->type() == pmWorld4) {
        saveWorld4(mapNode);
    }
	
	doc->SaveFile( path.c_str() );
    
    delete doc;
    doc = nullptr;
}

void XMLWorldSaver::saveWorld4(XMLElement * mapNode)
{
    World4* world = (World4*)world_;
    Map4* map = (Map4*)(world_->getMap());
    
    mapNode->SetAttribute("width", map->getWidth());
    mapNode->SetAttribute("height", map->getHeight());
    mapNode->SetAttribute("layers", (int)map->layers.size());
    mapNode->SetAttribute("worldtype", 0);
    mapNode->SetAttribute("tileset", map->getTileSet());
    
    XMLElement * layersNode = doc->NewElement("layers");
    mapNode->LinkEndChild(layersNode);
    
    for (int i = 0; i < map->layers.size(); ++i)
    {
        layersNode->LinkEndChild(saveMap4Layer(i));
    }

    XMLElement * robotsNode = doc->NewElement("robots");
    mapNode->LinkEndChild(robotsNode);

    for (int i = 0; i < world->robotCount(); ++i) {

        XMLElement * robotNode = saveRobot(i);
        robotsNode->LinkEndChild(robotNode);

        robotNode->LinkEndChild(saveRobotProgram(i));
        
        robotNode->LinkEndChild(saveTask(i));
    }

    XMLElement * hintsNode = doc->NewElement("hints");
    mapNode->LinkEndChild(hintsNode);

#ifdef MAP_EDITOR
    layer_->readyForSave();
#endif
    

    if(layer_->getHint())
        hintsNode->LinkEndChild(saveHint(-1));
    for (int i = 0; i < layer_->getProgramLayer()->layers.size(); ++i) {
        
#ifdef MAP_EDITOR
        layer_->getProgramLayer()->layers[i]->readyForSave();
#endif
        
        MethodLayer * method = layer_->getProgramLayer()->layers[i];
        XMLElement * hint = nullptr;
        if(method->getHint())
        {
            hint = saveHint(i);
            hintsNode->LinkEndChild(hint);
            hint->SetAttribute("target", method->name.c_str());
        }
    }
}

XMLElement * XMLWorldSaver::saveMap4Layer (int layer)
{
    Map4* map = (Map4*)(world_->getMap());
    
    XMLElement * layerNode = doc->NewElement("layer");
    
    unsigned char * Map = new unsigned char[2*map->width*map->height+1];
    
    for (int i = 0; i < map->height; ++i) {
        for (int j = 0; j < map->width; j++) {
            Map[2*(i*map->width + j)] = 0;
            for(int k = 0; k < 4; k++) Map[2*(i*map->width + j)] += (map->mapElements[layer][i][j]->nearElements[k]!=nullptr)*pow((double)2, 7-k);
            Map[2*(i*map->width + j)]++;
            Map[2*(i*map->width + j)+1] = map->mapElements[layer][i][j]->getType()+2;
        }
    }
    std::string str = base64_encode(Map, 2*map->width*map->height+1);
    
    XMLText * layerContent = doc->NewText(str.c_str());
    layerNode->LinkEndChild(layerContent);
    
    return layerNode;
}

XMLElement * XMLWorldSaver::saveRobotProgram (int robot)
{
    int resize = 0;
    
    XMLElement * functionsNode = doc->NewElement("functions");
    functionsNode->SetAttribute("resizable", resize);
#ifdef MAP_EDITOR
    for (int i = 0; i < ((Robot4*)world_->robotAt(robot))->program->layers.size(); ++i) {
        functionsNode->LinkEndChild(saveRobotMethod(robot,i));
    }
#else
    for (int i = 0; i < layer_->getProgramLayer()->layers.size(); ++i) {
        functionsNode->LinkEndChild(saveMethod(i));
    }

#endif
    return functionsNode;
}

XMLElement * XMLWorldSaver::saveProgram ()
{
    int resize = 0;
    
    XMLElement * functionsNode = doc->NewElement("functions");
    functionsNode->SetAttribute("resizable", resize);
    
    for (int i = 0; i < layer_->getProgramLayer()->layers.size(); ++i) {
        functionsNode->LinkEndChild(saveMethod(i));
    }
    return functionsNode;
}

XMLElement * XMLWorldSaver::saveRobotMethod (int robot, int method) {
    MethodLayer* methodLayer = ((Robot4*)world_->robotAt(robot))->program->layers[method];
    
    //MethodLayer* methodLayer = layer_->getProgramLayer()->layers[layer];
    
    XMLElement * functionNode = doc->NewElement("function");
    functionNode->SetAttribute("name", methodLayer->name.c_str());
    functionNode->SetAttribute("hide", methodLayer->hidden);
    
    XMLElement * contentNode = doc->NewElement("content");
    std::string content = "";
    for (int j = 0; j < methodLayer->methods.size(); ++j) {
        switch(methodLayer->methods[j].methodID)
        {
            case CMDA : content+=('A');break;
            case CMDB : content+=('B');break;
            case CMDC : content+=('C');break;
            default : content+=('0'+methodLayer->methods[j].methodID);break;
        }
        switch(methodLayer->methods[j].backgroundType)
        {
            case _method::pmNone : content+=('0');break; // не существует
            case _method::pmNormal : content+=('1');break; // нормальный
            case _method::pmBlocked : content+=('2');break; // залоченный
            case _method::pmChangeBackground: content+=('3');break;
        }
        switch(methodLayer->methods[j].executeType)
        {
            case _method::pmExecuteAlways : content+=('0');break;
            case _method::pmExecuteOnTrue : content+=('1');break;
            case _method::pmExecuteOnFalse : content+=('2');break;
        }
    }
    
    XMLText * text = doc->NewText(content.c_str());
    contentNode->LinkEndChild(text);
    functionNode->LinkEndChild(contentNode);
    
    
    if (methodLayer->repeater == -2) {
        XMLElement * repeaterNode = doc->NewElement("repeater");
        repeaterNode->SetAttribute("use", false);
        repeaterNode->SetAttribute("value", 0);
        functionNode->LinkEndChild(repeaterNode);
    } else {
        XMLElement * repeaterNode = doc->NewElement("repeater");
        repeaterNode->SetAttribute("use", methodLayer->useRepeater);
        repeaterNode->SetAttribute("value", methodLayer->repeater);
        functionNode->LinkEndChild(repeaterNode);
    }
    
    if (methodLayer->condition == -1) {
        XMLElement * conditionNode = doc->NewElement("condition");
        conditionNode->SetAttribute("use", false);
        conditionNode->SetAttribute("value", 0);
        functionNode->LinkEndChild(conditionNode);
    } else {
        XMLElement * conditionNode = doc->NewElement("condition");
        conditionNode->SetAttribute("use", methodLayer->useCondition);
        conditionNode->SetAttribute("value", methodLayer->condition);
        functionNode->LinkEndChild(conditionNode);
    }
    
    XMLElement * sizesNode = doc->NewElement("sizes");
    sizesNode->SetAttribute("lockwidth", methodLayer->resizable);
    sizesNode->SetAttribute("lockheight", methodLayer->resizable);
    functionNode->LinkEndChild(sizesNode);
    
    XMLElement * sizeNode = doc->NewElement("size");
    sizeNode->SetAttribute("width", methodLayer->width);
    sizeNode->SetAttribute("height", methodLayer->height);
    sizesNode->LinkEndChild(sizeNode);
    
    XMLElement * maxsizeNode = doc->NewElement("maxsize");
    maxsizeNode->SetAttribute("width", methodLayer->width);
    maxsizeNode->SetAttribute("height", methodLayer->maxHeight);
    sizesNode->LinkEndChild(maxsizeNode);
    
    if(methodLayer->hidden)
    {
        XMLElement * startNode = doc->NewElement("start");
        XMLText * startText = doc->NewText(methodLayer->givenText.c_str());
        startText->LinkEndChild(text);
        startNode->LinkEndChild(startText);
        functionNode->LinkEndChild(startNode);
        XMLElement * endNode = doc->NewElement("end");
        XMLText * endText = doc->NewText(methodLayer->doText.c_str());
        endText->LinkEndChild(text);
        endNode->LinkEndChild(endText);
        functionNode->LinkEndChild(endNode);
    }
    
    return functionNode;
}

XMLElement * XMLWorldSaver::saveMethod (int layer)
{
    MethodLayer* methodLayer = layer_->getProgramLayer()->layers[layer];
    
    XMLElement * functionNode = doc->NewElement("function");
    functionNode->SetAttribute("name", methodLayer->name.c_str());
    functionNode->SetAttribute("hide", methodLayer->hidden);
    
    XMLElement * contentNode = doc->NewElement("content");
    std::string content = "";
    for (int j = 0; j < methodLayer->methods.size(); ++j) {
        switch(methodLayer->methods[j].methodID)
        {
            case CMDA : content+=('A');break;
            case CMDB : content+=('B');break;
            case CMDC : content+=('C');break;
            default : content+=('0'+methodLayer->methods[j].methodID);break;
        }
        switch(methodLayer->methods[j].backgroundType)
        {
            case _method::pmNone : content+=('0');break; // не существует
            case _method::pmNormal : content+=('1');break; // нормальный
            case _method::pmBlocked : content+=('2');break; // залоченный
            case _method::pmChangeBackground: content+=('3');break;
        }
        switch(methodLayer->methods[j].executeType)
        {
            case _method::pmExecuteAlways : content+=('0');break;
            case _method::pmExecuteOnTrue : content+=('1');break;
            case _method::pmExecuteOnFalse : content+=('2');break;
        }
    }

    XMLText * text = doc->NewText(content.c_str());
    contentNode->LinkEndChild(text);
    functionNode->LinkEndChild(contentNode);
    
    XMLElement * repeaterNode = doc->NewElement("repeater");
    repeaterNode->SetAttribute("use", methodLayer->useRepeater);
    repeaterNode->SetAttribute("value", methodLayer->repeater);
    functionNode->LinkEndChild(repeaterNode);
    
    XMLElement * conditionNode = doc->NewElement("condition");
    conditionNode->SetAttribute("use", methodLayer->useCondition);
    conditionNode->SetAttribute("value", methodLayer->condition);
    functionNode->LinkEndChild(conditionNode);
    
    XMLElement * sizesNode = doc->NewElement("sizes");
    sizesNode->SetAttribute("lockwidth", methodLayer->resizable);
    sizesNode->SetAttribute("lockheight", methodLayer->resizable);
    functionNode->LinkEndChild(sizesNode);

    XMLElement * sizeNode = doc->NewElement("size");
    sizeNode->SetAttribute("width", methodLayer->width);
    sizeNode->SetAttribute("height", methodLayer->height);
    sizesNode->LinkEndChild(sizeNode);

    XMLElement * maxsizeNode = doc->NewElement("maxsize");
    maxsizeNode->SetAttribute("width", methodLayer->width);
    maxsizeNode->SetAttribute("height", methodLayer->maxHeight);
    sizesNode->LinkEndChild(maxsizeNode);
    
    if(methodLayer->hidden)
    {
        XMLElement * startNode = doc->NewElement("start");
        XMLText * startText = doc->NewText(methodLayer->givenText.c_str());
        startText->LinkEndChild(text);
        startNode->LinkEndChild(startText);
        functionNode->LinkEndChild(startNode);
        XMLElement * endNode = doc->NewElement("end");
        XMLText * endText = doc->NewText(methodLayer->doText.c_str());
        endText->LinkEndChild(text);
        endNode->LinkEndChild(endText);
        functionNode->LinkEndChild(endNode);
    }
    
    return functionNode;
}

XMLElement * XMLWorldSaver::saveTask (int robot) {
    XMLElement * tasksNode = doc->NewElement("tasks");
    
    TaskList *taskList = world_->robotAt(robot)->getTaskList();
    
    if(taskList->getTask(pmPaintTask))
    {
        XMLElement * paintNode = doc->NewElement("paint");
        paintNode->SetAttribute("count", ((PaintTask *)taskList->getTask(pmPaintTask))->needToPaint);
        tasksNode->LinkEndChild(paintNode);
    }
    
    if(taskList->getTask(pmPositionTask))
    {
        XMLElement * positionNode = doc->NewElement("position");
        
        positionNode->SetAttribute("x", ((PositionTask *)taskList->getTask(pmPositionTask))->targetPoint.x);
        positionNode->SetAttribute("y", ((PositionTask *)taskList->getTask(pmPositionTask))->targetPoint.y);
        positionNode->SetAttribute("layer", ((PositionTask *)taskList->getTask(pmPositionTask))->targetLayer);
        
        tasksNode->LinkEndChild(positionNode);
    }
    
//    
//    if(world_->robotAt(robot)->getTask()->grassToPaint >= 1) {
//        XMLElement * paintNode = doc->NewElement("paint");
//        paintNode->SetAttribute("count", world_->robotAt(robot)->getTask()->grassToPaint);
//        tasksNode->LinkEndChild(paintNode);
//    }
//    if(world_->robotAt(robot)->getTask()->endPoint.x >= 0) {
//        XMLElement * positionNode = doc->NewElement("position");
//        
//        positionNode->SetAttribute("x", (int)world_->robotAt(robot)->getTask()->endPoint.x);
//        positionNode->SetAttribute("y", (int)world_->robotAt(robot)->getTask()->endPoint.y);
//        positionNode->SetAttribute("layer", (int)world_->robotAt(robot)->getTask()->endLayer);
//        
//        tasksNode->LinkEndChild(positionNode);
//    }
//    if(world_->robotAt(robot)->getTask()->furnaceToBurn >= 1) {
//        XMLElement * furnaceNode = doc->NewElement("furnace");
//        furnaceNode->SetAttribute("count", (int)world_->robotAt(robot)->getTask()->furnaceToBurn);
//        
//        tasksNode->LinkEndChild(furnaceNode);
//    }
    
    return tasksNode;
}

XMLElement * XMLWorldSaver::saveHint(int methodlayer)
{
    Hint* toSave = nullptr;
    if(methodlayer != -1)toSave = layer_->getProgramLayer()->layers[methodlayer]->getHint();
    else toSave = layer_->getHint();
    
    XMLElement * hintNode = doc->NewElement("hint");
    
    for (int i = 0; i < toSave->hintParts.size(); ++i)
    {
        XMLElement * hintPartNode = doc->NewElement("part");
        if (toSave->hintParts[i].type == TextHint)
        {
            hintPartNode->SetAttribute("type", "text");
            XMLText * text = doc->NewText(toSave->hintParts[i].hintText.c_str());
            hintPartNode->LinkEndChild(text);
        }
        else if (toSave->hintParts[i].type == GlobalHint)
        {
            hintNode->SetAttribute("target", "global");
            XMLText * text = doc->NewText(toSave->hintParts[i].hintText.c_str());
            hintNode->LinkEndChild(text);
        }
        else
        {
            hintPartNode->SetAttribute("type", "function");
            std::string str = "";
            for (int j = 0; j < toSave->hintParts[i].funcHint.size(); ++j)
            {
                char tmp[10] = "";
                switch(toSave->hintParts[i].funcHint[j].methodID)
                {
                    case -6 : sprintf(tmp, "A");break;
                    case -7 : sprintf(tmp, "B");break;
                    case -8 : sprintf(tmp, "C");break;
                    default : sprintf(tmp, "%d",toSave->hintParts[i].funcHint[j].methodID);break;
                }
                str+=tmp;
            }
            XMLText * text = doc->NewText(str.c_str());
            hintPartNode->LinkEndChild(text);
        }
        hintNode->LinkEndChild(hintPartNode);
    }
    return hintNode;
}

XMLElement* XMLWorldSaver::saveRobot (int robot)
{
    AbstractRobot * robot_ = world_->robotAt(robot);
    XMLElement * robotNode = nullptr;

    
    switch (robot_->type()) {
        case pmRobot4:
        {
            Robot4 * toSave = (Robot4*)robot_;
            robotNode = doc->NewElement("robot");
            robotNode->SetAttribute("x",(int)toSave->getCurPosition().x);
            robotNode->SetAttribute("y",(int)toSave->getCurPosition().y);
            robotNode->SetAttribute("layer",(int)toSave->getCurLayer());
            robotNode->SetAttribute("state",(int)toSave->getCurDirection());
            robotNode->SetAttribute("type", 0);
            if (world_->getBaseRobot() == toSave) {
                robotNode->SetAttribute("baserobot", "yes");
            }
            break;
        }

        case pmLamp4:
            break;
            
        default:
            break;
    }
    return robotNode;
}
