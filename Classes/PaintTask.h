//
//  PaintTask.h
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 21.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PaintTask__
#define __pictomir_cocos2d_x__PaintTask__

#include <iostream>
#include <cocos2d.h>
#include "PMSettings.h"
#include "Task.h"

struct _paint_task_update_data: public _task_update_data
{
    _paint_task_update_data(bool increase, bool needed):
        _task_update_data(pmPaintTask),
        increase(increase),
        needed(needed)
    {}
    
    bool increase;
    bool needed;
};

class PaintTask : public Task
{
private:
    SYNTHESIZE(int, needToPaint, NeedToPaint);
    int badPainted = 0;
    int painted = 0;
public:
    friend class XMLWorldSaver;
    
    PaintTask (AbstractRobot *robot, int needToPaint):
        Task(robot),
        needToPaint(needToPaint)
    {}
    
    static PaintTask *create(AbstractRobot *robot, int needToPaint)
    {
        return new PaintTask(robot,needToPaint);
    }
    
    virtual bool complete()
    {
        return painted == needToPaint && badPainted == 0;
    }
    
    virtual void clear()
    {
        painted = 0;
        badPainted = 0;
    }
    virtual void update(const std::shared_ptr<_task_update_data> &data)
    {
        if(((_paint_task_update_data *)data.get())->needed)
        {
            if(((_paint_task_update_data *)data.get())->increase)
                ++painted;
            else
                --painted;
        }
        else
        {
            if(((_paint_task_update_data *)data.get())->increase)
                ++badPainted;
            else
                --badPainted;
        }
    }
    
    ~PaintTask()
    {
        
    }
};


#endif /* defined(__pictomir_cocos2d_x__PaintTask__) */
