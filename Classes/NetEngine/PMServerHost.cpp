//
//  PMServer.cpp
//  pictomir
//
//  Created by Anton Dedkov on 30.01.14.
//
//

#include "PMServerHost.h"
PMServerHost * PMServerHost::_self = nullptr;

PMServerHost::PMServerHost(enet_uint16 port): PMHost(port)
{
    this->cocos2d::Node::init();
    
    schedule(schedule_selector(PMServerHost::messageHandler), 0.1f);
    this->retain();
    resume();
}

PMServerHost::~PMServerHost()
{
    enet_deinitialize();
}

void PMServerHost::sendInitSingleplayerGame(const vector<PMClient>& clients, short world, short level)
{
    _init_singleplayer_game_sr_cl * data = new _init_singleplayer_game_sr_cl;
    
    data->priority = HIGH_PRIORITY;
    data->type = INIT_SINGLEPLAYER_GAME;
    data->size = sizeof(_init_singleplayer_game_sr_cl) - sizeof(pt_header);
    data->world = world;
    data->level = level;
    for(auto i = clients.begin(); i != clients.end(); i++)
    {
        sendTo(*i, data, sizeof(_init_singleplayer_game_sr_cl));
    }
    delete data;
}

void PMServerHost::sendInitMultiplayerGame(const PMClient client, short clientNeeded, short world, short level)
{
    _init_multiplayer_game_sr_cl data;
    data.type = INIT_MULTIPLAYER_GAME;
    data.priority = HIGH_PRIORITY;
    data.size = sizeof(_init_multiplayer_game_sr_cl) - sizeof(pt_header);
    data.world = world;
    data.level = level;
    data.clientNeeded = clientNeeded;
    
    sendTo(client, &data, sizeof(_init_multiplayer_game_sr_cl));
}

void PMServerHost::sendConnectMultiplayer(const PMClient client, ENetAddress address, short robot)
{
    _connect_multiplayer_game_sr_cl data;
    data.type = CONNECT_MULTIPLAYER_GAME;
    data.priority = HIGH_PRIORITY;
    data.size = sizeof(_connect_multiplayer_game_sr_cl) - sizeof(pt_header);
    data.robot = robot;
    data.address = address;
    data.address.port = PM_MULTIPLAYER_PORT;
    
    sendTo(client, &data, sizeof(_connect_multiplayer_game_sr_cl));
}

void PMServerHost::messageHandler(float dt)
{
    if(!messageDeque_.empty())
    {
        pt_header* packet = messageDeque_.front();
        messageDeque_.pop_front();
        
        switch (packet->type)
        {
            case CLIENT_CONNECTED:
                
                for(std::vector<PMClient>::iterator i = clients_.begin(); i != clients_.end(); ++i)
                {
                    if(i->id_ == ((_client_info*)packet)->id)
                    {
                        i->peer_->timeoutLimit = 1;
                        (uiObj_->*uiConnect_)(&*i);
                        break;
                    }
                }

                break;
                
            case CLIENT_DISCONNECTED:
                
                for(std::vector<PMClient>::iterator i = clients_.begin(); i != clients_.end(); ++i)
                {
                    if(i->id_ == ((_client_info*)packet)->id)
                    {
                        (uiObj_->*uiDisconnect_)(&*i);
                        clients_.erase(i);
                        break;
                    }
                }

                break;
                
            default:
                break;
        }
    }
    for(vector<PMClient>::iterator i = clients_.begin(); i != clients_.end(); i++)
    {
        if(!i->messageDeque_.empty())
        {
            pt_header* packet = i->messageDeque_.front();
            i->messageDeque_.pop_front();
            
            switch (packet->type)
            {
                default:
                    break;
            }
        }
    }

    

}

void PMServerHost::registerRefreshUIFunc(cocos2d::Ref* obj, CLIENT_CONN_FUNCTION connectFunc, CLIENT_CONN_FUNCTION disconnectFunc)
{
    uiObj_ = obj;
    uiConnect_ = connectFunc;
    uiDisconnect_= disconnectFunc;
}