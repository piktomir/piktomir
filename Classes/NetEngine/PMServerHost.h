//
//  PMServer.h
//  pictomir
//
//  Created by Anton Dedkov on 30.01.14.
//
//

#ifndef __pictomir__PMServer__
#define __pictomir__PMServer__

#include <cocos2d.h>
#include "PMNetwork.h"

using namespace cocos2d;

typedef void (cocos2d::Ref::* CLIENT_CONN_FUNCTION)(PMClient *);

class PMServerHost: public PMHost, public Node
{
public:
    
    static PMServerHost *_self;
    
    static PMServerHost * instance()
    {
        if(!_self)
            _self = new PMServerHost(PM_SERVICE_PORT);
        
        return _self;
    }
    static void destroy()
    {
        delete _self;
    }
    
    PMServerHost(enet_uint16 port);
    ~PMServerHost();
    
    void sendInitSingleplayerGame(const vector<PMClient>& clients, short world, short level);
    void sendInitMultiplayerGame(const PMClient client, short clientNeeded, short world, short level);
    void sendConnectMultiplayer(const PMClient client, ENetAddress address, short robot);
    
    void messageHandler(float dt);
    
    void registerRefreshUIFunc(cocos2d::Ref* obj, CLIENT_CONN_FUNCTION connectFunc, CLIENT_CONN_FUNCTION disconnectFunc);
    void RefreshUI();
        
private:
    
    cocos2d::Ref* uiObj_;
    CLIENT_CONN_FUNCTION uiConnect_;
    CLIENT_CONN_FUNCTION uiDisconnect_;
};

#endif /* defined(__pictomir__PMServer__) */
