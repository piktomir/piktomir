//
//  PMHost.h
//  pictomir-cocos2d-x
//
//  Created by Anton Dedkov on 14.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PMHost__
#define __pictomir_cocos2d_x__PMHost__

#include <iostream>
#include <thread>
#include <vector>
#include <deque>
#include "enet/enet.h"
#include "PMNetwork.h"
#include "PMClient.h"
#include "PMSession.h"

using std::vector;
using std::deque;
using std::thread;

class PMHost
{
public:
    
    PMHost(enet_uint16 port);
    ~PMHost();
    
    void networkEventHandler();
    
    vector<PMClient>* getClients();
    
    void sendTo(const PMClient& recipient, const void* data, size_t size, bool doFlush = true);
    void sendBroadcast(const void* data, size_t size, bool doFlush = true);
    void flush();
    
protected:
    
    ENetHost * host_;
    ENetEvent * event_;
    ENetPeer * peer_;
    ENetAddress * address_;
    
    vector<PMClient> clients_;
    
    deque<pt_header *> messageDeque_;
    
    thread * eventThread_;
};

#endif /* defined(__pictomir_cocos2d_x__PMHost__) */
