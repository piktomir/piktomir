//
//  mapEditorsMenu.h
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 11.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__mapEditorsMenu__
#define __pictomir_cocos2d_x__mapEditorsMenu__

#include "cocos2d.h"
#include "PMSettings.h"
#include "PMTextureCache.h"
USING_NS_CC;

enum PMMap4Edit {
    None = -1,
    EditingNone4,
    EditingGrass4,
    EditingWater4,
    EditingPaintingGrass4,
    EditingPaintedGrass4,
    
    EditingLeftWall4,
    EditingUpWall4,
    EditingNoLeftWall4,
    EditingNoUpWall4
};

class map4EditorsMenu : public cocos2d::Ref {
    SYNTHESIZE(PMMap4Edit, active, Active );
    
    SYNTHESIZE(cocos2d::Menu*, editMenu, EditMenu);
    
    cocos2d::MenuItemSprite * editors[5];
    cocos2d::MenuItemSprite * activeEditor;
    
public:
    static map4EditorsMenu* instanse;
    static map4EditorsMenu* getInstanse();
    
    map4EditorsMenu () {
        active = None;
        editMenu = cocos2d::Menu::create();
        
        editMenu->setScaleY(0.5);
        
        activeEditor = cocos2d::MenuItemSprite::create(Sprite::create("MapsPics/Active.png"), Sprite::create("MapsPics/Active.png"), Sprite::create("MapsPics/Active_Disabled.png"));
        activeEditor->setScaleY(2);
        activeEditor->setPosition(Point(1000,1000));
        editMenu->addChild(activeEditor);
        
        editors[0] = cocos2d::MenuItemSprite::create(PMTextureCache::instanse()->getMapGrassTile(MapElement::GreenGrass, DISABLED_STATE),
                                                     PMTextureCache::instanse()->getMapGrassTile(MapElement::GreenGrass, DISABLED_STATE),
                                                     PMTextureCache::instanse()->getMapGrassTile(MapElement::GreenGrass, DISABLED_STATE),
                                                     CC_CALLBACK_1(map4EditorsMenu::selectAction,this));
        editors[0]->setPosition(Point(0,0));
        editors[0]->setRotation(45);
        editMenu->addChild(editors[0]);
        
        editors[1] = cocos2d::MenuItemSprite::create(
                            PMTextureCache::instanse()->getMapGrassTile(MapElement::GreenGrass),
                            PMTextureCache::instanse()->getMapGrassTile(MapElement::GreenGrass),
                            PMTextureCache::instanse()->getMapGrassTile(MapElement::GreenGrass, DISABLED_STATE),
                            CC_CALLBACK_1(map4EditorsMenu::selectAction,this));
        editors[1]->setPosition(Point(100,0));
        editors[1]->setRotation(45);
        editMenu->addChild(editors[1]);
        
        editors[2] = cocos2d::MenuItemSprite::create(PMTextureCache::instanse()->getMapGrassTile(MapElement::BlueGrass),
                                                     PMTextureCache::instanse()->getMapGrassTile(MapElement::BlueGrass),
                                                     PMTextureCache::instanse()->getMapGrassTile(MapElement::BlueGrass, DISABLED_STATE),
                                                     CC_CALLBACK_1(map4EditorsMenu::selectAction, this));
        editors[2]->setPosition(Point(200,0));
        editors[2]->setRotation(45);
        editMenu->addChild(editors[2]);
        
        editors[3] = cocos2d::MenuItemSprite::create(PMTextureCache::instanse()->getMapGrassTile(MapElement::BrokenGrass),
                                                     PMTextureCache::instanse()->getMapGrassTile(MapElement::BrokenGrass),
                                                     PMTextureCache::instanse()->getMapGrassTile(MapElement::BrokenGrass, DISABLED_STATE),
                                                     CC_CALLBACK_1(map4EditorsMenu::selectAction, this));
        editors[3]->setPosition(Point(0,-100));
        editors[3]->setRotation(45);
        editMenu->addChild(editors[3]);
        
        editors[4] = cocos2d::MenuItemSprite::create(PMTextureCache::instanse()->getMapGrassTile(MapElement::RepairedGrass),
                                                     PMTextureCache::instanse()->getMapGrassTile(MapElement::RepairedGrass),
                                                     PMTextureCache::instanse()->getMapGrassTile(MapElement::RepairedGrass, DISABLED_STATE),
                                                     CC_CALLBACK_1(map4EditorsMenu::selectAction, this));
        editors[4]->setPosition(Point(100,-100));
        editors[4]->setRotation(45);
        editMenu->addChild(editors[4]);
        
        editMenu->retain();
    }
    
    void selectAction (cocos2d::Ref * sender) {
        
        
        PMMap4Edit tmp = None;
        for (int i = 0; i < 5; ++i) {
            if (sender == editors[i]) {
                tmp = (PMMap4Edit)i;
                break;
            }
        }
        
        if (active == tmp) {
            active = None;
            activeEditor->setPosition(Point(1000,1000));
        } else {
            active = tmp;
            activeEditor->setPosition(((MenuItemImage*)sender)->getPosition());
        }
    }
    
    void setEnabled (bool flag) {
        for (int i = 0; i < 5; ++i) {
            editors[i]->setEnabled(flag);
        }
        activeEditor->setEnabled(flag);
    }
};

#endif /* defined(__pictomir_cocos2d_x__mapEditorsMenu__) */
