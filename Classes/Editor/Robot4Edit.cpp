//
//  Robot4Edit.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 15.07.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "Robot4Edit.h"
#include "Robot4.h"
#include "cocos2d.h"
#include "MapEditorGameLayer.h"
#include "PMCombobox.h"
#include "PositionTask.h"
#include "PaintTask.h"
#include "mapEditorsMenu.h"

USING_NS_CC;

void Robot4Edit::setRobot(Robot4* robot) {
    robot_ = robot;
    
    state = robot->getCurDirection();
    
    if (robot->getTaskList()->getTask(pmPositionTask)) {
        endXCnt_ = ((PositionTask*)robot->getTaskList()->getTask(pmPositionTask))->getTargetPoint().x;
        endYCnt_ = ((PositionTask*)robot->getTaskList()->getTask(pmPositionTask))->getTargetPoint().y;
    } else {
        endXCnt_ = -1;
        endYCnt_ = -1;
    }
    
    if (robot->getTaskList()->getTask(pmPaintTask)) {
        endGrassCnt_ = ((PaintTask*)robot->getTaskList()->getTask(pmPaintTask))->getNeedToPaint();
    } else {
        endGrassCnt_ = 0;
    }
    
    
    char tmp[10] = "";
    sprintf(tmp,"%d", endXCnt_);
    endXCnt->setString(tmp);
    sprintf(tmp,"%d", endYCnt_);
    endYCnt->setString(tmp);
    sprintf(tmp,"%d", endGrassCnt_);
    paintGrassCnt->setString(tmp);
    paintSlider->setValue(endGrassCnt_);
    
    combobox_->selectItemAtIndex(state);
    
    switch (state) {
        case 0:
            setDown(nullptr);
            break;
        case 1:
            setRight(nullptr);
            break;
        case 2:
            setUp(nullptr);
            break;
        case 3:
            setLeft(nullptr);
            break;
    }
}

//////////////////////////////////////////////////////

void Robot4Edit::plusEndX (cocos2d::Ref * sender) {
    if (combobox) {
        return;
    }
    endXCnt_ ++;
    if (endXCnt_ == ((Map4*)MapEditorGameLayer::getInstanse()->getWorld()->getMap())->getWidth()) {
        endXCnt_--;
        return;
    }
    if (endXCnt_ == -1) {
        endXCnt_++;
    }
    robot_->getTaskList()->addTask(pmPositionTask, PositionTask::create(robot_, 0, Point(endXCnt_+1,endYCnt_+1)));
    
    char tmp[10] = "";
    sprintf(tmp,"%d", endXCnt_+1);
    endXCnt->setString(tmp);
}

void Robot4Edit::minusEndX (cocos2d::Ref * sender) {
    if (combobox) {
        return;
    }
    endXCnt_ --;
    if (endXCnt_ == -1) {
        endXCnt_--;
    }
    if(endXCnt_ == -3) {endXCnt_++;return;}
    robot_->getTaskList()->addTask(pmPositionTask, PositionTask::create(robot_, 0, Point(endXCnt_+1,endYCnt_+1)));
    char tmp[10] = "";
    sprintf(tmp,"%d", endXCnt_+1);
    endXCnt->setString(tmp);
}

void Robot4Edit::plusEndY (cocos2d::Ref * sender) {
    if (combobox) {
        return;
    }
    endYCnt_ ++;
    if (endYCnt_ == ((Map4*)MapEditorGameLayer::getInstanse()->getWorld()->getMap())->getHeight()) {
        endYCnt_--;
        return;
    }
    if (endYCnt_ == -1) {
        endYCnt_++;
    }
    robot_->getTaskList()->addTask(pmPositionTask, PositionTask::create(robot_, 0, Point(endXCnt_+1,endYCnt_+1)));
    char tmp[10] = "";
    sprintf(tmp,"%d", endYCnt_+1);
    endYCnt->setString(tmp);
}

void Robot4Edit::minusEndY (cocos2d::Ref * sender) {
    if (combobox) {
        return;
    }
    endYCnt_ --;
    
    if (endYCnt_ == -1) {
        endYCnt_--;
    }
    if(endYCnt_ == -3) {endYCnt_++;return;}
    robot_->getTaskList()->addTask(pmPositionTask, PositionTask::create(robot_, 0, Point(endXCnt_+1,endYCnt_+1)));
    
    char tmp[10] = "";
    sprintf(tmp,"%d", endYCnt_+1);
    endYCnt->setString(tmp);
}

void setMaxGrass () {
    
}

void Robot4Edit::setDown (cocos2d::Ref * sender) {
    if (types != nullptr) {
        removeChild(types,true);
        types = nullptr;
        
        combobox = false;
    }
    state = 0;
    
    robot_->setPosition(robot_->getCurPosition(), robot_->getCurLayer()/*layerCnt_*/, state);
    MapEditorGameLayer::getInstanse()->getWorld()->getMap()->update();
}

void Robot4Edit::setUp (cocos2d::Ref * sender) {
    if (types != nullptr) {
        removeChild(types,true);
        types = nullptr;
        
        combobox = false;
    }
    state = 2;
    
    robot_->setPosition(robot_->getCurPosition(), robot_->getCurLayer()/*layerCnt_*/, state);
    MapEditorGameLayer::getInstanse()->getWorld()->getMap()->update();
}

void Robot4Edit::setLeft (cocos2d::Ref * sender) {
    if (types != nullptr) {
        removeChild(types,true);
        types = nullptr;
        
        combobox = false;
    }
    state = 3;
    
    robot_->setPosition(robot_->getCurPosition(), robot_->getCurLayer()/*layerCnt_*/, state);
    MapEditorGameLayer::getInstanse()->getWorld()->getMap()->update();
}

void Robot4Edit::setRight (cocos2d::Ref * sender) {
    if (types != nullptr) {
        removeChild(types,true);
        types = nullptr;
        
        combobox = false;
    }
    state = 1;
    
    robot_->setPosition(robot_->getCurPosition(), robot_->getCurLayer()/*layerCnt_*/, state);
    MapEditorGameLayer::getInstanse()->getWorld()->getMap()->update();
}

void Robot4Edit::showRobotStates (cocos2d::Ref * sender) {
    int height = 120;
    if (types != nullptr) {
        removeChild(types,true);
        types = nullptr;
        
        combobox = false;
        return;
    }
    
    combobox = true;
    types = LayerColor::create(Color4B(144, 144, 144, 200), 180, height);
    types->setPosition((((Node*)sender)->getPosition()+Point(-100,-120)));
    
    
    MenuItemLabel* downLabel = MenuItemLabel::create(Label::create(LocalizedString("Down"), PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(Robot4Edit::setDown, this));
    
    downLabel->setAnchorPoint(Point(0,1));
    downLabel->setPosition(Point(10,height - 5));
    
    
    MenuItemLabel* rightLabel = MenuItemLabel::create(Label::create(LocalizedString("Right"), PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(Robot4Edit::setRight, this));
    
    rightLabel->setAnchorPoint(Point(0,1));
    rightLabel->setPosition(Point(10,height - 30));
    
    
    MenuItemLabel* upLabel = MenuItemLabel::create(Label::create(LocalizedString("Up"), PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(Robot4Edit::setUp, this));
    
    upLabel->setAnchorPoint(Point(0,1));
    upLabel->setPosition(Point(10,height - 55));
    
    MenuItemLabel* leftLabel = MenuItemLabel::create(Label::create(LocalizedString("Left"), PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(Robot4Edit::setLeft, this));
    leftLabel->setAnchorPoint(Point(0,1));
    leftLabel->setPosition(Point(10,height - 80));
    
    Menu * menu = Menu::create(downLabel, rightLabel, upLabel, leftLabel, nullptr);
    menu->setPosition( Point::ZERO);
    types->addChild(menu);
    
    addChild(types);
}

void Robot4Edit::onShowCombobox(cocos2d::Ref* sender)
{
    for (int i = 0; i < menu->getChildren().size(); ++i) {
        ((MenuItem*)menu->getChildren().at(i))->setEnabled(false);
    }
}

void Robot4Edit::onEndCombobox(cocos2d::Ref* sender)
{
    switch (((PMComboBox*)sender)->getSelectedItemIndex()) {
        case 0:
            setDown(nullptr);
            break;
        case 1:
            setRight(nullptr);
            break;
        case 2:
            setUp(nullptr);
            break;
        case 3:
            setLeft(nullptr);
            break;
    }
    
    for (int i = 0; i < menu->getChildren().size(); ++i) {
        ((MenuItem*)menu->getChildren().at(i))->setEnabled(true);
    }
}

void Robot4Edit::onCloseCombobox(cocos2d::Ref* sender)
{
    for (int i = 0; i < menu->getChildren().size(); ++i) {
        ((MenuItem*)menu->getChildren().at(i))->setEnabled(true);
    }
}

bool Robot4Edit::init()
{
    if ( !LayerColor::init() )
    {
        return false;
    }
    types = nullptr;
    Label * name = Label::create(LocalizedString("Robot"), PMSettings::instance()->getFont(), 22);
    name->setAnchorPoint(Point(0,1));
    name->setPosition(Point(20,-5));
    addChild(name);
    
    sizeHeight = -25;
    
    Label * state = Label::create(LocalizedString("State"), PMSettings::instance()->getFont(), 18);
    
    state->setAnchorPoint(Point(0,1));
    state->setPosition(Point(20,sizeHeight-5));
    
    std::vector<std::string> values;
    values.push_back(LocalizedString("Down"));
    values.push_back(LocalizedString("Right"));
    values.push_back(LocalizedString("Up"));
    values.push_back(LocalizedString("Left"));
    
    combobox_ = PMComboBox::create(values, this, menu_selector(Robot4Edit::onShowCombobox),
                                   menu_selector(Robot4Edit::onEndCombobox), menu_selector(Robot4Edit::onCloseCombobox),
                                   18.0, PMSettings::instance()->getFont().c_str());
    combobox_->setPosition(Point(100,sizeHeight-35));
    addChild(combobox_,100);
    
    sizeHeight-= 25;
    
    /////////////////////
    
    Label * taskLabel = Label::create(LocalizedString("Task"), PMSettings::instance()->getFont(), 22);
    taskLabel->setAnchorPoint(Point(0,1));
    taskLabel->setPosition(Point(20,sizeHeight-5));
    addChild(taskLabel);
    sizeHeight -= 25;
    
    Label * endX = Label::create("x", PMSettings::instance()->getFont(), 18);
    endX->setAnchorPoint(Point(0,1));
    endX->setPosition(Point(20,sizeHeight-5));
    
    MenuItemLabel* endXPlus = MenuItemLabel::create(Label::create("+", PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(Robot4Edit::plusEndX, this));
    endXPlus->setAnchorPoint(Point(0,1));
    endXPlus->setPosition(Point(155,sizeHeight-5));
    
    MenuItemLabel* endXMinus = MenuItemLabel::create(Label::create("-", PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(Robot4Edit::minusEndX, this));
    endXMinus->setAnchorPoint(Point(0,1));
    endXMinus->setPosition(Point(115,sizeHeight-5));
    
    endXCnt = Label::create("1", PMSettings::instance()->getFont(), 18);
    endXCnt->setAnchorPoint(Point(0,1));
    endXCnt->setPosition(Point(130,sizeHeight-5));
    
    
    Label * endY = Label::create("y", PMSettings::instance()->getFont(), 18);
    endY->setAnchorPoint(Point(0,1));
    endY->setPosition(Point(20,sizeHeight-30));
    
    MenuItemLabel* endYPlus = MenuItemLabel::create(Label::create("+", PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(Robot4Edit::plusEndY, this));
    endYPlus->setAnchorPoint(Point(0,1));
    endYPlus->setPosition(Point(155,sizeHeight-30));
    
    MenuItemLabel* endYMinus = MenuItemLabel::create(Label::create("-", PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(Robot4Edit::minusEndY, this));
    endYMinus->setAnchorPoint(Point(0,1));
    endYMinus->setPosition(Point(115,sizeHeight-30));
    
    endYCnt = Label::create("1", PMSettings::instance()->getFont(), 18);
    endYCnt->setAnchorPoint(Point(0,1));
    endYCnt->setPosition(Point(130,sizeHeight-30));

    
    Label * paintGrass = Label::create(LocalizedString("Paint"), PMSettings::instance()->getFont(), 18);
    paintGrass->setAnchorPoint(Point(0,1));
    paintGrass->setPosition(Point(20,sizeHeight-55));
    
    paintGrassCnt = Label::create("1", PMSettings::instance()->getFont(), 18);
    paintGrassCnt->setAnchorPoint(Point(0,1));
    paintGrassCnt->setPosition(Point(65,sizeHeight-54));
    
    paintSlider = ControlSlider::create(PICT_PATH "System/sliderTrack.png",
                                        PICT_PATH "System/sliderProgress.png",
                                        PICT_PATH "System/sliderThumb.png");
    
    paintSlider->addTargetWithActionForControlEvents(this, cccontrol_selector(Robot4Edit::setPaint),
                                                     Control::EventType::VALUE_CHANGED);
    
    paintSlider->setAnchorPoint( Point::ZERO );
    paintSlider->setPosition( Point(85,sizeHeight-80) );
    paintSlider->setScaleX(0.8);
    paintSlider->setMaximumValue( ((Map4*)MapEditorGameLayer::getInstanse()->getWorld()->getMap())->getGrassCount() );
    paintSlider->setMinimumValue( 0.0 );
    paintSlider->setValue(PMSettings::instance()->getBackgroundVolume());
    
    addChild(paintSlider);
    
    MenuItemLabel* showFunctions = MenuItemLabel::create(Label::create(LocalizedString("ShowFunctions"), PMSettings::instance()->getFont(), 18),CC_CALLBACK_1(Robot4Edit::showFunctions, this ));
    showFunctions->setAnchorPoint(Point(0,1));
    showFunctions->setPosition(Point(20,sizeHeight-80));
    
    menu = Menu::create( endXPlus, endXMinus, endYPlus, endYMinus, showFunctions, nullptr);
    menu->setPosition( Point::ZERO);
    addChild(menu);
    
    addChild(state);
    
    addChild(endX);
    addChild(endY);
    addChild(paintGrass);
    
    addChild(endXCnt);
    addChild(endYCnt);
    addChild(paintGrassCnt);
    
    return true;
}

void Robot4Edit::setPaint (cocos2d::Ref *sender, cocos2d::extension::Control::EventType controlEvent) {
    if (((Map4*)MapEditorGameLayer::getInstanse()->getWorld()->getMap())->getGrassCount() != paintSlider->getMaximumValue()) paintSlider->setMaximumValue( ((Map4*)MapEditorGameLayer::getInstanse()->getWorld()->getMap())->getGrassCount() );
    
    int value = paintSlider->getValue();
    
    endGrassCnt_ = value;
    
    if(robot_) {
        if (robot_->getTaskList()) {
            if (robot_->getTaskList()->getTask(pmPaintTask)) {
                ((PaintTask*)robot_->getTaskList()->getTask(pmPaintTask))->setNeedToPaint(endGrassCnt_);
            }
        }
    }
    
    char tmp[10] = "";
    sprintf(tmp,"%d", endGrassCnt_);
    paintGrassCnt->setString(tmp);
}

void Robot4Edit::showFunctions (cocos2d::Ref*) {
    Robot4 * robot = (Robot4*)MapEditorGameLayer::getInstanse()->getWorld()->robotAt(parent_->getActiveRobot());
    robot->program->setVisible(false);
    
    parent_->setActiveRobot(index);
    
    robot = (Robot4*)MapEditorGameLayer::getInstanse()->getWorld()->robotAt(parent_->getActiveRobot());
    robot->program->setVisible(true);
    
    MapEditorGameLayer::getInstanse()->setProgramLayer(robot->program);
    
    
    map4EditorsMenu::getInstanse()->getEditMenu()->removeFromParent();
    robot->program->addChild(map4EditorsMenu::getInstanse()->getEditMenu(),10);
}

void Robot4Edit::draw(cocos2d::Renderer* renderer, const kmMat4 &transform, bool transformUpdated)
{
    lineDrawCommand.init(_globalZOrder);
    lineDrawCommand.func = CC_CALLBACK_0(Robot4Edit::onDraw, this, transform, transformUpdated);
    Director::getInstance()->getRenderer()->addCommand(&lineDrawCommand);
    
    LayerColor::draw(renderer, transform, transformUpdated);
}

void Robot4Edit::onDraw(const kmMat4 &transform, bool transformUpdated)
{
    LayerColor::onDraw(transform, transformUpdated);
    kmMat4 oldMat;
    kmGLGetMatrix(KM_GL_MODELVIEW, &oldMat);
    kmGLLoadMatrix(&_modelViewTransform);
    
    DrawPrimitives::init();
    DrawPrimitives::setDrawColor4F(255,255,255,255);
    glLineWidth(2.0f);
    //draw lines
    DrawPrimitives::drawLine(Point(200,-200), Point(200,0));
    kmGLLoadMatrix(&oldMat);
}
