//
//  PMPopoverConditionbutton.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 17.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "PMPopoverConditionbutton.h"
#include "MethodLayer.h"
#include "PMTextureCache.h"
#include "RobotMethod.h"
#include "ProgramLayer.h"

const int PopoverWidth = 210;
const int PopoverHeight = 40;

PMPopoverConditionButton * PMPopoverConditionButton::instanse = nullptr;

PMPopoverConditionButton* PMPopoverConditionButton::create(int width, int realHeight, Node* parent) {
    
    if(!instanse) {
        instanse = new PMPopoverConditionButton;
        
        instanse->init();
        instanse->setup(width, realHeight);
        
        parent->addChild(instanse, 20);
    }
    
    instanse->parent_ = (MethodLayer*)parent;
    
    return instanse;
}

PMPopoverConditionButton* PMPopoverConditionButton::getInstanse () {
    return instanse;
}

bool PMPopoverConditionButton::init() {
    if ( !CCLayerColor::init() )
    {
        return false;
    }
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(PMPopoverConditionButton::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(PMPopoverConditionButton::touchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    
    setContentSize(Size(PopoverWidth, PopoverHeight));
    setColor(Color3B(144, 144, 144));
    setOpacity(230);
    setAnchorPoint(Point(0.5, 0));
    
    Sprite *Off = PMTextureCache::instanse()->getRobotConditionSprite(0, DISABLED_STATE);//PMTextureCache::instanse()->getConditionBackgroundSprite_Disabled();
    Off->setPosition(Point(5,5));
    Off->setAnchorPoint( Point::ZERO);
    Off->setScaleY(0.7);
    Off->setScaleX(0.7);
    addChild(Off);
    
    Sprite *On = PMTextureCache::instanse()->getRobotConditionSprite(0, NORMAL_STATE);//PMTextureCache::instanse()->getConditionBackgroundSprite();
    On->setPosition(Point(40,5));
    On->setAnchorPoint( Point::ZERO);
    On->setScaleY(0.7);
    On->setScaleX(0.7);
    addChild(On);
    
    for (int i = 1; i <= 4 ; ++i)
    {
        Sprite *r1 = PMTextureCache::instanse()->getRobotConditionSprite(i, NORMAL_STATE);
        r1->setPosition(Point(5+35*(i+1),5));
        r1->setAnchorPoint( Point::ZERO);
        r1->setScaleY(0.7);
        r1->setScaleX(0.7);
        addChild(r1);
    }
    
    return true;
}

void PMPopoverConditionButton::setup(int width, int realHeight) {
    showed = true;
    setVisible(true);
    int Y = realHeight - 5;
    setPosition( Point(20, Y ) );
}

bool PMPopoverConditionButton::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent) {
    return true;
}

void PMPopoverConditionButton::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent) {
    showed = false;
    Point touchPoint = convertTouchToNodeSpace(touch);
    
    if (touchPoint.x < PopoverWidth && touchPoint. x > 0 &&
        touchPoint.y < PopoverHeight && touchPoint.y > 0) {
        int ans = touchPoint.x/35;
        printf("%d\n",ans);
        
        parent_->setCondition (ans-1);
    } else {
        
    }
    parent_->getParentLayer()->setEnabled(true);
    removeFromParent();
    instanse = nullptr;
    setVisible(false);
    
    parent_->removeAllChildrenWithCleanup(true);
    parent_->drawFunctions();
}

