//
//  PMPopoverMethodButton.cpp
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 23.10.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "PMPopoverMethodButton.h"
#include "MethodLayer.h"
#include "PMTextureCache.h"
#include "RobotMethod.h"
#include "ProgramLayer.h"

const int PopoverWidth = 145;
const int PopoverHeight = 40;

PMPopoverMethodButton * PMPopoverMethodButton::instanse = nullptr;

PMPopoverMethodButton* PMPopoverMethodButton::create(int button, int width, int realHeight, Node* parent) {
    
    if(!instanse) {
        instanse = new PMPopoverMethodButton;
        
        instanse->init();
        instanse->setup(button, width, realHeight);
        
        parent->addChild(instanse, 20);
        instanse->setMethodIndex (((MethodLayer*)parent)->getIndex());
    }
//    else {
//        if (instanse->getMethodIndex() != ((MethodLayer*)parent)->getIndex()) {
//            instanse->setMethodIndex (((MethodLayer*)parent)->getIndex());
//            instanse->removeFromParent();
//            parent->addChild(instanse, 20);
//        }
//        
//        instanse->setup(button, width, realHeight);
//    }
    instanse->parent_ = (MethodLayer*)parent;
    
    return instanse;
}

PMPopoverMethodButton* PMPopoverMethodButton::getInstanse () {
    return instanse;
}

bool PMPopoverMethodButton::init() {
    if ( !CCLayerColor::init() )
    {
        return false;
    }
    
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(PMPopoverMethodButton::touchBegan, this);
    listener->onTouchEnded = CC_CALLBACK_2(PMPopoverMethodButton::touchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    setContentSize(Size(PopoverWidth, PopoverHeight));
    setColor(Color3B(144, 144, 144));
    setOpacity(230);
    setAnchorPoint(Point(0.5, 0));
    
    Sprite *Off = PMTextureCache::instanse()->getRobotMethodSprite(0, _method::pmExecuteAlways, DISABLED_STATE);
    //PMTextureCache::instanse()->getMethodBackgroundAlwaysSprite_Disabled();
    Off->setPosition(Point(5,5));
    Off->setAnchorPoint( Point::ZERO);
    Off->setScaleY(0.7);
    Off->setScaleX(0.7);
    addChild(Off);
    
    Sprite *Always = PMTextureCache::instanse()->getRobotMethodSprite(0, _method::pmExecuteAlways, NORMAL_STATE);//PMTextureCache::instanse()->getMethodBackgroundAlwaysSprite();
    Always->setPosition(Point(75,5));
    Always->setAnchorPoint( Point::ZERO);
    Always->setScaleY(0.7);
    Always->setScaleX(0.7);
    addChild(Always);
    
    Sprite *True = PMTextureCache::instanse()->getRobotMethodSprite(0, _method::pmExecuteOnTrue, NORMAL_STATE);// = PMTextureCache::instanse()->getMethodBackgroundOnTrueSprite();
    True->setPosition(Point(40,5));
    True->setAnchorPoint( Point::ZERO);
    True->setScaleY(0.7);
    True->setScaleX(0.7);
    addChild(True);
    
    Sprite *False = PMTextureCache::instanse()->getRobotMethodSprite(0, _method::pmExecuteOnFalse, NORMAL_STATE);// = PMTextureCache::instanse()->getMethodBackgroundOnFalseSprite();
    False->setPosition(Point(110,5));
    False->setAnchorPoint( Point::ZERO);
    False->setScaleY(0.7);
    False->setScaleX(0.7);
    addChild(False);
    
    return true;
}

void PMPopoverMethodButton::setup(int button, int width, int realHeight) {
    index_ = button;
    showed = true;
    setVisible(true);
    int Y = realHeight - BUTTON_TILE_SIZE - (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET);
    setPosition( Point((BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET) * (button%width + 1.0/2.0) - PopoverWidth / 2 + 20, Y - (button/width - 1) * (BUTTON_TILE_SIZE + METHODLAYER_TILE_OFFSET)) );
}

bool PMPopoverMethodButton::touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent) {
    return true;
}

void PMPopoverMethodButton::touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent) {
    showed = false;
    Point touchPoint = convertTouchToNodeSpace(touch);
    
    if (touchPoint.x < PopoverWidth && touchPoint. x > 0 &&
        touchPoint.y < PopoverHeight && touchPoint.y > 0) {
        int ans = touchPoint.x/35;
        printf("%d\n",ans);
        
        parent_->setMethod (ans,index_);
    } else {
        
    }
    parent_->getParentLayer()->setEnabled(true);
    removeFromParent();
    instanse = nullptr;
    setVisible(false);
}

