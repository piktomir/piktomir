//
//  PMPopoverConditionbutton.h
//  pictomir-cocos2d-x
//
//  Created by Danila Eremin on 17.12.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__PMPopoverConditionbutton__
#define __pictomir_cocos2d_x__PMPopoverConditionbutton__

#include <iostream>
#include "cocos-ext.h"
#include "MethodLayer.h"

USING_NS_CC;
USING_NS_CC_EXT;

class PMPopoverConditionButton : public LayerColor {
public:
    static PMPopoverConditionButton * instanse;
    static PMPopoverConditionButton * getInstanse ();
    static PMPopoverConditionButton* create(int width, int realHeight, Node* parent);
    virtual bool init();
    void setup (int width, int realHeight);

    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    void touchEnded(cocos2d::Touch *touch, cocos2d::Event *pEvent);
    
    CC_SYNTHESIZE(bool, showed, Showed);
    
    MethodLayer * parent_;
};

#endif /* defined(__pictomir_cocos2d_x__PMPopoverConditionbutton__) */
