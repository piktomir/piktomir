#include "AppDelegate.h"
#include "LoadLayer.h"
#include "AppMacros.h"
#include "utils.h"
#include <sys/stat.h>
#include "SimpleAudioEngine.h"

#include "PMSettings.h"
#include "FTPuser.h"
#include "PMTextureCache.h"
#include "PMManager.h"

USING_NS_CC;
using namespace CocosDenshion;


AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate() 
{
    SimpleAudioEngine::end();
//    PMTextureCache::deleteInstanse();
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    
    if(!glview)
    {
#if(CC_TARGET_PLATFORM != CC_PLATFORM_IOS && CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID)
        glfwInit();
        
        GLFWmonitor* _primaryMonitor = glfwGetPrimaryMonitor();
        if (nullptr == _primaryMonitor)
            return false;
        
        const GLFWvidmode* videoMode = glfwGetVideoMode(_primaryMonitor);
        
        glview = GLView::createWithRect("Piktomir", Rect(0, 0, videoMode->width, videoMode->height - 50), 1.0f);

        director->setOpenGLView(glview);
#else
	glview = GLView::create("Piktomir");
	director->setOpenGLView(glview);	
#endif
    }
    
    director->setProjection(Director::Projection::_2D);
    director->setDisplayStats(false);
    

#if(CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 && MAP_EDITOR)
	
    // copy ZIP in writablePath

    std::string fromstr = CCFileUtils::sharedFileUtils()->fullPathForFilename("zip.exe");
	CopyFileA(fromstr.c_str(), (writablePath + "zip.exe").c_str(), FALSE);

     
#endif
    
    
#if(CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
    platform_mkdir(writablePath.substr(0,writablePath.size()-1).c_str());
    
    std::vector<std::string> searchPaths;
    searchPaths.push_back(mediumResource.directory);
    FileUtils::getInstance()->setSearchPaths(searchPaths);
    director->setContentScaleFactor(mediumResource.size.height/designResolutionSize.height);
    
#endif

    
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::NO_BORDER);
	Size frameSize = glview->getFrameSize();
    
    if (frameSize.height > mediumResource.size.height)
	{
        std::vector<std::string> searchPaths;
        searchPaths.push_back(largeResource.directory);
        searchPaths.push_back(".");
        FileUtils::getInstance()->setSearchPaths(searchPaths);
        director->setContentScaleFactor(largeResource.size.height/designResolutionSize.height);
	}
    // if the frame's height is larger than the height of small resource size, select medium resource.
    else if (frameSize.height > iPhone5Resource.size.height)
    {
        std::vector<std::string> searchPaths;
        searchPaths.push_back(mediumResource.directory);
        searchPaths.push_back(".");
        FileUtils::getInstance()->setSearchPaths(searchPaths);
        director->setContentScaleFactor(mediumResource.size.height/designResolutionSize.height);
    }
    // if the frame's height is smaller than the height of medium resource size, select small resource.
	else if (frameSize.width == iPhone5Resource.size.height)
    {
		std::vector<std::string> searchPaths;
        searchPaths.push_back(iPhone5Resource.directory);
        searchPaths.push_back(".");
        FileUtils::getInstance()->setSearchPaths(searchPaths);
        glview->setDesignResolutionSize(iPhone5Resource.size.width, iPhone5Resource.size.height, ResolutionPolicy::NO_BORDER);
    }
    
    else if (frameSize.width == iPhoneResource.size.height)
    {
		std::vector<std::string> searchPaths;
        searchPaths.push_back(iPhone5Resource.directory);
        searchPaths.push_back(".");
        FileUtils::getInstance()->setSearchPaths(searchPaths);
        glview->setDesignResolutionSize(iPhoneResource.size.width, iPhoneResource.size.height, ResolutionPolicy::NO_BORDER);
    }
    
    
#endif
    
    PMSettings::instance()->setScreenSize(Director::getInstance()->getWinSize());
    CCLOG("screenSize.width = %f, height = %f", PMSettings::instance()->getScreenSize().width, PMSettings::instance()->getScreenSize().height);
    
#ifndef MAP_EDITOR
    SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Sounds/background.wav");
    SimpleAudioEngine::getInstance()->playBackgroundMusic("Sounds/background.wav", true);
    
    if(!PMSettings::instance()->getSoundEnabled())
        SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    
    SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(PMSettings::instance()->getBackgroundVolume());
    SimpleAudioEngine::getInstance()->setEffectsVolume(PMSettings::instance()->getEffectVolume());
    
    SimpleAudioEngine::getInstance()->preloadEffect("Sounds/loose_sound.wav");
    SimpleAudioEngine::getInstance()->preloadEffect("Sounds/win_sound.wav");
#endif
/*
    CCLOG("%f",(&*******&*&******&*****&***&*&*******&****&******&*&***&*****&*&******&sin)(10));
    CCLOG("%f",(&******&***&***********&**&***&*****&***&*&*****&***&***&****&*&******&sin)(10));
    CCLOG("%f",(&******&*********&*****&*&*****&***&***&**&****&*****&***&************&sin)(10));
    CCLOG("%f",(&*******&********&*****&*******&***&******&****&***&*&***&************&sin)(10));
    CCLOG("%f",(&********&*******&*****&*******&***&******&****&**&**&***&************&sin)(10));
    CCLOG("%f",(&*********&******&*****&*******&***&******&****&*&***&***&***&*&******&sin)(10));
    CCLOG("%f",(&**********&*****&*****&*******&***&******&****&*****&***&***&*&******&sin)(10));
    CCLOG("%f",(&******&***&*****&*****&*******&****&*****&*****&***&***&***&*&*******&sin)(10));
    CCLOG("%f",(&*******&*&******&*****&*******&*****&**&*&*&****&*&***&***&*&********&sin)(10));
    CCLOG("%f",(&sin)(10));
*/
  
    //RunNetworkThread
    
    PMManager::instance();
    
    MenuItemFont::setFontName(PMSettings::instance()->getFont());
    //Director::getInstance()->getConsole()->listenOnTCP(5678);
//    PMSettings::instance()->setMapPath("Maps");
//    PMSettings::instance()->loadWorlds();
    
#ifndef MAP_EDITOR
    director->runWithScene(LoadLayer::scene());
#else
    PMSettings::instance()->setMapPath(writablePath + "Maps/");
    director->runWithScene(MainMenuLayer::scene());
#endif
    
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
	if(PMSettings::instance()->getSoundEnabled())
		SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
	if(PMSettings::instance()->getSoundEnabled())
		 SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
