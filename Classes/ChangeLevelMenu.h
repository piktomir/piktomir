//
//  ChangeLevelMenu.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/17/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef __pictomir_cocos2d_x__ChangeLevelMenu__
#define __pictomir_cocos2d_x__ChangeLevelMenu__

#include <iostream>

#include <iostream>
#include "PMSettings.h"

USING_NS_CC;

class GameLayer;

class ChangeLevelMenu : public cocos2d::Layer
{
public:
    
    virtual bool init();
    
    CREATE_FUNC(ChangeLevelMenu);
    
    void setEnabled(bool flag);
    
    ChangeLevelMenu() :
        cocos2d::Layer(),
        parent(nullptr)
    {}
    
    
    ChangeLevelMenu(GameLayer *parent) :
        cocos2d::Layer(),
        parent(parent)
    {}
    
    static ChangeLevelMenu* create(GameLayer *parent)
    {
        ChangeLevelMenu *programLayer = new ChangeLevelMenu(parent);
        
        programLayer->init();
        programLayer->autorelease();
        
        return programLayer;
    }
    
    ~ChangeLevelMenu()
    {
        removeAllChildrenWithCleanup(true);
    }
    
    void saveStates();
    void restoreStates();
    
private:
    GameLayer *parent;
    cocos2d::Menu *menu;
    
    void nextLevel(cocos2d::Ref *sender);
    void prevLevel(cocos2d::Ref *sender);
    
    bool state;
};

#endif /* defined(__pictomir_cocos2d_x__ChangeLevelMenu__) */
