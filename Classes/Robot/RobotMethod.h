//
//  Method.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#ifndef pictomir_cocos2d_x_RobotMethod_h
#define pictomir_cocos2d_x_RobotMethod_h

#include "cocos2d.h"
#include "PMSettings.h"
#include "VariableList.h"
#include "protocol.h"

class AbstractRobot;

typedef void (AbstractRobot::* ROBOT_NATIVE_METHOD)(/*AbstractRobot **/);

const char NO_INSTR = -1;
const char INSTR_EXECUTE = 0;
const char INSTR_CHECK_CONDITION= 1;
const char INSTR_START_LOOP = 2;
const char INSTR_END_LOOP = 3;
const char INSTR_RETURN = 4;
const char INSTR_TEST = 5;


struct _instruction
{
    short command;
    short param;
    short otherParams[2];
    
    _instruction &operator=(const _instruction &other)
    {
        command = other.command;
        param = other.param;
        memcpy(otherParams, other.otherParams, 2);
        
        return *this;
    }
};

const _instruction RETURN_INSTR = {INSTR_RETURN, 0, {0, 0}};

struct _method
{
    enum ExecuteType
    {
        pmExecuteAlways = 0,
        pmExecuteOnTrue,
        pmExecuteOnFalse
    };
    
    enum BackgroundType
    {
        pmNone = 0,
        pmNormal,
        pmBlocked,
        pmChangeBackground
    };
    
    _method():
        methodID(-1),
        backgroundType(pmNormal),
        executeType(pmExecuteOnTrue)
    {}
    
    _method(int methodID, BackgroundType backgroundType, ExecuteType executeType):
        methodID(methodID),
        backgroundType(backgroundType),
        executeType(executeType)
    {}
    
    int methodID;
    BackgroundType backgroundType;
    
    ExecuteType executeType;
};

class RobotMethod
{
public:
    enum MethodType
    {
        NativeMethod,
        ProgrammedMethod,
        TempMethod
    };
    
    RobotMethod():
        parent(nullptr),
        methodType(NativeMethod),
        nativeMethod((ROBOT_NATIVE_METHOD)nullptr),
        inverseMethod(0)
    {}
    
    static RobotMethod* create()
    {
        return new RobotMethod;
    }
    
    RobotMethod(const RobotMethod &other):
        parent(other.parent),
        methodType(other.methodType),
        nativeMethod(other.nativeMethod),
        instructionList(other.instructionList),
        inverseMethod(other.inverseMethod)
    {}
        
    RobotMethod(AbstractRobot *parent,ROBOT_NATIVE_METHOD method,int inverseMethod):
        parent(parent),
        methodType(NativeMethod),
        nativeMethod(method),
        inverseMethod(inverseMethod)
    {}
    
    static RobotMethod* create(AbstractRobot *parent,ROBOT_NATIVE_METHOD method,int inverseMethod)
    {
        return new RobotMethod(parent, method, inverseMethod);
    }
    
    RobotMethod(AbstractRobot *parent,const std::vector<_instruction> instructionList, MethodType methodType):
        parent(parent),
        methodType(methodType),
        nativeMethod((ROBOT_NATIVE_METHOD)nullptr),
        instructionList(instructionList),
        inverseMethod(0)
    {}
    
    static RobotMethod* create(AbstractRobot *parent,const std::vector<_instruction> instructionList, MethodType methodType)  {
        return new RobotMethod(parent,instructionList,methodType);
    }

    void execute() const;
    static std::vector<_instruction> makeMethod(int method, std::vector<_method> methods,int layerIndex,int condition, int repeater, bool reservedTag);

    bool hasNotDefaultMethods();
    
    
    AbstractRobot *parent;
    SYNTHESIZE(MethodType, methodType, MethodType);
    ROBOT_NATIVE_METHOD nativeMethod;
    SYNTHESIZE(int, inverseMethod, InverseMethod);
        
public:    
    std::vector<_instruction> instructionList;

};

#endif
