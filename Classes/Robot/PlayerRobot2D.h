//
//  PlayerRobot2D.h
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 7/9/13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#ifndef pictomir_cocos2d_x_PlayerRobot2D_h
#define pictomir_cocos2d_x_PlayerRobot2D_h

#include "cocos2d.h"
#include "PlayerRobot.h"


class PlayerRobot2D : public PlayerRobot
{
    SYNTHESIZE(cocos2d::Point, curPosition, CurPosition);
    SYNTHESIZE(int, curDirection, CurDirection);
    
    SYNTHESIZE(cocos2d::Point, oldPosition, OldPosition);
    SYNTHESIZE(int, oldDirection, OldDirection);
    
    SYNTHESIZE(cocos2d::Point, startPosition, StartPosition);
    SYNTHESIZE(int, startDirection, StartDirection);
    
public:
    PlayerRobot2D(AbstractWorld *parentWorld):
        PlayerRobot(parentWorld),
        curPosition( cocos2d::Point::ZERO ),
        curDirection(0),
        oldPosition( cocos2d::Point::ZERO ),
        oldDirection(0),
        startPosition( cocos2d::Point::ZERO ),
        startDirection(0)
    {} 
};

#endif
