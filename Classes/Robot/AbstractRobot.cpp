//
//  AbstractRobot.cpp
//  pictomir-cocos2d-x
//
//  Created by [scaR] on 02.10.13.
//  Copyright (c) 2013 NIISI. All rights reserved.
//

#include "AbstractRobot.h"

using namespace std;

void AbstractRobot::checkTask()
{
    clearStateFlag(AbstractRobot::PlayingAnimation);
    clearStateFlag(AbstractRobot::Win);
    
    if(taskList->complete())
    {
        setStateFlag(AbstractRobot::Win);
    }
}

vector<int> AbstractRobot::getMethodList()
{
    vector<int> ret;
    
    for(map<int,RobotMethod>::iterator i = methodList.begin(); i != methodList.end(); ++i)
    {
        ret.push_back((*i).first);
    }
    
    return ret;
}

void AbstractRobot::cleanTempMethods()
{
    for(map<int,RobotMethod>::iterator i = methodList.begin(); i != methodList.end();)
    {
        if((*i).second.getMethodType() == RobotMethod::TempMethod)
        {
            methodList.erase(i++);
        }
        else
        {
            ++i;
        }
    }
    
}

void AbstractRobot::cleanAllMethods()
{
    for(map<int,RobotMethod>::iterator i = methodList.begin(); i != methodList.end();)
    {
        if((*i).second.getMethodType() != RobotMethod::NativeMethod)
        {
            methodList.erase(i++);
        }
        else
        {
            ++i;
        }
    }
    
}

void AbstractRobot::setMethodListFromNet(_pt_robot_instr_list *netList)
{
    cleanAllMethods();
    
    for(unsigned short i = 0; i < netList->programNum; ++i)
    {
        vector<_instruction> instrList;

        std::vector<_method> methodList;
        
        for(unsigned short j = 0; j < netList->programs[i].methodNum; ++j)
        {
            methodList.push_back( _method(netList->programs[i].methods[j],
                                          _method::pmNormal, (_method::ExecuteType)netList->programs[i].execType[j]));
        }
        
        instrList = RobotMethod::makeMethod(netList->programs[i].ID,methodList, -1,
                                            netList->programs[i].condition , netList->programs[i].repeater, true);
        
        addMethod(netList->programs[i].ID, RobotMethod(this, instrList,RobotMethod::TempMethod));
        
    }
    
}