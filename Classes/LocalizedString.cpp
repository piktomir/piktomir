//
//  LocalizedString.cpp
//  SkeletonX
//
//  Created by mac on 11-12-1.
//  Copyright (c) 2011年 GeekStudio. All rights reserved.
//

#include "cocos2d.h"

USING_NS_CC;

#include <iostream>
#include <sstream>
#include <string>
#include <map>
using namespace std;

#include "LocalizedString.h"

#include "PMSettings.h"

static ValueMap localizedStrings;

void ResetLanguage()
{
    localizedStrings.clear();
}

std::string LocalizedString(string key)
{
    if(localizedStrings.empty())
    {
        string postfix;
        
        switch ((LanguageType)PMSettings::instance()->getLanguage())
        {
            case LanguageType::RUSSIAN:
                postfix = "ru";
                break;
            case LanguageType::ENGLISH:
                postfix = "en";
                break;
            default:
                postfix = "en";
                break;
        }
    
        localizedStrings = cocos2d::FileUtils::getInstance()->getValueMapFromFile("localizations/Localized-" + postfix + ".plist");
    }
    
    if(localizedStrings.find(key) != localizedStrings.end())
        return localizedStrings.at(key).asString();
    else
        return "Not localized";
}
