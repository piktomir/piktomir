//
//  TaskList.h
//  pictomir
//
//  Created by [scaR] on 22.02.14.
//  Copyright (c) 2014 NIISI. All rights reserved.
//

#ifndef __pictomir__TaskList__
#define __pictomir__TaskList__

#include <map>
#include "Task.h"

enum TaskListType
{
    pmCompleteAllTasks,
    pmCompleteOneTask
};

class TaskList
{
private:
    TaskListType type;
    std::map<TaskType,Task *> tasks;
    
public:
    
    TaskList()
    {}
    
    TaskList(TaskListType type):
        type(type)
    {}
    
    void update(const std::shared_ptr<_task_update_data> &data)
    {
        if(tasks.find(data->taskType) != tasks.end())
            tasks[data->taskType]->update(data);
    }
    
    bool addTask(TaskType type, Task* task)
    {
        if(tasks.find(type) != tasks.end())
            return false;
        
        tasks.insert(std::pair<TaskType, Task*>(type,task));
        return true;
    }
    
    Task* getTask(TaskType type)
    {
        if(tasks.find(type) == tasks.end())
            return nullptr;
        
        return tasks[type];
    }
    
    bool complete()
    {
        bool ret;
        
        switch(type)
        {
            case pmCompleteAllTasks:
            {
                ret  = true;
                for(auto i = tasks.begin(); i != tasks.end(); ++i)
                {
                    if(!i->second->complete())
                    {
                        ret = false;
                        break;
                    }
                }
                break;
            }
            case pmCompleteOneTask:
            {
                ret  = false;
                for(auto i = tasks.begin(); i != tasks.end(); ++i)
                {
                    if(i->second->complete())
                    {
                        ret = true;
                        break;
                    }
                }
                break;
            }
        }
        
        return ret;
    }
    
    void clear()
    {
        for(auto i = tasks.begin(); i != tasks.end(); ++i)
        {
            i->second->clear();
        }

    }
    
    bool empty()
    {
        return tasks.empty();
    }
    
    ~TaskList()
    {
        for(auto i = tasks.begin(); i != tasks.end(); ++i)
            delete i->second;
    }
};


#endif /* defined(__pictomir__TaskList__) */
