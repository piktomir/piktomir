//
//  Robot4.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#include "Robot4.h"
#include "World4.h"
#include "PMSettings.h"
#include "GameLayer.h"
#include "PMTextureCache.h"
#include "EnvRobot2D.h"

USING_NS_CC;

using namespace std;

int sign (int a)
{
    if (a > 0) {
        return 1;
    } else if (a < 0) {
        return -1;
    } else return 0;
}

bool Robot4::init()
{
    if( !Sprite::init() )
        return false;
    
    stepCount = 0;
    
    retain();
    
    std::pair<int,RobotMethod> _move(Robot4::Move, RobotMethod(this,(ROBOT_NATIVE_METHOD)&Robot4::move, Robot4::MoveReverse));
    std::pair<int,RobotMethod> _turnLeft(Robot4::TurnLeft, RobotMethod(this,(ROBOT_NATIVE_METHOD)&Robot4::turnLeft,Robot4::TurnRight));
    std::pair<int,RobotMethod> _turnRight(Robot4::TurnRight, RobotMethod(this,(ROBOT_NATIVE_METHOD)&Robot4::turnRight,Robot4::TurnLeft));
    std::pair<int,RobotMethod> _paint(Robot4::Paint, RobotMethod(this,(ROBOT_NATIVE_METHOD)&Robot4::paint, Robot4::UnPaint));
    std::pair<int,RobotMethod> _moveReverse(Robot4::MoveReverse, RobotMethod(this,(ROBOT_NATIVE_METHOD)&Robot4::moveReverse, Robot4::Move));
    std::pair<int,RobotMethod> _unPaint(Robot4::UnPaint, RobotMethod(this,(ROBOT_NATIVE_METHOD)&Robot4::unPaint, Robot4::Paint));
    
    methodList.insert(_move);
    methodList.insert(_turnLeft);
    methodList.insert(_turnRight);
    methodList.insert(_paint);
    methodList.insert(_moveReverse);
    methodList.insert(_unPaint);
    
    return true;
}

void Robot4::initTexture()
{
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile(PICT_PATH "Robot/robot4.plist");
    
    SpriteFrame *frame = cache->getSpriteFrameByName(StringUtils::format("robot%d.png" , 5 * curDirection));
    
    setDisplayFrame(frame);
    
    //setAnchorPoint(Point::ZERO);
    
    removeAllChildrenWithCleanup(true);
}

void Robot4::incStepCount()
{
    ++stepCount;
    
    if(parentWorld->getBaseRobot() == this)
        ((Map4 *)parentWorld->getMap())->getControlLayer()->setStepCount(stepCount);
}

void Robot4::resetRobot()
{
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    removeAllChildrenWithCleanup(true);
    stopAllActions();
    setPosition(startPosition, startLayer, startDirection);
    
    taskList->clear();
    
    map->getControlLayer()->setStepCount(0);
}

void Robot4::setPosition(Point point, int layer, int direction)
{
    oldPosition = curPosition = point;
    oldDirection = curDirection = direction;
    oldLayer = curLayer = layer;
    
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    map->mapElements[layer][point.y][point.x]->addRobot(this);
    
    SpriteFrameCache *cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile(PICT_PATH "Robot/robot4.plist");
    
    SpriteFrame *frame = cache->getSpriteFrameByName(StringUtils::format("robot%d.png" , 5 * curDirection));
    
    setDisplayFrame(frame);
    
    
    Sprite::setPosition( map->Coordinates(point.x, point.y, -20, 20) );
}

bool Robot4::isGrassAt(Point point, int layer)
{
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    return (map->mapElements[layer][point.y][point.x]->getType() == MapElement::GreenGrass ||
            map->mapElements[layer][point.y][point.x]->getType() == MapElement::BlueGrass ||
            map->mapElements[layer][point.y][point.x]->getType() == MapElement::BrokenGrass ||
            map->mapElements[layer][point.y][point.x]->getType() == MapElement::RepairedGrass);
}

bool Robot4::canMoveTo(int x, int y, int direction)
{
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    return map->element(curLayer, curPosition.x, curPosition.y)->nearElements[direction] != nullptr && isGrassAt(Point(x,y), curLayer);
}

void Robot4::moveTo(int x, int y, bool reverse)
{
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    int direction = reverse ? (curDirection + 2) % 4 : curDirection;
    
    if(!canMoveTo(x, y, direction))
    {
        setStateFlag(AbstractRobot::Broken);
    }
    
    map->element(curLayer, curPosition.x, curPosition.y)->removeRobot(this);
    
    curPosition = Point(x, y);
    
    map->element(curLayer, curPosition.x, curPosition.y)->addRobot(this);
    
    map->update();
}

void Robot4::endMove()
{
    setOldPosition(getCurPosition());
    
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    for(auto &i : map->element(curLayer, curPosition.x, curPosition.y)->getRobots())
    {
        i->interact(this, CC_CALLBACK_1(Robot4::endInteract,this));
    }
    
    checkTask();
    
    incStepCount();
    
    map->centerMap();
}

void Robot4::endPaint()
{
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    if(!map->mapElements[curLayer][curPosition.y][curPosition.x]->isPainted())
    {
        map->mapElements[curLayer][curPosition.y][curPosition.x]->repair();
        taskList->update(CREATE_TASK_DATA(_paint_task_update_data, true,  map->mapElements[curLayer][curPosition.y][curPosition.x]->getType() == MapElement::RepairedGrass ));
    }
    
    checkTask();
    
    incStepCount();
}

void Robot4::endUnPaint()
{
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    if(map->mapElements[curLayer][curPosition.y][curPosition.x]->isPainted())
    {
        map->mapElements[curLayer][curPosition.y][curPosition.x]->disrepair();
        taskList->update(CREATE_TASK_DATA(_paint_task_update_data, false,  map->mapElements[curLayer][curPosition.y][curPosition.x]->getType() == MapElement::BrokenGrass ));
    }
    
    checkTask();
    
    incStepCount();
}

void Robot4::endTurn()
{
    oldDirection = curDirection;
    
    checkTask();
    
    incStepCount();
}

void Robot4::endInteract(AbstractRobot *robot)
{
    checkTask();
    
    incStepCount();
}

void Robot4::moveReverse()
{
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    int X = curPosition.x;
    int Y = curPosition.y;
    
    switch(curDirection)
    {
        case 0 : Y--; break;
        case 1 : X--; break;
        case 2 : Y++; break;
        case 3 : X++; break;
    }
    
    bool move = true;
    
    for(auto &i : map->mapElements[curLayer][Y][X]->getRobots())
    {
        if(!i->canMoveOn())
        {
            if(!i->interactable())
                setStateFlag(AbstractRobot::Broken);
            
            move = false;
            break;
        }
    }
    
    if(move)
    {
        moveTo(X, Y, true);
        
        taskList->update(CREATE_TASK_DATA(_position_task_update_data, curLayer, curPosition));
        
        if(!isBroken())
        {
            setStateFlag(AbstractRobot::PlayingAnimation);
            
            FiniteTimeAction* MoveRobot = MoveTo::create(PMSettings::instance()->getAnimationSpeed() / 2,
                                                         map->Coordinates(curPosition.x, curPosition.y, -20, 20) );
            FiniteTimeAction* MoveEnd = CallFunc::create(CC_CALLBACK_0(Robot4::endMove, this));
            runAction( Sequence::create(MoveRobot, MoveEnd, nullptr) );
        }
    }
    else if(!isBroken())
    {
        for(auto &i : map->mapElements[curLayer][Y][X]->getRobots())
        {
            i->interact(this, CC_CALLBACK_1(Robot4::endInteract,this));
        }
    }
}

void Robot4::move()
{
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    int X = curPosition.x;
    int Y = curPosition.y;
    
    switch(curDirection)
    {
        case 0 : Y++; break;
        case 1 : X++; break;
        case 2 : Y--; break;
        case 3 : X--; break;
    }
    
    bool move = true;
    
    for(auto &i : map->element(curLayer, X, Y)->getRobots())
    {
        if(!i->canMoveOn())
        {
            if(!i->interactable())
                setStateFlag(AbstractRobot::Broken);
            
            move = false;
            break;
        }
    }
    
    if(move)
    {
        moveTo(X, Y, false);
        
        taskList->update(CREATE_TASK_DATA(_position_task_update_data, curLayer, curPosition));
        
        if(!isBroken())
        {
            setStateFlag(AbstractRobot::PlayingAnimation);
            
            FiniteTimeAction* MoveRobot = MoveTo::create(PMSettings::instance()->getAnimationSpeed() / 2,
                                                         map->Coordinates(curPosition.x, curPosition.y, -20, 20) );
            FiniteTimeAction* MoveEnd = CallFunc::create(CC_CALLBACK_0(Robot4::endMove, this));
            runAction( Sequence::create(MoveRobot, MoveEnd, nullptr) );
        }
    }
    else if(!isBroken())
    {
        for(auto &i : map->mapElements[curLayer][Y][X]->getRobots())
        {
            i->interact(this, CC_CALLBACK_1(Robot4::endInteract,this));
        }
    }
}

void Robot4::turnLeft()
{
    
    curDirection = (curDirection + 1) % 4;
    //++stepCount;
    
    setStateFlag(AbstractRobot::PlayingAnimation);
    //clearStateFlag(AbstractRobot::Ready);
    
    AnimationCache *cache = AnimationCache::getInstance();
    cache->addAnimationsWithFile(PICT_PATH "Robot/robot4-animations.plist");
    Animation *animation = cache->getAnimation(StringUtils::format("%d->%d" , oldDirection, curDirection));
    animation->setDelayPerUnit(PMSettings::instance()->getAnimationSpeed()/5);
    Animate *animate = Animate::create(animation);
    
    
    FiniteTimeAction* TurnEnd = CallFunc::create(CC_CALLBACK_0(Robot4::endTurn, this));
    runAction( Sequence::create(animate,TurnEnd, nullptr) );
}

void Robot4::turnRight()
{
    curDirection = (curDirection + 3) % 4;
    //++stepCount;
    
    setStateFlag(AbstractRobot::PlayingAnimation);
    //clearStateFlag(AbstractRobot::Ready);
    
    AnimationCache *cache = AnimationCache::getInstance();
    cache->addAnimationsWithFile(PICT_PATH "Robot/robot4-animations.plist");
    Animation *animation = cache->getAnimation(StringUtils::format("%d->%d" , oldDirection, curDirection));
    animation->setDelayPerUnit(PMSettings::instance()->getAnimationSpeed()/5);
    Animate *animate = Animate::create(animation);
    
    FiniteTimeAction* TurnEnd = CallFunc::create(CC_CALLBACK_0(Robot4::endTurn, this));
    runAction( Sequence::create(animate,TurnEnd, nullptr) );
}

void Robot4::paint()
{
//    World4 *world = (World4 *)getParentWorld();
//	Map4 *map = world->getMap4();
    //
    //    if (map->mapElements[getCurLayer()][curPosition.y][curPosition.x]->getType() != MapElement::GreenGrass) {
    //			return;
    //	}
    //
    //++stepCount;
    setStateFlag(AbstractRobot::PlayingAnimation);
    //clearStateFlag(AbstractRobot::Ready);
    
    AnimationCache *cache = AnimationCache::getInstance();
    cache->addAnimationsWithFile(PICT_PATH "Robot/robot4-animations.plist");
    Animation *animation = cache->getAnimation(StringUtils::format("paint%d", curDirection));
    animation->setDelayPerUnit(PMSettings::instance()->getAnimationSpeed()/3.5);
    Animate *animate = Animate::create(animation);
    
    
    FiniteTimeAction* PaintEnd = CallFunc::create(CC_CALLBACK_0(Robot4::endPaint, this));
    runAction( Sequence::create(animate,PaintEnd, nullptr) );
}

void Robot4::unPaint()
{
//    World4 *world = (World4 *)getParentWorld();
//	Map4 *map = world->getMap4();
    
    //    if (map->mapElements[getCurLayer()][curPosition.y][curPosition.x]->getType() != MapElement::GreenGrass) {
    //        return;
    //	}
    
    //++stepCount;
    setStateFlag(AbstractRobot::PlayingAnimation);
    //clearStateFlag(AbstractRobot::Ready);
    
    AnimationCache *cache = AnimationCache::getInstance();
    cache->addAnimationsWithFile(PICT_PATH "Robot/robot4-animations.plist");
    Animation *animation = cache->getAnimation(StringUtils::format("paint%d", curDirection));
    animation->setDelayPerUnit(PMSettings::instance()->getAnimationSpeed()/3.5);
    Animate *animate = Animate::create(animation);
    
    FiniteTimeAction* PaintEnd = CallFunc::create(CC_CALLBACK_0(Robot4::endUnPaint, this));
    runAction( Sequence::create(animate,PaintEnd, nullptr) );
}

void Robot4::win()
{
    //fireWorks = ParticleFireworks::create();
    
    //fireWorks->setPosition(Point::ZERO);
    
    //addChild(fireWorks);
}

void Robot4::destroy()
{
    AnimationCache *cache = AnimationCache::getInstance();
    cache->addAnimationsWithFile(PICT_PATH "Robot/robot4-animations.plist");
    Animation *animation = cache->getAnimation(StringUtils::format("destroy%d", curDirection));
    animation->setDelayPerUnit(PMSettings::instance()->getAnimationSpeed()/3);
    Animate *animate = Animate::create(animation);
    
    runAction(animate);
}

bool Robot4::checkCondition(int condition)
{
    Condition cond  = (Condition) condition;
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    incStepCount();
    
    if (cond == Robot4::GrassIsBlue) {
        return
        map->mapElements[curLayer][curPosition.y][curPosition.x]->getType() == MapElement::BlueGrass ||
        map->mapElements[curLayer][curPosition.y][curPosition.x]->getType() == MapElement::RepairedGrass;
    } else if (cond == Robot4::GrassIsGreen) {
        return map->mapElements[curLayer][curPosition.y][curPosition.x]->getType() == MapElement::GreenGrass ||
        map->mapElements[curLayer][curPosition.y][curPosition.x]->getType() == MapElement::BrokenGrass;
    } else if (cond == Robot4::Wall) {
        return isNearWall();
    } else if (cond == Robot4::NoWall) {
        return !isNearWall();
    }
    return true;
}

bool Robot4::isNearWall()
{
    Map4 *map = ((World4 *)parentWorld)->getMap4();
    
    if (curDirection == 0) {
        return map->mapElements[curLayer][curPosition.y][curPosition.x]->nearElements[Map4::Down] == nullptr;
    } else if (curDirection == 1) {
        return map->mapElements[curLayer][curPosition.y][curPosition.x]->nearElements[Map4::Right] == nullptr;
    } else if (curDirection == 2) {
        return map->mapElements[curLayer][curPosition.y][curPosition.x]->nearElements[Map4::Up] == nullptr;
    } else {
        return map->mapElements[curLayer][curPosition.y][curPosition.x]->nearElements[Map4::Left] == nullptr;
    }
}