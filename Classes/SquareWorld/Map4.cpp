//
//  Map4.cpp
//  pictomir-cocos2d-x
//
//  Created by Nikita Besshaposhnikov on 28.11.12.
//  Copyright (c) 2012 NIISI. All rights reserved.
//

#include "Map4.h"
#include "PMSettings.h"
#include "World4.h"
#include "GameLayer.h"
#include "ScrollLayer.h"
#include "MapContainer.h"
#include "ScrollLayer.h"
#include "ControlLayer4.h"

#include "MapEditorGameLayer.h"

USING_NS_CC;

cocos2d::Point Map4::Coordinates(int x, int y, double dx, double dy)
{
    return Point(mapElementSize.width * x + dx,getRealHeight()-mapElementSize.height * y + dy);
}

Map4 *Map4::create()
{
    Map4 *map = new Map4();
    map->autorelease();
    return map;
}

Map4 *Map4::create(World4* parent,int width, int height, int layerNum, int tileset)
{
    Map4 *map = new Map4(parent, width, height, layerNum, tileset);
    map->init();
    map->autorelease();
    return map;
}

bool Map4::init()
{
    if( !AbstractMap::init() )
    {
        return false;
    }
    
    setContentSize(PMSettings::instance()->mapSize());
    setPosition( Point(0, 0) );
    
    
    for (int i = 0; i < mapElements.size(); ++i) {
        mapElements[i] = std::vector<std::vector<MapElement *> >();
        mapElements[i].resize(height);
        for (int j = 0; j < height; j++) {
            mapElements[i][j] = std::vector<MapElement *>();
            mapElements[i][j].resize(width);
            for (int k = 0; k < width; k++) {
                mapElements[i][j][k] = new MapElement(4);
            }
        }
    }
    
    return true;
}

void Map4::drawMap(bool previewDraw)
{
    scroll  = ScrollLayer::create(this,
                                  PMSettings::instance()->mapSize(),
                                  previewDraw
                                  );
    
    scroll->setPosition(Point::ZERO);
    
    activeLayer = ((PlayerRobot2D *)_world->getBaseRobot())->getStartLayer();
    
    for (int j = 0; j < mapElements.size(); ++j) {
        Layer *mapLayer = Layer::create();
        mapLayer->setContentSize(Size(getRealWidth(), getRealHeight()));
        MapContainer *mapContainer  = MapContainer::create(mapLayer);
        mapContainer->setContentSize(Size(getRealWidth(), getRealHeight()));
        
        
        layers.push_back(mapContainer);
        if(!previewDraw)
        {
            if(j <= activeLayer + DISPLAYED_LAYERS/2 && j>= activeLayer - DISPLAYED_LAYERS/2 )
            {
                scroll->getParallax()->addChild(layers[j], 1, Point(1.0, 1.0),
                                                Point(PMSettings::instance()->mapSize().width/2, PMSettings::instance()->mapSize().height/2));
            }
        }
        //mapLayer->setRotation( 45.0f );
    
        for (int  i = 0; i < _world->robotCount(); ++i)
        {
            if(_world->robotAt(i)->parentType() == pmPlayerRobot)
            {
                PlayerRobot2D *robot = (PlayerRobot2D *)_world->robotAt(i);
                
                if (robot->getCurLayer() == j)
                {
                    robot->setRotation(-45.0f);
                    robot->setScaleY(2.0f);
                    mapLayer->addChild(robot,2);
                }
            }
            else if(_world->robotAt(i)->parentType() == pmEnvRobot)
            {
                EnvRobot2D *robot = (EnvRobot2D *)_world->robotAt(i);
                
                if (robot->getCurLayer() == j)
                {
                    robot->setRotation(-45.0f);
                    robot->setScaleY(2.0f);
                    mapLayer->addChild(robot,2);
                    
                    for(auto &i : robot->getParts())
                        addChild(i, 2);
                    
                }
            }
        }

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                Sprite *sprite = mapElements[j][y][x]->getGrassSprite();
                
                if (sprite != nullptr)
                {
                    mapLayer->addChild(sprite, 0);
                }
                
                sprite = mapElements[j][y][x]->getWallSprite();
                
                if (sprite != nullptr) {
                    mapLayer->addChild(sprite,1);
                }
            }
        }
        
#ifdef MAP_EDITOR
        resizeSprite = Sprite::create("System/unchecked.png");
        resizeSprite->setPosition(Coordinates(width, 0, -mapElementSize.width / 2, mapElementSize.height / 2));
        mapLayer->addChild(resizeSprite,1);
#endif
        
        //layers[j]->setScaleY(0.5);
        
       
        
    }
    if(!previewDraw)
    {
        scrollToInitialPosition();
        
        addChild(scroll);
        
        controlLayer = ControlLayer4::create(world()->getBaseRobotManager());
        addChild(controlLayer,10);
    }
}

void Map4::update()
{    
    for (int j = 0; j < mapElements.size(); ++j)
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                Sprite *sprite = mapElements[j][y][x]->getWallSprite();
                mapElements[j][y][x]->setWallZOrder(2 * getWallZOrder(x,y,j));
                if (sprite != nullptr)
                {
                    layers[j]->getMapLayer()->reorderChild(sprite, mapElements[j][y][x]->getWallZOrder());
                }
            }
        }
    }
    
    for (int i = 0; i < _world->robotCount(); ++i)
    {
        if(_world->robotAt(i)->parentType() == pmPlayerRobot)
        {
            PlayerRobot2D *robot  = (PlayerRobot2D *)_world->robotAt(i);
        
            int x = robot->getCurPosition().x;
            int y = robot->getCurPosition().y;
            int layer = robot->getCurLayer();
        
            layers[layer]->getMapLayer()->reorderChild(robot, mapElements[layer][y][x]->getWallZOrder() + 1);
        }
        if(_world->robotAt(i)->parentType() == pmEnvRobot)
        {
            EnvRobot2D *robot  = (EnvRobot2D *)_world->robotAt(i);
            
            int x = robot->getCurPosition().x;
            int y = robot->getCurPosition().y;
            int layer = robot->getCurLayer();
            
            layers[layer]->getMapLayer()->reorderChild(robot, mapElements[layer][y][x]->getWallZOrder() + 1);
        }
        
    }
    
    centerMap();
}

int Map4::getWallZOrder(int x, int y, int layer)
{
    int ZX = 0;
    int ZY = 0;
    
    for (int i = 0; i < _world->robotCount(); ++i)
    {
        PlayerRobot2D *robot  = (PlayerRobot2D *)_world->robotAt(i);
        
        if (robot->getCurLayer()!=layer)
            continue;
        else if (x >robot->getCurPosition().x)
            ZY++;
        else if (y > robot->getCurPosition().y)
            ZX++;
    }
    
    if (ZX == _world->robotCount() )
        return 2*ZX;
    else if (ZY == _world->robotCount())
        return 2*ZY;
    else if (ZX != 0 || ZY != 0)
        return ZX+ZY;
    
    return 0;
}

void Map4::centerMap()
{
    int layer = ((PlayerRobot2D *)_world->getBaseRobot())->getCurLayer();
    
    Point robotPosotion = layers[layer]->getMapLayer()->convertToWorldSpaceAR(((PlayerRobot2D *)_world->getBaseRobot())->getPosition());
    
    FiniteTimeAction* moveLayerX = nullptr;
    FiniteTimeAction* moveLayerY = nullptr;
    
    //Layer *mapLayer = layers[layer];
    
    if(robotPosotion.x < 25 )
        moveLayerX = MoveTo::create(SYSTEM_ANIMATION_DELAY, Point(scroll->getParallax()->getPositionX() + 100 ,scroll->getParallax()->getPositionY()));
    
    if(robotPosotion.x > PMSettings::instance()->mapSize().width - 25)
        moveLayerX = MoveTo::create(SYSTEM_ANIMATION_DELAY, Point(scroll->getParallax()->getPositionX() - 100 ,scroll->getParallax()->getPositionY()));
    
    if(robotPosotion.y < 25)
        moveLayerY = MoveTo::create(SYSTEM_ANIMATION_DELAY, Point(scroll->getParallax()->getPositionX() ,scroll->getParallax()->getPositionY() + 100));
    
    if(robotPosotion.y > PMSettings::instance()->mapSize().height - 25)
        moveLayerY = MoveTo::create(SYSTEM_ANIMATION_DELAY, Point(scroll->getParallax()->getPositionX() ,scroll->getParallax()->getPositionY() - 100));
    
    if(moveLayerX != nullptr && moveLayerY !=nullptr)
        scroll->getParallax()->runAction( Sequence::create(moveLayerX, moveLayerY, nullptr));
    
    else if(moveLayerY != nullptr && moveLayerX == nullptr)
        scroll->getParallax()->runAction( Sequence::create(moveLayerY, nullptr));
    
    else if(moveLayerY == nullptr && moveLayerX != nullptr)
        scroll->getParallax()->runAction( Sequence::create(moveLayerX, nullptr));
}

void Map4::clean()
{
    for (int j = 0; j < layers.size(); ++j)
    {
        for (int i = 0; i < height; ++i)
        {
            for (int k = 0; k < width; ++k)
            {
                mapElements[j][i][k]->restore();
            }
        }
    }
    
    for (int i = 0; i < world()->robotCount(); ++i)
    {
        world()->robotAt(i)->resetRobot();
    }
    
    scroll->setPosition( Point::ZERO );
    
    scrollToInitialPosition();
    
    update();
}

void Map4::setEnabled(bool flag)
{
    AbstractMap::setEnabled(flag);
    
    controlLayer->setEnabled(flag);
}

void Map4::deleteMapElement (MapElement* mapElement,int layer) {
    layers[layer]->getMapLayer()->removeChild(mapElement->getGrassSprite());
    layers[layer]->getMapLayer()->removeChild(mapElement->getWallSprite());
}

void Map4::drawMapElement (MapElement* mapElement,int layer) {
    if(mapElement->getGrassSprite()!=nullptr)layers[layer]->getMapLayer()->addChild(mapElement->getGrassSprite(),0);
    if(mapElement->getWallSprite()!=nullptr)layers[layer]->getMapLayer()->addChild(mapElement->getWallSprite(),1);
}


int Map4::resizeAtTouch(cocos2d::Point point)
{
#ifdef MAP_EDITOR
    MapEditorGameLayer::getInstanse()->changeMapSize (width+point.x, height+point.y);
    resizeSprite->setPosition(Coordinates(width, 0, -mapElementSize.width / 2, mapElementSize.height / 2));
    
    if(point.x!=0 && point.y != 0) return 0;
    if(point.x!=0) return 1;
    if(point.y!=0) return 2;
    return -1;
#else
	return -1;
#endif
}

void Map4::resize (int width_, int height_) {
    if (width_ < width) {
        int dw = width-width_;
        for (int i = 0; i < layers.size(); ++i) {
            for (int j = 0; j < height; ++j) {
                
                mapElements[i][j][width_-1]->nearElements[Map4::Right] = nullptr;
                for (int k = 0; k < dw; ++k) {
                    deleteMapElement(mapElements[i][j][width-k-1],i);
                    mapElements[i][j].pop_back();
                }
            }
        }
    } else if (width_ > width) {
        int dw = width_-width;
        for (int i = 0; i < mapElements.size(); ++i) {
            for (int j = 0; j < height; ++j) {
                mapElements[i][j].resize(width_);
                
                for (int k = 0; k < dw; ++k) {
                    mapElements[i][j][width+k] = new MapElement(4);
                    
                    mapElements[i][j][width+k]->setElement(MapElement::GreenGrass);
                    
                    if (j > 0) {
                        mapElements[i][j][width+k]->nearElements[Map4::Up] = mapElements[i][j-1][width+k];
                        mapElements[i][j-1][width+k]->nearElements[Map4::Down] = mapElements[i][j][width+k];
                    }
                    mapElements[i][j][width+k]->nearElements[Map4::Down] = nullptr;
                    
                    mapElements[i][j][width+k]->nearElements[Map4::Right] = nullptr;
                    mapElements[i][j][width+k]->nearElements[Map4::Left] = mapElements[i][j][width+k-1];
                    mapElements[i][j][width+k-1]->nearElements[Map4::Right] = mapElements[i][j][width+k];
                    drawMapElement(mapElements[i][j][width+k],i);
                }
            }
        }
    }
    
    width=width_;
    
    if (height_ < height) {
        for (int i = 0; i < mapElements.size(); ++i) {
            for (int j = 0; j < width; ++j) {
                mapElements[i][height_-1][j]->nearElements[Map4::Down] = nullptr;
            }
            for (int k = height-1; k >= height_; --k)
                for (int j = 0; j < width; ++j)
                {
                    deleteMapElement(mapElements[i][k][j],i);
                }
            mapElements[i].pop_back();
        }
    } else if (height_ > height) {
        for (int i = 0; i < mapElements.size(); ++i) {
            mapElements[i].resize(height_);
            for (int k = height; k < height_; ++k) {
                mapElements[i][k] = std::vector<MapElement *>();
                mapElements[i][k].resize(width_);
                for (int j = 0; j < width_; j++) {
                    mapElements[i][k][j] = new MapElement(4);
                }
                
                for (int j = 0; j < width_; ++j) {
                    mapElements[i][k][j]->setElement(MapElement::GreenGrass);
                    
                    mapElements[i][k][j]->nearElements[Map4::Up] = mapElements[i][k-1][j];
                    mapElements[i][k-1][j]->nearElements[Map4::Down] = mapElements[i][k][j];
                    
                    mapElements[i][k][j]->nearElements[Map4::Down] = nullptr;
                    mapElements[i][k][j]->nearElements[Map4::Right] = nullptr;
                    
                    if(j > 0) {
                        mapElements[i][k][j]->nearElements[Map4::Left] = mapElements[i][k][j-1];
                        mapElements[i][k][j-1]->nearElements[Map4::Right] = mapElements[i][k][j];
                    }
                    
                    drawMapElement(mapElements[i][k][j],i);
                }
            }
        }
    }
    
    height=height_;
    
    for (int i = 0; i < mapElements.size(); ++i) {
        layers[i]->getMapLayer()->setContentSize(Size(getRealWidth(), getRealHeight()));
        layers[i]->setContentSize(Size(getRealWidth(), getRealHeight()));
        for (int j = 0; j < height; ++j) {
            for (int k = 0; k < width; ++k) {
                cocos2d::Point coord = Coordinates(k,j,0,0);
                if(mapElements[i][j][k]->getWallSprite())mapElements[i][j][k]->getWallSprite()->setPosition(coord + Point(-7,7));
                if(mapElements[i][j][k]->getGrassSprite())mapElements[i][j][k]->getGrassSprite()->setPosition(coord );
            }
        }
    }
    
    
    for (int i = 0; i < _world->robotCount(); ++i) {
        if(getWorld()->robotAt(i)->type() == pmRobot4)((PlayerRobot2D*)(getWorld()->robotAt(i)))->setPosition(Coordinates(((PlayerRobot2D*)(getWorld()->robotAt(i)))->getCurPosition().x, ((PlayerRobot2D*)(getWorld()->robotAt(i)))->getCurPosition().y, -20, 20));
    }
    
    update();
}

void Map4::changeLayers (int layer) {
    if (layer < mapElements.size()) {
        int dl = mapElements.size() - layer;
        for (int i = 0; i < dl; ++i) {
            scroll->removeChild(layers[mapElements.size()-1-i]);
            mapElements.pop_back();
            layers.pop_back();
        }
    } else if (layer > mapElements.size()) {
        int dl = layer - mapElements.size();
        mapElements.resize(layer);
        for (int i = 0; i < dl; ++i) {
            mapElements[layer-dl+i] = std::vector<std::vector<MapElement *> >();
            mapElements[layer-dl+i].resize(height);
            for (int j = 0; j < height; ++j) {
                mapElements[layer-dl+i][j].resize(width);
                for (int k = 0; k < width; ++k) {
                    mapElements[layer-dl+i][j][k] = new MapElement(4);
                    mapElements[layer-dl+i][j][k]->setElement(MapElement::GreenGrass);
                    if(k > 0) {
                        mapElements[layer-dl+i][j][k]->nearElements[Map4::Up] = mapElements[layer-dl+i][j][k-1];
                        mapElements[layer-dl+i][j][k-1]->nearElements[Map4::Down] = mapElements[layer-dl+i][j][k];
                    }
                    mapElements[layer-dl+i][j][k]->nearElements[Map4::Down] = nullptr;
                    mapElements[layer-dl+i][j][k]->nearElements[Map4::Right] = nullptr;
                    
                    if(j > 0) {
                        mapElements[layer-dl+i][j][k]->nearElements[Map4::Left] = mapElements[layer-dl+i][j-1][k];
                        mapElements[layer-dl+i][j-1][k]->nearElements[Map4::Right] = mapElements[layer-dl+i][j][k];
                    }
                    cocos2d::Point coord = Coordinates(k,j,0,0);
                    if(mapElements[layer-dl+i][j][k]->getWallSprite())mapElements[layer-dl+i][j][k]->getWallSprite()->setPosition(coord + Point(7,-7));
                    if(mapElements[layer-dl+i][j][k]->getGrassSprite())mapElements[layer-dl+i][j][k]->getGrassSprite()->setPosition( coord );
                }
            }
        }
        for (int j = layer-dl; j < mapElements.size(); ++j) {
            Layer *mapLayer = Layer::create();
            mapLayer->setContentSize(Size(getRealWidth(), getRealHeight()));
            MapContainer *mapContainer  = MapContainer::create(mapLayer);
            mapContainer->setContentSize(Size(getRealWidth(), getRealHeight()));
            
            
            layers.push_back(mapContainer);
            
            
            
            //mapLayer->setRotation( 45.0f );
            
            for (int  i = 0; i < _world->robotCount(); ++i)
            {
                PlayerRobot2D *robot = (PlayerRobot2D *)_world->robotAt(i);
                
                if (robot->getCurLayer() == j)
                {
                    robot->setRotation(-45.0f);
                    robot->setScaleY(2.0f);
                    mapLayer->addChild(robot,2);
                    
                }
            }
            
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    Sprite *sprite = mapElements[j][y][x]->getGrassSprite();
                    
                    if (sprite != nullptr)
                    {
                        mapLayer->addChild(sprite, 0);
                    }
                    
                    sprite = mapElements[j][y][x]->getWallSprite();
                    
                    if (sprite != nullptr) {
                        mapLayer->addChild(sprite,1);
                    }
                }
            }
            
            //layers[j]->setScaleY(0.5);
            
            if(j <= activeLayer + DISPLAYED_LAYERS/2 && j>= activeLayer - DISPLAYED_LAYERS/2 ) {
                scroll->addChild(layers[j]);
            }
            
        }
        
        
    }
    
    scrollToInitialPosition();
}


cocos2d::Point Map4::dxdyAtPoint(cocos2d::Point point)
{
    int x = (point.x) / mapElementSize.width;
    int y =  (point.y) / mapElementSize.height;
    return Point(x,y);
}

cocos2d::Point Map4::coordAtPoint(cocos2d::Point point)
{
    int x = (point.x + mapElementSize.width/2) / mapElementSize.width;
    if (x < 0) {
        x = 0;
    }
    if (x >= width) x = width-1;
    int y =  (getRealHeight() - point.y + mapElementSize.width/2) / mapElementSize.height;
    if (y < 0) {
        y = 0;
    }
    if (y >= height) y = height-1;
    return Point(x,y);
}

cocos2d::Point Map4::coordAtTouch(cocos2d::Point point, int layer)
{
#ifdef MAP_EDITOR
	PMMap4Edit active = map4EditorsMenu::getInstanse()->getActive();
	if (active == None) {
		return Point(-1,-1);
	}
	return Point(point.x + mapElementSize.width/2,getRealHeight() - (2*point.y + 1) * mapElementSize.width/2);
#else
	return Point::ZERO;
#endif
}

cocos2d::Point Map4::xyAtTouch (cocos2d::Point point, int layer) {
    int x = (point.x + mapElementSize.width/2) / mapElementSize.width;
    int y =  (getRealHeight() - point.y + mapElementSize.width/2) / mapElementSize.height;
    if( x < 0 || y < 0 || x >= width || y >= height ) return Point(-1,-1);
    return Point(x,y);
}

void Map4::addUpWall (Point point, int layer)
{
    Point pos = Point::ZERO;
    bool LeftWall = false;
    if(!mapElements[layer][point.y][point.x]->nearElements[Up]) {
        return;
    }
    if(!mapElements[layer][point.y][point.x]->nearElements[Left]) LeftWall = true;
    
    mapElements[layer][point.y][point.x]->nearElements[Up]->nearElements[Down] = nullptr;
    mapElements[layer][point.y][point.x]->nearElements[Up] = nullptr;
    
    Sprite * sprite = mapElements[layer][point.y][point.x]->getWallSprite();
    if(sprite)layers[layer]->getMapLayer()->removeChild(sprite);
    if ( point.x==0 || point.y == 0 ) {
        sprite = nullptr;
    } else if(LeftWall) {
        pos = mapElements[layer][point.y][point.x]->getGrassSprite()->getPosition() + Point(-7,7);
        sprite = PMTextureCache::instanse()->getMapWallTile(2);
    } else {
        pos = mapElements[layer][point.y][point.x]->getGrassSprite()->getPosition() + Point(-7,35);
        sprite = PMTextureCache::instanse()->getMapWallTile(1);
    }
    mapElements[layer][point.y][point.x]->setWallSprite(sprite);
    if(sprite) {
        sprite->setPosition(pos);
        layers[layer]->getMapLayer()->addChild(sprite,1);
    }
    
    update();
    return;
}
void Map4::deleteUpWall (Point point, int layer)
{
    Point pos = Point::ZERO;
    bool LeftWall = false;
    if(mapElements[layer][point.y][point.x]->nearElements[Up]) {
        return;
    }
    if ( point.x==0 || point.y == 0 ) return;
    
    if(!mapElements[layer][point.y][point.x]->nearElements[Left]) LeftWall = true;
    
    mapElements[layer][point.y][point.x]->nearElements[Up] = mapElements[layer][point.y-1][point.x];
    mapElements[layer][point.y-1][point.x]->nearElements[Down] = mapElements[layer][point.y][point.x];
    
    Sprite * sprite = mapElements[layer][point.y][point.x]->getWallSprite();
    if(sprite)layers[layer]->getMapLayer()->removeChild(sprite);
    
    if(LeftWall) {
        pos = mapElements[layer][point.y][point.x]->getGrassSprite()->getPosition() + Point(-35,7);
        sprite = PMTextureCache::instanse()->getMapWallTile(0);
    } else sprite = nullptr;
    
    mapElements[layer][point.y][point.x]->setWallSprite(sprite);
    if(sprite) {
        sprite->setPosition(pos);
        layers[layer]->getMapLayer()->addChild(sprite,1);
    }
    
    update();
    return;
}
void Map4::addLeftWall (Point point, int layer)
{
    Point pos = Point::ZERO;
    bool UpWall = false;
    if(!mapElements[layer][point.y][point.x]->nearElements[Left]) {
        return;
    }
    if(!mapElements[layer][point.y][point.x]->nearElements[Up]) UpWall = true;
    
    mapElements[layer][point.y][point.x]->nearElements[Left]->nearElements[Right] = nullptr;
    mapElements[layer][point.y][point.x]->nearElements[Left] = nullptr;
    
    Sprite * sprite = mapElements[layer][point.y][point.x]->getWallSprite();
    if(sprite)layers[layer]->getMapLayer()->removeChild(sprite);
    if ( point.x==0 || point.y == 0 ) {
        sprite = nullptr;
    } else if(UpWall) {
        pos = mapElements[layer][point.y][point.x]->getGrassSprite()->getPosition() + Point(-7,7);
        sprite = PMTextureCache::instanse()->getMapWallTile(2);
    } else {
        pos = mapElements[layer][point.y][point.x]->getGrassSprite()->getPosition() + Point(-35,7);
        sprite = PMTextureCache::instanse()->getMapWallTile(0);
    }
    mapElements[layer][point.y][point.x]->setWallSprite(sprite);
    if(sprite) {
        sprite->setPosition(pos);
        layers[layer]->getMapLayer()->addChild(sprite,1);
    }
    
    update();
    return;
}
void Map4::deleteLeftWall (Point point, int layer)
{
    Point pos = Point::ZERO;
    bool UpWall = false;
    if(mapElements[layer][point.y][point.x]->nearElements[Left]) {
        return;
    }
    if ( point.x==0 || point.y == 0 ) return;
    
    if(!mapElements[layer][point.y][point.x]->nearElements[Up]) UpWall = true;
    
    mapElements[layer][point.y][point.x]->nearElements[Left] = mapElements[layer][point.y][point.x-1];
    mapElements[layer][point.y][point.x-1]->nearElements[Right] = mapElements[layer][point.y][point.x];
    
    Sprite * sprite = mapElements[layer][point.y][point.x]->getWallSprite();
    if(sprite) {
        layers[layer]->getMapLayer()->removeChild(sprite);
    }
    
    if(UpWall) {
        pos = mapElements[layer][point.y][point.x]->getGrassSprite()->getPosition() + Point(-7,35);
        sprite = PMTextureCache::instanse()->getMapWallTile(1);
    } else sprite = nullptr;
    mapElements[layer][point.y][point.x]->setWallSprite(sprite);
    if(sprite) {
        sprite->setPosition(pos);
        layers[layer]->getMapLayer()->addChild(sprite,1);
    }
    
    update();
    return;
}

void Map4::switchLeftWall (Point point, int layer) {
    if (mapElements[layer][point.y][point.x]->nearElements[Left] == nullptr) {
        deleteLeftWall(point, layer);
    } else addLeftWall(point, layer);
}

void Map4::switchUpWall (Point point, int layer) {
    point.y+=1;
    if (mapElements[layer][point.y][point.x]->nearElements[Up] == nullptr) {
        deleteUpWall(point, layer);
    } else addUpWall(point, layer);
}

void Map4::changeElementTypeAtPoint (cocos2d::Point point, int layer)
{
#ifdef MAP_EDITOR
    PMMap4Edit active = map4EditorsMenu::getInstanse()->getActive();
    if (active == None) {
        return;
    }
    int x = (point.x + mapElementSize.width/2) / mapElementSize.width;
    int y =  (getRealHeight() - point.y + mapElementSize.width/2) / mapElementSize.height;
    if( x < 0 || y < 0 || x >= width || y >= height ) return;
    
    if (active <= EditingPaintedGrass4) {
        Point pos = mapElements[layer][y][x]->getGrassSprite()->getPosition();
        
        layers[layer]->getMapLayer()->removeChild(mapElements[layer][y][x]->getGrassSprite());
        mapElements[layer][y][x]->setElement(active-1);
        if(mapElements[layer][y][x]->getGrassSprite()) {
            layers[layer]->getMapLayer()->addChild(mapElements[layer][y][x]->getGrassSprite());
            mapElements[layer][y][x]->getGrassSprite()->setPosition(pos);
        }
        
        update();
        return;
    }
    
    if (active == EditingLeftWall4 ) {
        addLeftWall(Point(x,y),layer);
        return;
    }
    
    if (active == EditingUpWall4 ) {
        addUpWall(Point(x,y),layer);
        return;
    }
    
    if (active == EditingNoLeftWall4 ) {
        deleteLeftWall(Point(x,y),layer);
        return;
    }
    
    if (active == EditingNoUpWall4 ) {
        deleteUpWall(Point(x,y),layer);
        return;
    }
#endif
}

void Map4::drawNewRobot (PlayerRobot2D * robot) {
    robot->setRotation(-45.0f);
    robot->setScaleY(2.0f);
    layers[robot->getCurLayer()]->getMapLayer()->addChild(robot,2);
}
