LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Editor/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/FTP/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Layers/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Map/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/NetEngine/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Robot/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/SquareWorld/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/Teacher/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Classes/XML/*.cpp)

LOCAL_SRC_FILES :=../main.cpp

LOCAL_SRC_FILES += $(FILE_LIST:$(LOCAL_PATH)/%=%)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Editor
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/FTP
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Layers
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Map
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/NetEngine
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Robot
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/SquareWorld
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/Teacher
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Classes/XML
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../libs/enet-linux/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../libs/cocos2d/external/curl/include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../libs/cocos2d/extensions
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../libs/cocos2d
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../libs/cocos2d/cocos/storage/local-storage
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../libs/enet-linux/include

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_localstorage_static
LOCAL_WHOLE_STATIC_LIBRARIES += enet_static

LOCAL_STATIC_LIBRARIES := curl_static_prebuilt
LOCAL_STATIC_LIBRARIES := enet_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,2d)
$(call import-module,audio/android)
$(call import-module,Box2D)
$(call import-module,extensions)
$(call import-module,storage/local-storage)
$(call import-module,../enet-linux)


